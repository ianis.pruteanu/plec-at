<?php

return [

    // Languages
    'languages' => [
        'ro' => 'Română',
        'ru' => 'Русский',
    ],
    'tickets_amount_limit' => 50,
    // User roles
    'roles' => [
        'administrator' => [
            'slug' => 'administrator',
            'value' => '1',
        ],
        'moderator' => [
            'slug' => 'moderator',
            'value' => '2',
        ],
        'owner' => [
            'slug' => 'owner',
            'value' => '3',
        ],
        'driver' => [
            'slug' => 'driver',
            'value' => '4',
        ],
        'user' => [
            'slug' => 'user',
            'value' => '5',
        ],
    ],

    // User statuses
    'user_statuses' => [
        'inactive' => '0',
        'active' => '1',
        'suspended' => '2',
        'deleted' => '3',
    ],

    // Timezones
    'timezones' => [
        '1' => 'Europe/Moscow',
        '2' => 'Europe/Bucharest',
        '3' => 'Europe/Budapest',
        '4' => 'Europe/London'
    ],

    // Currencies
    'currencies' => [
        '1' => 'MDL',
        '2' => 'RON',
        '3' => 'Ruble',
        '4' => 'Euro',
        '5' => 'Dolari',
    ],

    'ticketsPaymentInformation' => [
        1 => 'paid_currency',
        2 => 'paid_amount',
        3 => 'phone_number',
        4 => 'email_address'
    ],
    // Commission Types
    'commissions' => [
        1 => 'Procent',
        2 => 'Fix'
    ],

    // Bus Marks
    'busMarks' => [
        'mercedes' => 'Mercedes',
        'setra' =>'Setra',
        'neoplan' => 'Neoplan',
        'scania' => 'Scania',
        'volvo' => 'Volvo',
        'bova' => 'Bova',
    ],

    // Bus Models
    'busModels' => [
        'mercedes' => [
            'tourismo' => 'Tourismo',
            'travego' => 'Travego',
            '33150' => '33150',
        ],

        'setra' => [
            'alta' => '2001',
            'a2a' => '2010',
        ],
    ],

    // Default user password
    'default_password' => 'chisinauClujNapoca0102',
];