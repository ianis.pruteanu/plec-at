<?php

return [
    'driver'          => 'google',
    'google'          => [
        'apikey' => env('URL_SHORTENER_GOOGLE_API_KEY', 'AIzaSyC4Ha4HJUpqxnAyHYWdRO3PbUEx9bCCVOg'),
    ],
    'bitly'           => [
        'username' => env('URL_SHORTENER_BITLY_USERNAME', ''),
        'password' => env('URL_SHORTENER_BITLY_PASSWORD', ''),
    ],
    'connect_timeout' => 2,
    'timeout'         => 2,
];
