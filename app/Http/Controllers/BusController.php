<?php

namespace App\Http\Controllers;

use App\Models\Buses;
use App\Models\Companies;
use App\Providers\CustomValidationServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Bus;

class BusController extends Controller
{
    function list() {
        $buses = Buses::all();
        return view('backend.buses.list')
            ->with('buses', $buses);
    }

    public function create()
    {
        $companies = Companies::where('status', '<>', 4)->get();
        return view('backend.buses.create')
            ->with('companies', $companies);
    }

    public function store(Request $request)
    {
        // validate the bus form

        // Legal Number Unique
        $uniqueLegalNumber = strtoupper($request->legal_number);
        if (count(Buses::where('legal_number', $request->legal_number)->get()) > 0) {
            return back()->withErrors('Legal Number already taken!')->withInput();
        }

        $bus               = new Buses();
        $bus->company_id   = $request->owner;
        $bus->model        = $request->model;
        $bus->mark         = $request->mark;
        $bus->slug         = $request->model . "-" . $request->mark . "-" . str_slug($request->legal_number);
        $bus->legal_number = $uniqueLegalNumber;
        $bus->year         = $request->year;
        $bus->color        = $request->color;
        $bus->seats        = $request->seats;
        if ($bus->save()) {
            // Flash message
            flash('The bus was successfully created!')->success();
            return redirect()->route('buses.list');
        }
    }

    public function show($slug)
    {

        $companies = Companies::where('status', '<>', 4)->get();
        $bus       = Buses::where('slug', $slug)->firstOrFail();

        return view('backend.buses.edit')
            ->with('bus', $bus)
            ->with('companies', $companies);
    }

    public function edit($slug)
    {

        $bus = Buses::where('slug', $slug)->firstOrFail();
        $companies = Companies::all();
        return view('backend.buses.edit')
            ->with('companies', $companies)
            ->with('bus', $bus);
    }

    public function save(Request $request)
    {

        // get bus id
        $bus = Buses::where('id', $request->bus)->firstOrFail();

        // Legal Number Unique
        $uniqueLegalNumber = strtoupper($request->legal_number);
        if (count(Buses::where('legal_number', $request->legal_number)->get()) > 0) {
            if ((int)Buses::where('legal_number', $request->legal_number)->pluck('id')->first() != $request->bus) {
                return back()->withErrors('Legal Number already taken!')->withInput();
            }
        }

        $bus->company_id   = $request->owner;
        $bus->model        = $request->model;
        $bus->mark         = $request->mark;
        $bus->slug         = $request->model . "-" . $request->mark . "-" . str_slug($request->legal_number);
        $bus->legal_number = $uniqueLegalNumber;
        $bus->year         = $request->year;
        $bus->color        = $request->color;
        $bus->seats        = $request->seats;
        if ($request->image) {
            $imageName = str_slug($request->legal_number) . "." . $request->image->getClientOriginalExtension();
            $request->image->move(public_path('images/buses'), $imageName);
            $bus->image = 'images/buses/' . $imageName;
        }
        if ($bus->save()) {
            flash('The bus was successfully updated!')->success();
            return redirect()->route('buses.list');
        }
    }

    public function getModels($slug)
    {
        $models = config('config.busModels.' . $slug);
        return response()->json($models, 200);
    }
}
