<?php

namespace App\Http\Controllers;

use App\Models\Cities;
use App\Models\Countries;
use App\Models\Places;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class SettingsController extends Controller
{
    public function cities() {

        $countries = Countries::all();
        return view('backend.settings.cities')
            ->with(compact('countries'));
    }

    public function places() {

        return view('backend.settings.places');
    }

    public function addCity(Request $request) {

        $city = new Cities();
        $city->country_id = $request->country;
        $city->slug = $request->slug;
        if($city->save()) {
            return redirect()->back();
        }
    }

    public function addLocation(Request $request) {

        $place = new Places();
        $place->city_id = $request->city;
        $place->type = 1;
        $place->name = $request->slug;
        if($place->save()) {
            return redirect()->back();
        }
    }


    public function listRoles($roleName = null) {

        $roles = Role::all();
        $role = null;
        $permissions = null;
        $allPermissions = null;
        if($roleName) {
            $allPermissions = Permission::all();
            $role = Role::where('name', $roleName)->first();
        }
        return view('backend.settings.roles')
            ->with('roles', $roles)
            ->with('role', $role)
            ->with('allPermissions', $allPermissions);
    }

    public function changeRolePermission($sw, $role, $permission) {
        $rol = Role::where('id', $role)->first();
        ($sw === 'true') ? $rol->givePermissionTo($permission) : $rol->revokePermissionTo($permission);
    }


    public function addPermission(Request $request) {
        $permission = Permission::create(['guard_name' => 'web', 'name' => $request->name]);
        $permission->save();
        return redirect()->back();
    }
}
