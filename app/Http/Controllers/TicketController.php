<?php

namespace App\Http\Controllers;

use App\Mail\Ticket;
use App\Models\Companies;
use App\Models\Courses;
use App\Models\Revenue;
use App\Models\TicketDetails;
use App\Models\Tickets;
use Carbon\Carbon;
use PDF;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    public function list() {

        $tickets = Tickets::orderBy('created_at', 'desc')->get();
        $companies = Companies::all();
        return view('backend.tickets.list')
            ->with('companies', $companies)
            ->with('tickets', $tickets);
    }


    public function show($code) {
        $ticket = Tickets::where('code', $code)->first();
        if(!$ticket) {
            return redirect()->route('tickets.list');
        }
        return view('backend.tickets.show')
            ->with('ticket', $ticket);
    }

    // confirmation after customer take the bus
    public function confirm($id) {
        $ticket = Tickets::where('id', $id)->first();
        if(!$ticket) {
            return redirect()->back();
        }
        $ticket->status = 2;
        $ticket->save();
        foreach(config('config.ticketsPaymentInformation') as $key => $value) {

            if($key > 2) { break; }
            $details = new TicketDetails();
            $details->ticket_id = $ticket->id;
            $details->information_type = $value;
            if($key == 1) {
                $details->information_value = $ticket->trip->course->main_currency;
            } else if ($key == 2) {
                $details->information_value = $ticket->trip->prices()->where('currency', $ticket->trip->course->main_currency)->first()->amount;
            }
            $details->save();

        }
        return redirect()->back();
    }


    public function filter($company = null, $course = null) {
        $today = Carbon::today();
        if($company) {
            $company = Companies::where('id', $company)->first();
            $ticketsList = $company->tickets;

            if($course) {
                $course = Courses::where('id', $course)->first();
                $tripsId = $course->trips->pluck('id');
                $ticketsList = Tickets::whereIn('trip_id', $tripsId)->get();
            }
        } else {
            $ticketsList = Tickets::where('date', '>=', $today)->get();
        }
        $tickets = array();
        foreach($ticketsList as $key => $ticket) {
            $tickets[$key] = array('code' => $ticket->code);
            $tickets[$key] += array('from' => trans('geography.'.$ticket->trip->departureCity->slug));
            $tickets[$key] += array('to' => trans('geography.'.$ticket->trip->arrivalCity->slug));
            $tickets[$key] += array('time' => Carbon::parse($ticket->date.$ticket->trip->departure_time)->format('d M Y H:i'));
            $tickets[$key] += array('user' => $ticket->user->first_name." ".$ticket->user->last_name);
            $tickets[$key] += array('email' => $ticket->user->email);
            if($ticket->user->phone) {
                $tickets[$key] += array('email' => $ticket->user->email." / ".$ticket->user->phone);
            }
            switch ($ticket->status) {
                case 0: $tickets[$key] += array('status' => "<span class=\"\"><i class=\"fa fa-circle-o-notch\"></i>".trans('dashboard.unconfirmed')."</span>");
                case 1: $tickets[$key] += array(
                    'status' => "<span class=\"font-blue\"><i class=\"fa fa-bookmark\"></i>". trans('dashboard.booked')."</span>".
                                        " <a href=\"ticket/".$ticket->id."/confirm\" class=\"btn green-dark btn-cons btn-xs\">".
                                            "<i class=\"fa fa-check\"></i></a>");
                case 2: $tickets[$key] += array('status' => "<span class=\"font-green-dark\"><i class=\"fa fa-hand-pointer-o\"></i>".trans('dashboard.confirmed')."</span>");
                case 3: $tickets[$key] += array('status' => "<span class=\"font-green\"><i class=\"fa fa-check\"></i>".trans('dashboard.paid')."</span>");
                case 4: $tickets[$key] += array('status' => "<span class=\"font-red-intense\"><i class=\"fa fa-close\"></i>".trans('dashboard.canceled')."</span>");
                case 5: $tickets[$key] += array('status' => "<span class=\"font-purple-intense\"><i class=\"fa fa-close\"></i>".trans('dashboard.refunded')."</span>");
            }
        }
        return response()->json($tickets);
    }

    public function filterByDate($date) {
        $date = Carbon::parse($date)->format('Y-m-d');
        $ticketsList = Tickets::where('date', $date)->get();

        $tickets = array();
        foreach($ticketsList as $key => $ticket) {
            $tickets[$key] = array('code' => $ticket->code);
            $tickets[$key] += array('from' => trans('geography.'.$ticket->trip->departureCity->slug));
            $tickets[$key] += array('to' => trans('geography.'.$ticket->trip->arrivalCity->slug));
            $tickets[$key] += array('time' => Carbon::parse($ticket->date.$ticket->trip->departure_time)->format('d M Y H:i'));
            $tickets[$key] += array('user' => $ticket->user->first_name." ".$ticket->user->last_name);
            $tickets[$key] += array('email' => $ticket->user->email);
            if($ticket->user->phone) {
                $tickets[$key] += array('email' => $ticket->user->email." / ".$ticket->user->phone);
            }
            switch ($ticket->status) {
                case 0: $tickets[$key] += array('status' => "<span class=\"\"><i class=\"fa fa-circle-o-notch\"></i>".trans('dashboard.unconfirmed')."</span>");
                case 1: $tickets[$key] += array(
                    'status' => "<span class=\"font-blue\"><i class=\"fa fa-bookmark\"></i>". trans('dashboard.booked')."</span>".
                        " <a href=\"ticket/".$ticket->id."/confirm\" class=\"btn green-dark btn-cons btn-xs\">".
                        "<i class=\"fa fa-check\"></i></a>");
                case 2: $tickets[$key] += array('status' => "<span class=\"font-green-dark\"><i class=\"fa fa-hand-pointer-o\"></i>".trans('dashboard.confirmed')."</span>");
                case 3: $tickets[$key] += array('status' => "<span class=\"font-green\"><i class=\"fa fa-check\"></i>".trans('dashboard.paid')."</span>");
                case 4: $tickets[$key] += array('status' => "<span class=\"font-red-intense\"><i class=\"fa fa-close\"></i>".trans('dashboard.canceled')."</span>");
                case 5: $tickets[$key] += array('status' => "<span class=\"font-purple-intense\"><i class=\"fa fa-close\"></i>".trans('dashboard.refunded')."</span>");
            }
        }
        return response()->json($tickets);
    }


    public function download($id) {
        $ticket = Tickets::where('id', $id)->first();
        if(!$ticket) {
            return redirect()->back();
        }
        $pdf = PDF::loadView('emails.tickets.ticket', ['ticket'=>$ticket]);
        return $pdf->download($ticket->code.'.pdf');
    }

}
