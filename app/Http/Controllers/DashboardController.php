<?php

namespace App\Http\Controllers;

use App\Models\Revenue;
use App\Models\Tickets;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Swap;

class DashboardController extends Controller
{
    public function login() {
        return view('backend.login');
    }

    public function dashboard() {

        $fromDate = Carbon::now()->subDay()->startOfWeek()->toDateString(); // or ->format(..)
        $tillDate = Carbon::now()->subDay()->toDateString();

        // Total summ paid by users
        $totalIncome = Revenue::sum('paid');
        // Total income - Braintree commission = App Income
        $appIncome = Revenue::where('tax_amount', '<>', 0)->where('removed', 0)->select('paid', 'tax_amount')->get();
        $appIncome = $appIncome->sum('paid') - $appIncome->sum('tax_amount');
        // Our business
        $revenue = Revenue::sum('revenue');
        // Tickets counters
        $soldTickets = Revenue::where('tax_amount', '<>', 0)->where('removed', 0)->count();
        $confirmedTickets = Revenue::where('tax_amount', '==', 0)->where('removed', 0)->count();
        // TODAY calcules
        $todayIncome = Revenue::whereDate('created_at', Carbon::today()->toDateString())->sum('paid');
        $soldTicketsToday = Revenue::whereDate('created_at', Carbon::today()->toDateString())->count();
        // WEEK calcules
        $weekIncome = Revenue::whereDate('created_at', '>=', $fromDate)->whereDate('created_at', '<=', $tillDate)->sum('paid');
        $soldTicketsThisWeek = Revenue::whereDate('created_at', '>=', $fromDate)->whereDate('created_at', '<=', $tillDate)->count();

        return view('backend.dashboard')
            ->with('soldTickets', $soldTickets)
            ->with('confirmedTickets', $confirmedTickets)
            ->with('soldTicketsToday', $soldTicketsToday)
            ->with('soldTicketsThisWeek', $soldTicketsThisWeek)
            ->with('totalIncome', number_format($totalIncome, 2, '.', ','))
            ->with('appIncome', number_format($appIncome, 2, '.', ','))
            ->with('todayIncome', number_format($todayIncome, 2, '.', ','))
            ->with('weekIncome', number_format($weekIncome, 2, '.', ','))
            ->with('revenue', $revenue);
    }


    public function calculateRevenue() {

        $calculated = Revenue::select('ticket_id')->where('removed', 0)->get();
        $toCalculate = Tickets::whereIn('status', [2, 3])->whereNotIn('id', $calculated)->get();
        foreach($toCalculate as $ticket) {
            $paidCurrency = $ticket->details->where('information_type', 'paid_currency')->pluck('information_value')->first();
            $paidAmount = $ticket->details->where('information_type', 'paid_amount')->pluck('information_value')->first();
            // check if paid amount was in EURO
            if($paidCurrency <> 4) {
                $exchangeRate = Swap::latest('EUR/' . config('config.currencies.' . $paidCurrency))->getValue(); // get exchange rate
                $paidAmount = $paidAmount / $exchangeRate; // convert paid amount to euro
            }


            // Create revenue record
            $revenue = new Revenue();
            $revenue->ticket_id = $ticket->id;
            $revenue->paid = $paidAmount;
            $revenue->currency = 4; // EURO currency index

            // check if ticket was buyed ONLINE
            if($ticket->status == 3) {
                if($ticket->trip->revenue->type == 1) {
                    $tax_amount = (float) $paidAmount * config('config.braintree.default_rate') / 100 + config('config.braintree.default_commission');
                    $appRevenue = (float) $paidAmount * $ticket->trip->revenue->amount / 100;
                    $companyRevenue = (float) $paidAmount - $tax_amount - $appRevenue;

                    $revenue->tax_rate = config('config.braintree.default_rate');
                    $revenue->tax_amount = $tax_amount;
                    $revenue->comission_type = 1;
                    $revenue->revenue = $appRevenue;
                    $revenue->company_revenue = $companyRevenue;
                } else {
                    if($ticket->trip->revenue->currency <> 4) {
                        $revenueExchangeRate = Swap::latest('EUR/' . config('config.currencies.' . $ticket->trip->revenue->currency))->getValue();
                        $appRevenue = (float) $ticket->trip->revenue->amount / $revenueExchangeRate;
                        $companyRevenue = (float) $revenue->paid - $appRevenue;
                    } else {
                        $appRevenue = $ticket->trip->revenue->amount;
                        $companyRevenue = $revenue->paid - $appRevenue;
                    }
                    $tax_amount = (float) $revenue->paid * config('config.braintree.default_rate') / 100 + config('config.braintree.default_commission');
                    $appRevenue = $appRevenue - $tax_amount;
                    if($appRevenue < 8) {
                        $appRevenue += (float) $revenue->paid * 2 / 100;
                        $companyRevenue = (float) $revenue->paid - $appRevenue;
                    }
                    $revenue->tax_rate = config('config.braintree.default_rate');
                    $revenue->tax_amount = $tax_amount;
                    $revenue->comission_type = 2;
                    $revenue->revenue = $appRevenue;
                    $revenue->company_revenue = $companyRevenue;

                }
            } else {
                if($ticket->trip->revenue->type == 1) {
                    $tax_amount = (float) $paidAmount * config('config.braintree.default_rate') / 100 + config('config.braintree.default_commission');
                    $appRevenue = (float) $paidAmount * $ticket->trip->revenue->amount / 100 + $tax_amount;
                    $companyRevenue = (float) $paidAmount - $appRevenue;

                    $revenue->comission_type = 1;
                    $revenue->revenue = $appRevenue;
                    $revenue->company_revenue = $companyRevenue;
                } else {
                    if($ticket->trip->revenue->currency <> 4) {
                        $revenueExchangeRate = Swap::latest('EUR/' . config('config.currencies.' . $ticket->trip->revenue->currency))->getValue();
                        $appRevenue = (float) $ticket->trip->revenue->amount / $revenueExchangeRate;
                        $companyRevenue = (float) $paidAmount - $appRevenue;
                    } else {
                        $appRevenue = $ticket->trip->revenue->amount;
                        $companyRevenue = $revenue->paid - $appRevenue;
                    }
                    $revenue->comission_type = 2;
                    $revenue->revenue = $appRevenue;
                    $revenue->company_revenue = $companyRevenue;

                }
            }
            $revenue->save();
        }
        return redirect()->back();
    }

}
