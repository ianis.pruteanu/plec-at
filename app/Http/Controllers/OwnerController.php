<?php

namespace App\Http\Controllers;

use App\Models\Courses;
use App\Models\Tickets;
use Illuminate\Http\Request;

class OwnerController extends Controller
{

    private function company() {
        return auth()->user()->company;
    }


    public function dashboard() {

        return view('backend.owner.dashboard');
    }


    public function tickets($comanyName, $courseId = null) {
        $tickets = $this->company()->tickets;
        if($courseId) {
            $course = Courses::where('id', $courseId)->firstOrFail();
            $tripsId = array();
            foreach($course->trips as $trip) {
                $tripsId[] = $trip->id;
            }
            $tickets = Tickets::whereIn('trip_id', $tripsId)->get();
        }
        return view('backend.owner.tickets')
            ->with('tickets', $tickets);
    }


    public function courses() {
        $courses = $this->company()->courses;
        return view('backend.owner.courses')
            ->with('courses', $courses);
    }


    public function settings() {
        $company = $this->company();
        return view('backend.owner.settings')
            ->with(compact('company'));
    }
}
