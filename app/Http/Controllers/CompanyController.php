<?php

namespace App\Http\Controllers;

use App\Models\Companies;
use App\Models\Tickets;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class CompanyController extends Controller
{
    function list() {

        $companies = Companies::all();
        return view('backend.company.list')
            ->with(compact('companies'));
    }


    public function create() {
        $owners = User::role('owner')->where('status', 1)->get();
        return view('backend.company.create')
            ->with(compact('owners'));
    }


    public function store(Request $request) {
        $this->validate($request, [
            'legal_name'     => 'required',
            'displayed_name' => 'required',
        ]);
        if (Input::get('new_owner') == null) {
            $this->validate($request, [
                'owner' => 'required',
            ]);
        } else {
            $this->validate($request, [
                'first_name' => 'required',
                'last_name'  => 'required',
                'email'      => 'nullable|email|unique:users',
                'phone'      => 'nullable|numeric|unique:users',
            ]);
        }

        if ($request->new_owner) {
            $owner             = new User();
            $owner->first_name = $request->first_name;
            $owner->last_name  = $request->last_name;
            $username          = strtolower($request->first_name . '.' . $request->last_name);

            // check to see if already exist in database this username
            $databaseUsernames = DB::table('users')->pluck('username');
            $usernamesArray    = [];
            foreach ($databaseUsernames as $name) {
                array_push($usernamesArray, $name);
            }
            if (in_array($username, $usernamesArray)) {
                $randomNumber       = rand(1, 50);
                $secondRandomNumber = $randomNumber + 1;
                $username           = $username . '.' . $randomNumber;
                // check again to see if the newly created username exist in database
                if (in_array($username, $usernamesArray)) {
                    $username = $username . '.' . $secondRandomNumber;
                }
            }
            $owner->username  = $username;
            $owner->email    = $request->email;
            $owner->phone    = $request->phone;
            $owner->password = bcrypt('roller08');
            $owner->pic      = 'images/users/placeholder.png';
            $owner->status   = 1;
            $saved           = $owner->save();
            $owner->assignRole('owner');

            if ($saved == true) {
                flash('The owner was successfully created!')->success();
                \Session::flash('messages.1.level', 'success');
                \Session::flash('messages.1.content', 'User was created!');
            }
        }

        $company = new Companies();
        if ($request->new_owner) {
            $company->owner_id = $owner->id;
        } else {
            $company->owner_id = $request->owner;
        }
        $company->legal_name     = $request->legal_name;
        $company->slug           = str_slug($request->legal_name);
        $company->displayed_name = $request->displayed_name;
        $company->logo           = 'images/companies/placeholder.png';
        $company->status         = '0';
        $saved                   = $company->save();

        // if ($saved == true) {
        //     \Session::flash('messages.0.level', 'success');
        //     \Session::flash('messages.0.content', 'Company was created!');
        flash('The comapny was successfully created!')->success();
        return redirect()->route('companies.list');
        // }
    }


    public function show($slug)
    {
        $company = Companies::where('slug', $slug)->first();
        if (!$company) {
            \Session::flash('message.level', 'danger');
            \Session::flash('message.content', trans('dashboard.company_unknown'));
            return redirect()->route('companies.list');
        }
        $tickets = Tickets::where('company_id', $company->id)->get();

        return view('backend.company.show')
            ->with('tickets', $tickets)
            ->with(compact('company'));
    }


    public function edit($slug)
    {
        $company = Companies::where('slug', $slug)->first();
        $owners  = User::role('owner')->get();
        if (!$company) {
            \Session::flash('message.level', 'danger');
            \Session::flash('message.content', trans('dashboard.company_unknown'));
            return redirect()->route('companies.list');
        }

        return view('backend.company.edit')
            ->with(compact('owners'))
            ->with(compact('company'));
    }


    public function save(Request $request) {
        $this->validate($request, [
            'legal_name'     => 'required',
            'displayed_name' => 'required',
        ]);
        if (Input::get('new_owner') == null) {
            $this->validate($request, [
                'owner' => 'required',
            ]);
        } else {
            $this->validate($request, [
                'first_name' => 'required',
                'last_name'  => 'required',
                'email'      => 'nullable|email|unique:users',
                'phone'      => 'nullable|numeric|unique:users',
            ]);
        }

        if ($request->new_owner) {
            $owner             = new User();
            $owner->first_name = $request->first_name;
            $owner->last_name  = $request->last_name;
            $username          = strtolower($request->first_name . '.' . $request->last_name);

            // check to see if already exist in database this username
            $databaseUsernames = DB::table('users')->pluck('username');
            $usernamesArray    = [];
            foreach ($databaseUsernames as $name) {
                array_push($usernamesArray, $name);
            }
            if (in_array($username, $usernamesArray)) {
                $randomNumber       = rand(1, 50);
                $secondRandomNumber = $randomNumber + 1;
                $username           = $username . '.' . $randomNumber;
                // check again to see if the newly created username exist in database
                if (in_array($username, $usernamesArray)) {
                    $username = $username . '.' . $secondRandomNumber;
                }
            }
            $owner->username  = $username;
            $owner->email    = $request->email;
            $owner->phone    = $request->phone;
            $owner->password = bcrypt('roller08');
            $owner->pic      = 'images/users/placeholder.png';
            $owner->status   = 1;
            $saved           = $owner->save();
            $owner->assignRole('owner');

            if ($saved == true) {
                flash('The owner was successfully created!')->success();
                \Session::flash('messages.1.level', 'success');
                \Session::flash('messages.1.content', 'User was created!');
            }
        }

        $company = Companies::where('id', $request->company_id)->firstOrFail();
        if (!$company) {
            \Session::flash('messages.1.level', 'danger');
            \Session::flash('messages.1.content', trans('dashboard.company_unknown'));
            return redirect()->route('companies.list');
        }

        $company->legal_name     = $request->legal_name;
        $company->displayed_name = $request->displayed_name;
        if (Input::get('new_owner') == '1') {
            // get from database the newly created user id
            $owserId = DB::table('users')->where('username', $username)->pluck('id')[0];
            $company->owner_id = $owserId;
        }else{
            $company->owner_id = $request->owner;
        }
        if($request->logo) {
            $imageName = str_slug($request->legal_name).".".$request->logo->getClientOriginalExtension();
            $request->logo->move(public_path('images/companies'), $imageName);
            $company->logo = 'images/companies/'.$imageName;
        }
        $saved = $company->save();

        if ($saved == true) {
            flash('The comapny was successfully updated!')->success();
            \Session::flash('messages.1.level', 'info');
            \Session::flash('messages.1.content', trans('dashboard.company_was_edited'));
            return redirect()->route('companies.list');
        }
    }


    public function getCourses($companyId) {
        $company = Companies::where('id', $companyId)->first();
        $courses = $company->courses;
        return response()->json($courses);
    }
}
