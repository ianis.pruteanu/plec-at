<?php

namespace App\Http\Controllers;

use App\Models\Countries;
use App\Models\TripPrices;
use App\Models\TripRevenue;
use App\Models\Trips;
use Illuminate\Http\Request;

class TripController extends Controller
{
    public function create(Request $request)
    {

        $trip                  = new Trips();
        $trip->course_id       = $request->course_id;
        $trip->departure_city  = $request->departure_city;
        $trip->departure_place = $request->departure_place;
        $trip->departure_time  = $request->departure_time;
        $trip->departure_timezone = 2;
        $trip->arrival_city    = $request->arrival_city;
        $trip->arrival_place   = $request->arrival_place;
        $trip->arrival_time    = $request->arrival_time;
        $trip->arrival_timezone = 2;
        $trip->tur_retur       = $request->re_tur;
        $trip->status          = 1;
        $saved = $trip->save();

        $tripRevenue = new TripRevenue();
        $tripRevenue->trip_id = $trip->id;
        $tripRevenue->type = 1;
        $tripRevenue->amount = 0;
        $tripRevenue->status = 1;
        $tripRevenue->save();

        if($saved) {
            return redirect()->back();
        }
    }


    public function show($slug, $id) {

        $trip = Trips::where('id', $id)->first();
        $countries = Countries::all();
        if(!$trip) {
            return redirect()->back();
        }

        $exist_currencies = TripPrices::where('trip_id', $id)
            ->where('status', 1)
            ->pluck('currency')->toArray();

        return view('backend.trip.show')
            ->with('countries', $countries)
            ->with(compact('exist_currencies'))
            ->with('trip', $trip);
    }

    public function save(Request $request) {
        $trip = Trips::where('id', $request->trip_id)->first();
        if(!$trip) {
            return redirect()->back();
        }

        $trip->departure_city = $request->departure_city;
        $trip->departure_place = $request->departure_place;
        $trip->departure_time = $request->departure_time;
        $trip->departure_timezone = $request->departure_timezone;
        $trip->arrival_city = $request->arrival_city;
        $trip->arrival_place = $request->arrival_place;
        $trip->arrival_time = $request->arrival_time;
        $trip->arrival_timezone = $request->arrival_timezone;
        $trip->duration = $request->duration;
        $trip->status = $request->status;
        $trip->can_buy = ($request->can_buy) ? $request->can_buy : 0;
        $trip->pay_at_bus = ($request->can_pay_at_bus) ? $request->can_pay_at_bus : 0;
        $trip->save();
        return redirect()->back();
    }


    public function updatePrices(Request $request) {

        $actualPrices = TripPrices::where('trip_id', $request->trip_id)->where('status', 1)->get();
        foreach($request->price_id as $key => $input) {

            if($input == null AND $request->amount[$key] == null) {
                break;
            } elseif($input == null) {
                $newPrice = new TripPrices();
                $newPrice->trip_id = $request->trip_id;
                $newPrice->currency = $request->currency[$key];
                switch ($request->currency[$key]) {
                    case 1: $newPrice->currency_index = 'mdl'; break;
                    case 2: $newPrice->currency_index = 'ron'; break;
                    case 3: $newPrice->currency_index = 'eur'; break;
                    case 4: $newPrice->currency_index = 'usd'; break;
                    case 5: $newPrice->currency_index = 'rub'; break;
                }
                $newPrice->amount = $request->amount[$key];
                $newPrice->status = 1;
                $newPrice->save();
            } else {
                $findPrice = TripPrices::where('id', $request->price_id[$key])->first();
                if($findPrice->amount <> $request->amount[$key]) {
                    $findPrice->status = 2;
                    $findPrice->save();

                    $newPrice = new TripPrices();
                    $newPrice->trip_id = $request->trip_id;
                    $newPrice->currency = $request->currency[$key];
                    switch ($request->currency[$key]) {
                        case 1: $newPrice->currency_index = 'mdl'; break;
                        case 2: $newPrice->currency_index = 'ron'; break;
                        case 3: $newPrice->currency_index = 'eur'; break;
                        case 4: $newPrice->currency_index = 'usd'; break;
                        case 5: $newPrice->currency_index = 'rub'; break;
                    }
                    $newPrice->amount = $request->amount[$key];
                    $newPrice->status = 1;
                    $newPrice->save();
                }
            }
        }
        return redirect()->back();
    }


    public function updateCommission(Request $request) {
        $trip = Trips::where('id', $request->trip_id)->first();
        if(!$trip->revenue) {
            $revenue = new TripRevenue();
            $revenue->trip_id = $trip->id;
            $revenue->type = $request->type;
            $revenue->amount = $request->rate;
            ($request->type == 1) ? $revenue->currency = null : $revenue->currency = $request->currency;
            $revenue->status = 1;
            $revenue->save();
        } else {
            $trip->revenue->status = 2;
            $trip->revenue->save();

            $revenue = new TripRevenue();
            $revenue->trip_id = $trip->id;
            $revenue->type = $request->type;
            $revenue->amount = $request->rate;
            ($request->type == 1) ? $revenue->currency = null : $revenue->currency = $request->currency;
            $revenue->status = 1;
            $revenue->save();
        }
        return redirect()->back();

    }
    public function closePrice($id) {
        $price = TripPrices::where('id', $id)->first();
        $price->status = 2;
        $price->save();
        return response()->json('',200);
    }
}
