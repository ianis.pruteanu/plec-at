<?php

namespace App\Http\Controllers;

use App\Mail\AccountValidator;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\Providers\CustomValidationServiceProvider;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function list() {
        $users = User::where('status', '<>', 3)->get();
        return view('backend.users.list')
            ->with('users', $users);
    }

    public function create() {
        return view('backend.users.create');
    }

    public function profile($id) {
        $user = User::where('id', $id)->firstOrFail();
        return view('backend.users.profile')
            ->with(compact('user'));
    }

    public function settings($id) {
        $user = User::where('id', $id)->firstOrFail();
        if(Auth::user()->id <> $id) {
            \Session::flash('message.level', 'danger');
            \Session::flash('message.content', 'Access denied!');
            return redirect()->route('user.profile', Auth::user()->id);
        }
        return view('backend.users.settings')
            ->with('user', $user);
    }

    public function edit($id) {
        $user = User::where('id', $id)->first();
        if(!$user) {
            return redirect()->route('users.list');
        }
        return view('backend.users.edit')->with('user', $user);
    }

    public function personalInfo($id){
        $val       = new CustomValidationServiceProvider($this->request);
        $validator = $val->backendPersonalInfoValidator($this->request->all());
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        // save the user contacts
        DB::table('users')->where('id', \Auth::user()->id)->update(['email' => $this->request->email, 'phone' => $this->request->phone]);

        return back()->with('message', 'Your contact details was updated!');
    }

    public function personalPicture($id){
        // validate iamge
        $val       = new CustomValidationServiceProvider($this->request);
        $validator = $val->userImageValidator($this->request->all());
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $user = User::find($id);
        // get file from form
        $file = Input::file('userImage');

        if ($file !== null) {
            $userPhotoName = $file->getClientOriginalName();
            $userImagePath = 'images/users/' . $userPhotoName;
            $file->move('images/users/', $userPhotoName);

            // save image from the form into database
            $user->pic = $userImagePath;
            if ($user->save()) {
                return redirect()->back()->with('message', 'Your Profile Picture was changed!');
            }
        }
    }

    public function personalPassword($id){
        $val       = new CustomValidationServiceProvider($this->request);
        $validator = $val->passwordValidator($this->request->all());
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        // check if current password is corect
        if (!\Auth::attempt(['id' => \Auth::user()->id, 'password' => $this->request->actual_password])) {
            return back()->withErrors('Your Actual Password is incorect!');
        } else {
            DB::table('users')->where('id', \Auth::user()->id)->update(['password' => bcrypt($this->request->new_password)]);
            return back()->with('message', 'Your Password was changed!');
        }
    }

    public function store(Request $request) {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'nullable|email|unique:users',
            'phone' => 'nullable|numeric|unique:users',
            'role' => 'required',
        ]);
        $user = new User;
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $username         = strtolower($request->get('first_name') . '.' . $request->get('last_name'));

        // check to see if already exist in database this username
        $databaseUsernames = DB::table('users')->pluck('username');
        $usernamesArray    = [];
        foreach ($databaseUsernames as $name) {
            array_push($usernamesArray, $name);
        }
        if (in_array($username, $usernamesArray)) {
            $randomNumber = rand(1, 50);
            $secondRandomNumber = $randomNumber + 1;
            $username = $username . '.' . $randomNumber;
            // check again to see if the newly created username exist in database
            if (in_array($username, $usernamesArray)) {
                $username =  $username . '.' . $secondRandomNumber;
            }
        }
        $user->username = $username;
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->password = bcrypt('roller08');
        $user->pic = 'images/users/placeholder.png';
        $user->status = ($request->status) ? 1 : 0;
        $user->confirmation_token = str_random(35);
        $saved = $user->save();
        $user->assignRole($request->input('role'));
        if($saved == true) {
            if(!$request->status) {
                Mail::to($request->email)->send(new AccountValidator($user));
            }
//            // flash('The user was successfully created!')->success();
//            \Session::flash('message.level', 'success');
//            \Session::flash('message.content', 'User was created!');
            return redirect()->route('users.list');
        }
    }

    public function save(Request $request) {
        $user = User::where('id', $request->input('id'))->first();
        if(!$user) {
            return redirect()->route('users.list');
        }

        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'nullable|email|unique:users,email,'.$user->id,
            'phone' => 'nullable|numeric|unique:users,phone,'.$user->id,
        ]);

        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $username         = strtolower($request->get('first_name') . '.' . $request->get('last_name'));

        // check to see if already exist in database this username
        $databaseUsernames = DB::table('users')->pluck('username');
        $usernamesArray    = [];
        foreach ($databaseUsernames as $name) {
            array_push($usernamesArray, $name);
        }
        if (in_array($username, $usernamesArray)) {
            $randomNumber = rand(1, 50);
            $secondRandomNumber = $randomNumber + 1;
            $username = $username . '.' . $randomNumber;
            // check again to see if the newly created username exist in database
            if (in_array($username, $usernamesArray)) {
                $username =  $username . '.' . $secondRandomNumber;
            }
        }
        $user->username = $username;
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        if(Input::get('active') == '1'){
            $user->status = 1;
        }else{$user->status = 0;}
        $user->save();
        $user->syncRoles($request->input('role'));

        flash('The user was successfully updated!')->success();

        return redirect()->route('users.list');
    }

    public function delete($id) {
        $user = User::where('id', $id)->firstOrFail();
        if(!$user) {
            \Session::flash('message.level', 'danger');
            \Session::flash('message.content', 'User unknown!');
            return redirect()->route('users.list');
        }

        if($user->update(['status' => 3])) {
            \Session::flash('message.level', 'info');
            \Session::flash('message.content', 'User was deleted!');
            return redirect()->route('users.list');
        }
    }


    public function resetPassword(Request $request) {
        $user = User::where('id', $request->id)->first();
        if(!$user) {
            return redirect()->route('users.list');
        }
        $user->password = bcrypt($request->password);
        $user->save();
        return redirect()->route('user.edit', $request->id);
    }
}
