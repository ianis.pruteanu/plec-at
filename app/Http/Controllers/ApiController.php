<?php

namespace App\Http\Controllers;

use App\Mail\AccountValidator;
use App\Models\Cities;
use App\Models\Courses;
use App\Models\Tickets;
use App\Models\Trips;
use App\Providers\CustomValidationServiceProvider;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

class ApiController extends Controller
{
    public function getDepartures() {

        $tripDepartures = Trips::all();
        $departures = array();
        $usedDepartures = array();
        foreach ($tripDepartures as $city) {
            if (!in_array($city->departure_city, $usedDepartures)) {
                $departures[] = array(
                    'id' => $city->departure_city,
                    'name' => trans('geography.' . $city->departureCity->slug),
                    'slug' => $city->departureCity->slug
                );
                $usedDepartures[] = $city->departure_city;
            }
        }
        return response()->json($departures);
    }


    public function getDestinations($departureId) {

        $trips = Trips::where('departure_city', $departureId)->get();
        $destinations = array();
        $usedArrivals = array();
        foreach ($trips as $trip) {
            if (!in_array($trip->arrival_city, $usedArrivals)) {
                $destinations[] = array(
                    'id' => $trip->arrival_city,
                    'name' => trans('geography.' . $trip->arrivalCity->slug),
                    'slug' => $trip->arrivalCity->slug
                );
                $usedArrivals[] = $trip->arrival_city;
            }
        }
        return response()->json($destinations);
    }


    public function getTrips(Request $request) {

        $trips = Trips::where('departure_city', $request->departure_city)
            ->where('arrival_city', $request->arrival_city)
            ->get();
        $allowed = array();
        foreach($trips as $trip) {
            switch ($trip->course->frequency) {
                case 1: $allowed[] = $trip->id; break;
                case 2: if($trip->tur_retur == 1) {
                    $correctDate = Carbon::parse($trip->course->start_date)->addDay()->format('Y-m-d');
                    if($this->checkIfInRange($correctDate, $request->date)) { $allowed[] = $trip->id; }
                } else {
                    if($this->checkIfInRange($trip->course->start_date, $request->date)) { $allowed[] = $trip->id; }
                } break;
                case 3: $allowed[] = $trip->id; break;
                case 4: $allowed[] = $trip->id; break;
                case 5: $allowed[] = $trip->id; break;
            }
        }
        $goodTrips = Trips::whereIn('id', $allowed)->where('status', 1)->get();
        $tripsData = array();
        foreach($goodTrips as $key => $trip) {

            /* Calculate duration */
            $start = Carbon::createFromFormat('m-d-Y H:i:s', $request->date.$trip->departure_time, config('config.timezones.'.$trip->departure_timezone));
            $end = Carbon::createFromFormat('Y-m-d H:i:s', $start, config('config.timezones.'.$trip->arrival_timezone))->addDays($trip->duration)->setTime(
                Carbon::parse($trip->arrival_time)->format('H'),
                Carbon::parse($trip->arrival_time)->format('i'),
                Carbon::parse($trip->arrival_time)->format('s'));
            $duration = Carbon::createFromFormat('Y-m-d H:i:s', $start, config('config.timezones.'.$trip->arrival_timezone))->diff($end);
            $days = $duration->days;
            $hours = $duration->h;
            $minutes = $duration->i;

            $carbonDepartureTime = $request->date.' '.$trip->departure_time;
            $now = Carbon::now()->setTimezone(config('config.timezones.'.$trip->departure_timezone));
            $bookingTimeLimit = Carbon::createFromFormat('m-d-Y H:i:s', $carbonDepartureTime, config('config.timezones.'.$trip->departure_timezone))
                ->subHours($trip->course->last_hour_payment);
            /* End Calculations */

            $tripsData[$key] = array('id' => $trip->id);
            $tripsData[$key] += array('slug' => str_slug($trip->departureCity->slug.'-'.$trip->arrivalCity->slug.'-'
                .$trip->id));
            $tripsData[$key] += array('courseId' => $trip->course->id);
            $courseTripsId = $trip->course->trips->pluck('id');
            $ticketsCounter = Tickets::where('date', $request->date)->whereIn('trip_id', $courseTripsId)->count();
            if(($trip->course->defaultBus AND $trip->course->defaultBus->seats == $ticketsCounter)) {
                $tripsData[$key] += array('onlyInfo' => true);
                $tripsData[$key] += array('infoMessage' => 'Sold Out');
            } elseif($ticketsCounter == config('config.tickets_amount_limit')) {
                $tripsData[$key] += array('onlyInfo' => true);
                $tripsData[$key] += array('infoMessage' => 'Sold Out');
            } elseif($now > $bookingTimeLimit) {
                $tripsData[$key] += array('onlyInfo' => true);
            }
            $tripsData[$key] += array('canBuy' => ($trip->can_buy == 1) ? true : false);
            $tripsData[$key] += array('payAtBus' => ($trip->pay_at_bus == 1) ? true : false);
            $tripsData[$key] += array('date' => Carbon::createFromFormat('m-d-Y', $request->date)->format('d m Y'));
            $tripsData[$key] += array('bookDate' => Carbon::createFromFormat('m-d-Y', $request->date)->format('Y-m-d'));
            $tripsData[$key] += array('departureCity' => trans('geography.'.$trip->departureCity->slug));
            $tripsData[$key] += array('departurePlace' => ($trip->departurePlace) ? $trip->departurePlace->name : null);
            $tripsData[$key] += array('departureHour' => Carbon::parse($trip->departure_time)->format('H:i'));
            $tripsData[$key] += array('arrivalCity' => trans('geography.'.$trip->arrivalCity->slug));
            $tripsData[$key] += array('arrivalPlace' => ($trip->arrivalPlace) ? $trip->arrivalPlace->name : null);
            $tripsData[$key] += array('arrivalHour' => Carbon::parse($trip->arrival_time)->format('H:i'));
            $tripsData[$key] += array('duration' => [
                'days' => $days,
                'hours' => $hours,
                'minutes' => $minutes
            ]);
            if($trip->firstPrice()) {
                $tripsData[$key] += array('price' => [
                    'amount' => $trip->firstPrice()->amount,
                    'currency_index' => strtoupper($trip->firstPrice()->currency_index),
                    "currency" => $trip->firstPrice()->currency
                ]);
            }
            if($trip->prices) {
                $tripsData[$key] += array('allPrices' => []);
                foreach($trip->prices as $kp => $price) {
                    $tripsData[$key]['allPrices'][$kp] = array(
                        "amount" => $price->amount,
                        "currency_index" => strtoupper($price->currency_index),
                        "currency" => $price->currency
                    );
                }
            }
        }
        return response()->json($tripsData, 200);
    }


    public function checkIfInRange($startDate, $needDate) {
        $trigger = false;
        $needDate = Carbon::createFromFormat('m-d-Y', $needDate)->format('Y-m-d');
        $travelDate = Carbon::parse($needDate);
        for($start = Carbon::parse($startDate); $start <= $travelDate; $start->addDays(2)) {
            $trigger = ($start == $travelDate) ? true : false;

        }
        return $trigger;
    }


    public function getAllDestinations() {
        $destinationsId = DB::table('trips')
            ->distinct('arrival_city')
            ->pluck('arrival_city')
            ->toArray();

        $destinations = DB::table('cities')->whereIn('id', $destinationsId)->get();
        $destinations = $this->formatCityName($destinations);
        
        return response()->json($destinations, 200);
    }


    public function register(Request $request) {
        if (is_numeric($request->email)) {
            $this->validate($request, [
                'first_name'            => 'required|string|max:255',
                'last_name'             => 'required|string|max:255',
                'email'                 => 'required|numeric|unique:users',
                'password'              => 'required|string|min:6|confirmed',
                'password_confirmation' => 'required|string|min:6',
            ]);
        } else {
            $this->validate($request, [
                'first_name'            => 'required|string|max:255',
                'last_name'             => 'required|string|max:255',
                'email'                 => 'required|string|email|max:255|unique:users,email',
                'password'              => 'required|string|min:6|confirmed',
                'password_confirmation' => 'required|string|min:6',
            ]);
        }

        // replace diacritics with normal text
        $firstName = $this->replaceDiacritics($request->first_name);
        $lastName  = $this->replaceDiacritics($request->last_name);
        // set up the username
        $username         = strtolower($firstName . '.' . $lastName);
        // check to see if already exist in database this username
        $databaseUsernames = DB::table('users')->pluck('username');
        $usernamesArray    = [];
        foreach ($databaseUsernames as $name) {
            array_push($usernamesArray, $name);
        }
        if (in_array($username, $usernamesArray)) {
            $randomNumber = rand(1, 50);
            $secondRandomNumber = $randomNumber + 1;
            $username = $username . '.' . $randomNumber;
            // check again to see if the newly created username exist in database
            if (in_array($username, $usernamesArray)) {
                $username =  $username . '.' . $secondRandomNumber;
            }
        }
        // if the email or phone field is numeric save into the phone column
        if(is_numeric($request->email)){
            $phone = $request->email;
            $email = null;
        }else{
            $email = $request->email;
            $phone = null;
        }
        $user             = new User();
        $user->first_name = $firstName;
        $user->last_name = $lastName;
        $user->username = $username;
        $user->email = $email;
        $user->phone = $phone;
        $user->password = bcrypt($request->password);
        $user->pic = 'images/users/placeholder.png';
        $user->confirmation_token = str_random(30);
        $user->status = 0;
        $user->save();
        $user->assignRole('user');
        $this->registerUserOnBraintree($user);
        Mail::to($user->email)->send(new AccountValidator($user));
        return response()->json(['message' => 'Utilizatorul a fost creat. Verifica email-ul pentru a valida contul.'], 201);
    }


    public function login(Request $request) {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        $credentials = $request->only('email', 'password');
        try {
            if(!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['message' => 'Date incorecte'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['message' => 'Could not create token'], 500);
        }
        $user = Auth::user();
        switch($user->status) {
            case 0:
        }
        return response()->json([
            'token' => $token,
            'user' => $user],
            200);
    }


    public function logout(Request $request) {
        JWTAuth::invalidate($request->token);
        return response()->json('', 200);
    }


    public function getUserProfile($username) {
        if(!$username) {
            return response()->json(['message' => 'User not found'], 404);
        }
        $user = JWTAuth::parseToken()->toUser();
        return response()->json($user, 200);
    }


    public function changePassword(Request $request) {
        $user = JWTAuth::parseToken()->toUser();
        $val       = new CustomValidationServiceProvider($request);
        $validator = $val->passwordValidator($request->all());

        if ($validator->fails()) {
            return response()->json(['message' => 'Parolele nu coincid'], 402);
        }
        // check if current password is corect
        if (!\Auth::attempt(['id' => $user->id, 'password' => $request->actual_password])) {
            return response()->json(['message' => 'Parola actuala este greșită'], 402);
        } else {
            DB::table('users')->where('id', $user->id)->update(['password' => bcrypt($request->new_password)]);
            return response()->json(['message' => 'Parola a fost modificată'], 200);
        }
    }


    public function getUserTickets() {
        $user = JWTAuth::parseToken()->toUser();
        $tickets = array();
        foreach ($user->tickets as $key => $ticket) {
            $tickets[$key] = array('id' => $ticket->id);
            $tickets[$key] += array('code' => $ticket->code);
            $tickets[$key] += array('departureCity' => trans('geography.'.$ticket->trip->departureCity->slug));
            /* Calculate duration */
            $start = Carbon::createFromFormat('Y-m-d H:i:s', $ticket->date.$ticket->trip->departure_time)->toDateTimeString();
            $end = Carbon::createFromFormat('Y-m-d H:i:s', $start)->addDays($ticket->trip->duration)->setTime(
                Carbon::parse($ticket->trip->arrival_time)->format('H'),
                Carbon::parse($ticket->trip->arrival_time)->format('i'),
                Carbon::parse($ticket->trip->arrival_time)->format('s')
            );
            $duration = Carbon::createFromFormat('Y-m-d H:i:s', $start)->diff($end);

            $days = $duration->days;
            $hours = $duration->h;
            $minutes = $duration->i;
            /* End Calculations */

            $tickets[$key] += array('departurePlace' => $ticket->trip->departurePlace->name);
            $tickets[$key] += array('departureHour' => Carbon::parse($ticket->trip->departure_time)->format('H:i'));
            $tickets[$key] += array('arrivalCity' => trans('geography.'.$ticket->trip->arrivalCity->slug));
            $tickets[$key] += array('arrivalPlace' => $ticket->trip->arrivalPlace->name);
            $tickets[$key] += array('arrivalHour' => Carbon::parse($ticket->trip->arrival_time)->format('H:i'));
            $tickets[$key] += array('duration' => [
                'days' => $days,
                'hours' => $hours,
                'minutes' => $minutes
            ]);

            if($ticket->revenue) {
                $tickets[$key] += array('paid' => [
                    'amount' => $ticket->revenue->paid,
                    'currency' => config('config.merchants.' . $ticket->revenue->currency . '.index')
                ]);
            } else {
                $tickets[$key] += array('price' => [
                    'amount' => $ticket->trip->firstPrice()->amount,
                    'currency' => strtoupper($ticket->trip->firstPrice()->currency_index)
                ]);
            }
            switch ($ticket->status) {
                case 0: $tickets[$key] += array('status' => 'Neconfirmat');
                case 1: $tickets[$key] += array('status' => 'Rezervat');
                case 2: $tickets[$key] += array('status' => 'Confirmat');
                case 3: $tickets[$key] += array('status' => 'Achitat');
                case 4: $tickets[$key] += array('status' => 'Anulat');
                case 5: $tickets[$key] += array('status' => 'Returnat');
            }

        }
        return response()->json($tickets, 200);
    }


    public function updateContacts(Request $request) {
        $user = JWTAuth::parseToken()->toUser();
        $request->request->add(['userId' => $user->id]);
        $val = new CustomValidationServiceProvider($request);
        $validator = $val->manageContactsValidator($request->all());
        if ($validator->fails()) {
            return response()->json(['message' => 'Date incorecte'], 402);
        }
        if($request->email <> $user->email || $request->phone <> $user->phone) {
            DB::table('users')
                ->where('id', $user->id)
                ->update([
                    'email' => $request->email,
                    'phone' => $request->phone
                ]);
            return response()->json(['message' => 'Datele au fost actualizate'], 200);
        }

    }


    public function formatCityName($object) {
        $cities = array();
        for($i = 'a'; $i<='z'; $i++) {
            foreach($object as $key => $item) {
                if(substr($item->slug, 0, 1) === $i) {
                    $cities[strtoupper($i)][$key] = array('id' => $item->id);
                    $cities[strtoupper($i)][$key] += array('name' => trans('geography.' . $item->slug));
                }
            }
        }

        return $cities;
    }


    public function registerUserOnBraintree($user) {
        $result = \Braintree_Customer::create([
            'firstName' => $user->first_name,
            'lastName' => $user->last_name,
            'email' => $user->email,
            'phone' => $user->phone
        ]);

        if($result->success) {
            $user->braintree_id = $result->customer->id;
            $user->save();
            return $result->customer->id;
        } else {
            $errors = array();
            foreach($result->errors->deepAll() as $error) {
                $errors[] = $error->message;
            }
            return response()->json($errors, 403);
        }
    }


    public function replaceDiacritics($string_to_replace){
        $transliteration = array(
            'Ĳ' => 'I', 'Ö' => 'O', 'Œ' => 'O', 'Ü' => 'U', 'ä' => 'a', 'æ' => 'a',
            'ĳ' => 'i', 'ö' => 'o', 'œ' => 'o', 'ü' => 'u', 'ß' => 's', 'ſ' => 's',
            'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A',
            'Æ' => 'A', 'Ā' => 'A', 'Ą' => 'A', 'Ă' => 'A', 'Ç' => 'C', 'Ć' => 'C',
            'Č' => 'C', 'Ĉ' => 'C', 'Ċ' => 'C', 'Ď' => 'D', 'Đ' => 'D', 'È' => 'E',
            'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ē' => 'E', 'Ę' => 'E', 'Ě' => 'E',
            'Ĕ' => 'E', 'Ė' => 'E', 'Ĝ' => 'G', 'Ğ' => 'G', 'Ġ' => 'G', 'Ģ' => 'G',
            'Ĥ' => 'H', 'Ħ' => 'H', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
            'Ī' => 'I', 'Ĩ' => 'I', 'Ĭ' => 'I', 'Į' => 'I', 'İ' => 'I', 'Ĵ' => 'J',
            'Ķ' => 'K', 'Ľ' => 'K', 'Ĺ' => 'K', 'Ļ' => 'K', 'Ŀ' => 'K', 'Ł' => 'L',
            'Ñ' => 'N', 'Ń' => 'N', 'Ň' => 'N', 'Ņ' => 'N', 'Ŋ' => 'N', 'Ò' => 'O',
            'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ø' => 'O', 'Ō' => 'O', 'Ő' => 'O',
            'Ŏ' => 'O', 'Ŕ' => 'R', 'Ř' => 'R', 'Ŗ' => 'R', 'Ś' => 'S', 'Ş' => 'S',
            'Ŝ' => 'S', 'Ș' => 'S', 'Š' => 'S', 'Ť' => 'T', 'Ţ' => 'T', 'Ŧ' => 'T',
            'Ț' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ū' => 'U', 'Ů' => 'U',
            'Ű' => 'U', 'Ŭ' => 'U', 'Ũ' => 'U', 'Ų' => 'U', 'Ŵ' => 'W', 'Ŷ' => 'Y',
            'Ÿ' => 'Y', 'Ý' => 'Y', 'Ź' => 'Z', 'Ż' => 'Z', 'Ž' => 'Z', 'à' => 'a',
            'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ā' => 'a', 'ą' => 'a', 'ă' => 'a',
            'å' => 'a', 'ç' => 'c', 'ć' => 'c', 'č' => 'c', 'ĉ' => 'c', 'ċ' => 'c',
            'ď' => 'd', 'đ' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e',
            'ē' => 'e', 'ę' => 'e', 'ě' => 'e', 'ĕ' => 'e', 'ė' => 'e', 'ƒ' => 'f',
            'ĝ' => 'g', 'ğ' => 'g', 'ġ' => 'g', 'ģ' => 'g', 'ĥ' => 'h', 'ħ' => 'h',
            'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ī' => 'i', 'ĩ' => 'i',
            'ĭ' => 'i', 'į' => 'i', 'ı' => 'i', 'ĵ' => 'j', 'ķ' => 'k', 'ĸ' => 'k',
            'ł' => 'l', 'ľ' => 'l', 'ĺ' => 'l', 'ļ' => 'l', 'ŀ' => 'l', 'ñ' => 'n',
            'ń' => 'n', 'ň' => 'n', 'ņ' => 'n', 'ŉ' => 'n', 'ŋ' => 'n', 'ò' => 'o',
            'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ø' => 'o', 'ō' => 'o', 'ő' => 'o',
            'ŏ' => 'o', 'ŕ' => 'r', 'ř' => 'r', 'ŗ' => 'r', 'ś' => 's', 'š' => 's',
            'ť' => 't', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ū' => 'u', 'ů' => 'u',
            'ű' => 'u', 'ŭ' => 'u', 'ũ' => 'u', 'ų' => 'u', 'ŵ' => 'w', 'ÿ' => 'y',
            'ý' => 'y', 'ŷ' => 'y', 'ż' => 'z', 'ź' => 'z', 'ž' => 'z', 'Α' => 'A',
            'Ά' => 'A', 'Ἀ' => 'A', 'Ἁ' => 'A', 'Ἂ' => 'A', 'Ἃ' => 'A', 'Ἄ' => 'A',
            'Ἅ' => 'A', 'Ἆ' => 'A', 'Ἇ' => 'A', 'ᾈ' => 'A', 'ᾉ' => 'A', 'ᾊ' => 'A',
            'ᾋ' => 'A', 'ᾌ' => 'A', 'ᾍ' => 'A', 'ᾎ' => 'A', 'ᾏ' => 'A', 'Ᾰ' => 'A',
            'Ᾱ' => 'A', 'Ὰ' => 'A', 'ᾼ' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D',
            'Ε' => 'E', 'Έ' => 'E', 'Ἐ' => 'E', 'Ἑ' => 'E', 'Ἒ' => 'E', 'Ἓ' => 'E',
            'Ἔ' => 'E', 'Ἕ' => 'E', 'Ὲ' => 'E', 'Ζ' => 'Z', 'Η' => 'I', 'Ή' => 'I',
            'Ἠ' => 'I', 'Ἡ' => 'I', 'Ἢ' => 'I', 'Ἣ' => 'I', 'Ἤ' => 'I', 'Ἥ' => 'I',
            'Ἦ' => 'I', 'Ἧ' => 'I', 'ᾘ' => 'I', 'ᾙ' => 'I', 'ᾚ' => 'I', 'ᾛ' => 'I',
            'ᾜ' => 'I', 'ᾝ' => 'I', 'ᾞ' => 'I', 'ᾟ' => 'I', 'Ὴ' => 'I', 'ῌ' => 'I',
            'Θ' => 'T', 'Ι' => 'I', 'Ί' => 'I', 'Ϊ' => 'I', 'Ἰ' => 'I', 'Ἱ' => 'I',
            'Ἲ' => 'I', 'Ἳ' => 'I', 'Ἴ' => 'I', 'Ἵ' => 'I', 'Ἶ' => 'I', 'Ἷ' => 'I',
            'Ῐ' => 'I', 'Ῑ' => 'I', 'Ὶ' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M',
            'Ν' => 'N', 'Ξ' => 'K', 'Ο' => 'O', 'Ό' => 'O', 'Ὀ' => 'O', 'Ὁ' => 'O',
            'Ὂ' => 'O', 'Ὃ' => 'O', 'Ὄ' => 'O', 'Ὅ' => 'O', 'Ὸ' => 'O', 'Π' => 'P',
            'Ρ' => 'R', 'Ῥ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Ύ' => 'Y',
            'Ϋ' => 'Y', 'Ὑ' => 'Y', 'Ὓ' => 'Y', 'Ὕ' => 'Y', 'Ὗ' => 'Y', 'Ῠ' => 'Y',
            'Ῡ' => 'Y', 'Ὺ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'P', 'Ω' => 'O',
            'Ώ' => 'O', 'Ὠ' => 'O', 'Ὡ' => 'O', 'Ὢ' => 'O', 'Ὣ' => 'O', 'Ὤ' => 'O',
            'Ὥ' => 'O', 'Ὦ' => 'O', 'Ὧ' => 'O', 'ᾨ' => 'O', 'ᾩ' => 'O', 'ᾪ' => 'O',
            'ᾫ' => 'O', 'ᾬ' => 'O', 'ᾭ' => 'O', 'ᾮ' => 'O', 'ᾯ' => 'O', 'Ὼ' => 'O',
            'ῼ' => 'O', 'α' => 'a', 'ά' => 'a', 'ἀ' => 'a', 'ἁ' => 'a', 'ἂ' => 'a',
            'ἃ' => 'a', 'ἄ' => 'a', 'ἅ' => 'a', 'ἆ' => 'a', 'ἇ' => 'a', 'ᾀ' => 'a',
            'ᾁ' => 'a', 'ᾂ' => 'a', 'ᾃ' => 'a', 'ᾄ' => 'a', 'ᾅ' => 'a', 'ᾆ' => 'a',
            'ᾇ' => 'a', 'ὰ' => 'a', 'ᾰ' => 'a', 'ᾱ' => 'a', 'ᾲ' => 'a', 'ᾳ' => 'a',
            'ᾴ' => 'a', 'ᾶ' => 'a', 'ᾷ' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd',
            'ε' => 'e', 'έ' => 'e', 'ἐ' => 'e', 'ἑ' => 'e', 'ἒ' => 'e', 'ἓ' => 'e',
            'ἔ' => 'e', 'ἕ' => 'e', 'ὲ' => 'e', 'ζ' => 'z', 'η' => 'i', 'ή' => 'i',
            'ἠ' => 'i', 'ἡ' => 'i', 'ἢ' => 'i', 'ἣ' => 'i', 'ἤ' => 'i', 'ἥ' => 'i',
            'ἦ' => 'i', 'ἧ' => 'i', 'ᾐ' => 'i', 'ᾑ' => 'i', 'ᾒ' => 'i', 'ᾓ' => 'i',
            'ᾔ' => 'i', 'ᾕ' => 'i', 'ᾖ' => 'i', 'ᾗ' => 'i', 'ὴ' => 'i', 'ῂ' => 'i',
            'ῃ' => 'i', 'ῄ' => 'i', 'ῆ' => 'i', 'ῇ' => 'i', 'θ' => 't', 'ι' => 'i',
            'ί' => 'i', 'ϊ' => 'i', 'ΐ' => 'i', 'ἰ' => 'i', 'ἱ' => 'i', 'ἲ' => 'i',
            'ἳ' => 'i', 'ἴ' => 'i', 'ἵ' => 'i', 'ἶ' => 'i', 'ἷ' => 'i', 'ὶ' => 'i',
            'ῐ' => 'i', 'ῑ' => 'i', 'ῒ' => 'i', 'ῖ' => 'i', 'ῗ' => 'i', 'κ' => 'k',
            'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => 'k', 'ο' => 'o', 'ό' => 'o',
            'ὀ' => 'o', 'ὁ' => 'o', 'ὂ' => 'o', 'ὃ' => 'o', 'ὄ' => 'o', 'ὅ' => 'o',
            'ὸ' => 'o', 'π' => 'p', 'ρ' => 'r', 'ῤ' => 'r', 'ῥ' => 'r', 'σ' => 's',
            'ς' => 's', 'τ' => 't', 'υ' => 'y', 'ύ' => 'y', 'ϋ' => 'y', 'ΰ' => 'y',
            'ὐ' => 'y', 'ὑ' => 'y', 'ὒ' => 'y', 'ὓ' => 'y', 'ὔ' => 'y', 'ὕ' => 'y',
            'ὖ' => 'y', 'ὗ' => 'y', 'ὺ' => 'y', 'ῠ' => 'y', 'ῡ' => 'y', 'ῢ' => 'y',
            'ῦ' => 'y', 'ῧ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'p', 'ω' => 'o',
            'ώ' => 'o', 'ὠ' => 'o', 'ὡ' => 'o', 'ὢ' => 'o', 'ὣ' => 'o', 'ὤ' => 'o',
            'ὥ' => 'o', 'ὦ' => 'o', 'ὧ' => 'o', 'ᾠ' => 'o', 'ᾡ' => 'o', 'ᾢ' => 'o',
            'ᾣ' => 'o', 'ᾤ' => 'o', 'ᾥ' => 'o', 'ᾦ' => 'o', 'ᾧ' => 'o', 'ὼ' => 'o',
            'ῲ' => 'o', 'ῳ' => 'o', 'ῴ' => 'o', 'ῶ' => 'o', 'ῷ' => 'o', 'А' => 'A',
            'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'E',
            'Ж' => 'Z', 'З' => 'Z', 'И' => 'I', 'Й' => 'I', 'К' => 'K', 'Л' => 'L',
            'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S',
            'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'K', 'Ц' => 'T', 'Ч' => 'C',
            'Ш' => 'S', 'Щ' => 'S', 'Ы' => 'Y', 'Э' => 'E', 'Ю' => 'Y', 'Я' => 'Y',
            'а' => 'A', 'б' => 'B', 'в' => 'V', 'г' => 'G', 'д' => 'D', 'е' => 'E',
            'ё' => 'E', 'ж' => 'Z', 'з' => 'Z', 'и' => 'I', 'й' => 'I', 'к' => 'K',
            'л' => 'L', 'м' => 'M', 'н' => 'N', 'о' => 'O', 'п' => 'P', 'р' => 'R',
            'с' => 'S', 'т' => 'T', 'у' => 'U', 'ф' => 'F', 'х' => 'K', 'ц' => 'T',
            'ч' => 'C', 'ш' => 'S', 'щ' => 'S', 'ы' => 'Y', 'э' => 'E', 'ю' => 'Y',
            'я' => 'Y', 'ð' => 'd', 'Ð' => 'D', 'þ' => 't', 'Þ' => 'T', 'ა' => 'a',
            'ბ' => 'b', 'გ' => 'g', 'დ' => 'd', 'ე' => 'e', 'ვ' => 'v', 'ზ' => 'z',
            'თ' => 't', 'ი' => 'i', 'კ' => 'k', 'ლ' => 'l', 'მ' => 'm', 'ნ' => 'n',
            'ო' => 'o', 'პ' => 'p', 'ჟ' => 'z', 'რ' => 'r', 'ს' => 's', 'ტ' => 't',
            'უ' => 'u', 'ფ' => 'p', 'ქ' => 'k', 'ღ' => 'g', 'ყ' => 'q', 'შ' => 's',
            'ჩ' => 'c', 'ც' => 't', 'ძ' => 'd', 'წ' => 't', 'ჭ' => 'c', 'ხ' => 'k',
            'ჯ' => 'j', 'ჰ' => 'h', 'Š' => 'S', 'š' => 's', 'ș' => 's', 'ş' => 's',
            'ă' => 'a', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A',
            'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E',
            'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I',
            'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O',
            'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U',
            'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a', 'â' => 'a',
            'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e',
            'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i',
            'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o',
            'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u',
            'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y', 'ţ' => 't', 'Ţ' => 'T', 'Ț' => 'T',
            'Ș' => 'S', 'Ş' => 'S', 'Â', 'ț' => 't'
        );

        $str = str_replace(array_keys($transliteration), array_values($transliteration), $string_to_replace);

        return $str;
    }
}
