<?php

namespace App\Http\Controllers;

use App\Models\Companies;
use App\Models\Countries;
use App\Models\Courses;
use App\Models\Places;
use App\Models\Tickets;
use App\Providers\CustomValidationServiceProvider;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    function list() {
        $courses = Courses::all();
        return view('backend.courses.list')
            ->with('courses', $courses);
    }

    public function create()
    {
        $countries = Countries::all();
        $companies = Companies::all();
        return view('backend.courses.create')
            ->with('companies', $companies)
            ->with('countries', $countries);
    }

    public function store(Request $request)
    {
        $course                  = new Courses();
        $course->name            = $request->name;
        $course->slug            = str_slug($request->name.$request->departure_hour);
        $course->company_id      = $request->company_id;
        $course->departure_city  = $request->departure_city;
        $course->departure_place = $request->departure_place;
        $course->departure_hour  = $request->departure_hour;
        $course->arrival_city    = $request->arrival_city;
        $course->arrival_place   = $request->arrival_place;
        $course->arrival_hour    = $request->arrival_hour;
        $course->frequency       = $request->frequency;
        if ($request->frequency == 5) {
            $fre = '0000000';
            if ($request->custom_frequency !== null) {
                if (in_array('mon', $request->custom_frequency)) {$fre[0] = 1;}
                if (in_array('tue', $request->custom_frequency)) {$fre[1] = 1;}
                if (in_array('wed', $request->custom_frequency)) {$fre[2] = 1;}
                if (in_array('thu', $request->custom_frequency)) {$fre[3] = 1;}
                if (in_array('fri', $request->custom_frequency)) {$fre[4] = 1;}
                if (in_array('sat', $request->custom_frequency)) {$fre[5] = 1;}
                if (in_array('sun', $request->custom_frequency)) {$fre[6] = 1;}
            }
            $course->custom_frequency = $fre;
        } else {
            $course->custom_frequency = null;
        }
        ($request->frequency == 2) ? $course->start_date = $request->start_date : $course->start_date = null;
        $course->main_currency = $request->main_currency;
        $lastIdentifier = Courses::select('identifier')->get()->last();
        $course->identifier = $lastIdentifier->identifier + 1;
        if ($course->save()) {
            flash('The course was successfully created!')->success();
            return redirect()->route('course.show', $course->slug);
        }

    }

    public function show($slug)
    {
        $course    = Courses::where('slug', $slug)->firstOrFail();
        $countries = Countries::has('cities')->get();
        $tripsId = $course->trips->pluck('id');
        $tickets = Tickets::whereIn('trip_id', $tripsId)->get();
        return view('backend.courses.show')
            ->with('countries', $countries)
            ->with('tickets', $tickets)
            ->with('course', $course);
    }

    public function edit($slug)
    {
        $countries = Countries::all();
        $course    = Courses::where('slug', $slug)->firstOrFail();
        return view('backend.courses.edit')
            ->with(compact('course'))
            ->with('countries', $countries);
    }

    public function save(Request $request)
    {

        $course                  = Courses::where('id', $request->course)->firstOrFail();
        $course->name            = $request->name;
        $course->departure_city  = $request->departure_city;
        $course->departure_place = $request->departure_place;
        $course->departure_hour  = $request->departure_hour;
        $course->arrival_city    = $request->arrival_city;
        $course->arrival_place   = $request->arrival_place;
        $course->arrival_hour    = $request->arrival_hour;
        $course->frequency       = $request->frequency;
        if ($request->frequency == 5) {
            $fre = '0000000';
            if ($request->custom_frequency !== null) {
                if (in_array('mon', $request->custom_frequency)) {$fre[0] = 1;}
                if (in_array('tue', $request->custom_frequency)) {$fre[1] = 1;}
                if (in_array('wed', $request->custom_frequency)) {$fre[2] = 1;}
                if (in_array('thu', $request->custom_frequency)) {$fre[3] = 1;}
                if (in_array('fri', $request->custom_frequency)) {$fre[4] = 1;}
                if (in_array('sat', $request->custom_frequency)) {$fre[5] = 1;}
                if (in_array('sun', $request->custom_frequency)) {$fre[6] = 1;}
            }
            $course->custom_frequency = $fre;
        } else {
            $course->custom_frequency = null;
        }
        ($request->frequency == 2) ? $course->start_date = $request->start_date : $course->start_date = null;
        $course->main_currency = $request->main_currency;
        $course->default_bus_id = $request->default_bus;
        $course->last_hour_payment = $request->limit_hours;
        if ($course->save()) {
            flash('The course was successfully updated!')->success();
            return redirect()->route('course.show', $course->slug);
        }
    }

    public function getPlaces($id)
    {
        $places = Places::where('city_id', $id)->get();
        return response()->json($places);
    }

}
