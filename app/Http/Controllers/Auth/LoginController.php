<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required',
            'password' => 'required',
        ]);
    }

    protected function userCredentials($user, $pass)
    {
        if (filter_var($user, FILTER_VALIDATE_EMAIL)) {
            return Auth::validate(['email' => $user, 'password' => $pass]);
        } else if (is_numeric($user)) {
            return Auth::validate(['phone' => $user, 'password' => $pass]);
        } else {
            return Auth::validate(['username' => $user, 'password' => $pass]);
        }
    }

    public function login(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $userCredentials = $this->userCredentials($request->email, $request->password);
        if (!$userCredentials) {
            return back()->withErrors(['Username or password are incorect!'])->withInput();
        }

        if (Auth::attempt(['username' => $request->email, 'password' => $request->password])) {
            \Session::put('applocale', User::where('username', $request->email)->select('lang')->first());
            if(auth()->user()->hasRole('owner')) {
                return redirect()->route('owner.dashboard', auth()->user()->company->slug);
            } else {
                return redirect()->route('dashboard');
            }
        } elseif (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            \Session::put('applocale', User::where('email', $request->email)->select('lang')->first());
            if(auth()->user()->hasRole('owner')) {
                return redirect()->route('owner.dashboard', auth()->user()->company->slug);
            } else {
                return redirect()->route('dashboard');
            }
        } elseif (Auth::attempt(['phone' => $request->email, 'password' => $request->get('password')])) {
            \Session::put('applocale', User::where('phone', $request->email)->select('lang')->first());
            if(auth()->user()->hasRole('owner')) {
                return redirect()->route('owner.dashboard', auth()->user()->company->slug);
            } else {
                return redirect()->route('dashboard');
            }
        }
    }
}
