<?php

namespace App\Http\Controllers;

use App\Mail\SendTicket;
use App\Mail\SendTicketValidation;
use App\Mail\Ticket;
use App\Mail\ValidateAccountAndTicket;
use App\Models\Revenue;
use App\Models\TicketDetails;
use App\Models\Tickets;
use App\Models\TripRevenue;
use App\Models\Trips;
use App\User;
use function MongoDB\BSON\toJSON;
use PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use JWTAuth;
use Swap\Laravel\Facades\Swap;

class BookingController extends Controller
{
    public function bookTicket(Request $request) {

        // check trip
        $trip = Trips::where('id', $request->trip_id)->first();
        if(!$trip) {
            return response()->json(['message' => 'Destinație necunoscută'], 403);
        }

        // check if user is able
        // to make a booking
        // or a payment
        // by date
        $carbonDepartureTime = $request->date.' '.$trip->departure_time;
        $now = Carbon::now()->setTimezone(config('config.timezones.'.$trip->departure_timezone));
        $bookingTimeLimit = Carbon::createFromFormat('Y-m-d H:i:s', $carbonDepartureTime, config('config.timezones.'.$trip->departure_timezone))
            ->subHours($trip->course->last_hour_payment);
        if($now > $bookingTimeLimit) {
            return response()->json(['message' => 'Rezervarea pentru aceasta cursă nu mai este posibilă'], 401);
        }

        // check if user is authenticated
//        if($request->token) {
//            $user = JWTAuth::parseToken()->authenticate();
//        } else {
//            $user = false;
//        }

        // check if email/user exists
        $user = User::where('email', $request->email)->first();

        if($user) {
            if($request->first_name <> $user->first_name || $request->last_name <> $user->last_name) {
                $firstNameToUse = $request->first_name;
                $lastNameToUse = $request->last_name;
            }
            if($request->phone <> $user->phone) {
                $phoneToUser = $request->phone;
            }

        } else {

            $firstNameToUse = $request->first_name;
            $lastNameToUse = $request->last_name;
            $phoneToUser = $request->phone;

            // validate new user
            $validator = $this->validate($request, [
                'first_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email'
            ]);

            if (isset($validator['errors'])) {
                return response()->json(['message' => $validator->messages()], 200);
            }

            // replace diacritics with normal text
            $firstName = $this->replaceDiacritics($request->first_name);
            $lastName = $this->replaceDiacritics($request->last_name);

            // set up the username
            $username = strtolower($firstName . '.' . $lastName);

            // check to see if already exist in database this username
            $databaseUsernames = DB::table('users')->pluck('username');
            $usernamesArray = [];
            foreach ($databaseUsernames as $name) {
                array_push($usernamesArray, $name);
            }
            if (in_array($username, $usernamesArray)) {
                $randomNumber = rand(1, 50);
                $secondRandomNumber = $randomNumber + 1;
                $username = $username . '.' . $randomNumber;
                // check again to see if the newly created username exist in database
                if (in_array($username, $usernamesArray)) {
                    $username = $username . '.' . $secondRandomNumber;
                }
            }

            $user = new User();
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->first_name = $firstNameToUse;
            $user->last_name = $lastNameToUse;
            $user->username = $username;
            $user->password = bcrypt(config('config.default_password'));
            $user->status = 0;
            $user->save();
        }
        if(!$user->phone AND $request->phone) {
            $user->phone = $request->phone;
            $user->save();
        }
        $ticket = new Tickets();
        $ticket->trip_id = $request->trip_id;
        $ticket->company_id = $trip->course->company_id;
        $ticket->user_id = $user->id;
        $ticket->date = $request->date;
        $ticket->code = $this->makeCode($trip, $request->date);

        // check if user has more than 6 confirmed tickets
        // this means that user si trusted
        if($user->numberOfConfirmedTickets() < 6) {
            $ticket->status = 0;
            $ticket->confirmation_token = str_random(30);
            $okay = $ticket->save();

            if($request->phone AND $request->phone <> $user->phone) {
                $ticketDetails = new TicketDetails();
                $ticketDetails->information_type = config('config.ticketsPaymentInformation.3');
                $ticketDetails->information_value = $request->phone;
                $ticketDetails->ticket_id = $ticket->id;
                $ticketDetails->save();
            }

            Mail::to($user->email)->send(new SendTicketValidation($user, $ticket));
        } else {
            $ticket->status = 1;
            $okay = $ticket->save();

            if($request->phone AND $request->phone <> $user->phone) {
                $ticketDetails = new TicketDetails();
                $ticketDetails->information_type = config('config.ticketsPaymentInformation.3');
                $ticketDetails->information_value = $request->phone;
                $ticketDetails->ticket_id = $ticket->id;
                $ticketDetails->save();
            }

            $pdf = PDF::loadView('emails.tickets.ticket', ['ticket'=>$ticket]);
            $pdf->save('tickets/'.$ticket->code.'.pdf')->stream($ticket->code.'.pdf');
            Mail::to($ticket->user->email)->send(new Ticket($ticket));
        }
        if($okay) {
            return response()->json(["message" => "Biletul a fost rezervat"], 200);
        }

    }

    // Generate unique ticket code
    // Ex: CI 123 1206 34 12
    // CI -> initials of departure and arrival cities
    // 123 -> course indentifier
    // 1206 -> 6 december, date of booking
    // 34* -> trip ID
    // 12 -> 12th booked ticket
    public static function makeCode($trip, $date) {

        $initials = strtoupper(substr($trip->departureCity->slug, 0, 1).
            substr($trip->arrivalCity->slug, 0, 1))
            .$trip->course->identifier;
        $day = Carbon::createFromFormat('Y-m-d', $date)->format('m').Carbon::createFromFormat('Y-m-d', $date)->format('d');
        $counter = Tickets::where('date', $date)->count() + 1;
        ($counter < 10) ? $counter = '0'.$counter : "";
        $code = $initials.$day.$trip->id.$counter;
        return $code;
    }


    public function replaceDiacritics($string_to_replace){
        $transliteration = array(
            'Ĳ' => 'I', 'Ö' => 'O', 'Œ' => 'O', 'Ü' => 'U', 'ä' => 'a', 'æ' => 'a',
            'ĳ' => 'i', 'ö' => 'o', 'œ' => 'o', 'ü' => 'u', 'ß' => 's', 'ſ' => 's',
            'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A',
            'Æ' => 'A', 'Ā' => 'A', 'Ą' => 'A', 'Ă' => 'A', 'Ç' => 'C', 'Ć' => 'C',
            'Č' => 'C', 'Ĉ' => 'C', 'Ċ' => 'C', 'Ď' => 'D', 'Đ' => 'D', 'È' => 'E',
            'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ē' => 'E', 'Ę' => 'E', 'Ě' => 'E',
            'Ĕ' => 'E', 'Ė' => 'E', 'Ĝ' => 'G', 'Ğ' => 'G', 'Ġ' => 'G', 'Ģ' => 'G',
            'Ĥ' => 'H', 'Ħ' => 'H', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
            'Ī' => 'I', 'Ĩ' => 'I', 'Ĭ' => 'I', 'Į' => 'I', 'İ' => 'I', 'Ĵ' => 'J',
            'Ķ' => 'K', 'Ľ' => 'K', 'Ĺ' => 'K', 'Ļ' => 'K', 'Ŀ' => 'K', 'Ł' => 'L',
            'Ñ' => 'N', 'Ń' => 'N', 'Ň' => 'N', 'Ņ' => 'N', 'Ŋ' => 'N', 'Ò' => 'O',
            'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ø' => 'O', 'Ō' => 'O', 'Ő' => 'O',
            'Ŏ' => 'O', 'Ŕ' => 'R', 'Ř' => 'R', 'Ŗ' => 'R', 'Ś' => 'S', 'Ş' => 'S',
            'Ŝ' => 'S', 'Ș' => 'S', 'Š' => 'S', 'Ť' => 'T', 'Ţ' => 'T', 'Ŧ' => 'T',
            'Ț' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ū' => 'U', 'Ů' => 'U',
            'Ű' => 'U', 'Ŭ' => 'U', 'Ũ' => 'U', 'Ų' => 'U', 'Ŵ' => 'W', 'Ŷ' => 'Y',
            'Ÿ' => 'Y', 'Ý' => 'Y', 'Ź' => 'Z', 'Ż' => 'Z', 'Ž' => 'Z', 'à' => 'a',
            'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ā' => 'a', 'ą' => 'a', 'ă' => 'a',
            'å' => 'a', 'ç' => 'c', 'ć' => 'c', 'č' => 'c', 'ĉ' => 'c', 'ċ' => 'c',
            'ď' => 'd', 'đ' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e',
            'ē' => 'e', 'ę' => 'e', 'ě' => 'e', 'ĕ' => 'e', 'ė' => 'e', 'ƒ' => 'f',
            'ĝ' => 'g', 'ğ' => 'g', 'ġ' => 'g', 'ģ' => 'g', 'ĥ' => 'h', 'ħ' => 'h',
            'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ī' => 'i', 'ĩ' => 'i',
            'ĭ' => 'i', 'į' => 'i', 'ı' => 'i', 'ĵ' => 'j', 'ķ' => 'k', 'ĸ' => 'k',
            'ł' => 'l', 'ľ' => 'l', 'ĺ' => 'l', 'ļ' => 'l', 'ŀ' => 'l', 'ñ' => 'n',
            'ń' => 'n', 'ň' => 'n', 'ņ' => 'n', 'ŉ' => 'n', 'ŋ' => 'n', 'ò' => 'o',
            'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ø' => 'o', 'ō' => 'o', 'ő' => 'o',
            'ŏ' => 'o', 'ŕ' => 'r', 'ř' => 'r', 'ŗ' => 'r', 'ś' => 's', 'š' => 's',
            'ť' => 't', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ū' => 'u', 'ů' => 'u',
            'ű' => 'u', 'ŭ' => 'u', 'ũ' => 'u', 'ų' => 'u', 'ŵ' => 'w', 'ÿ' => 'y',
            'ý' => 'y', 'ŷ' => 'y', 'ż' => 'z', 'ź' => 'z', 'ž' => 'z', 'Α' => 'A',
            'Ά' => 'A', 'Ἀ' => 'A', 'Ἁ' => 'A', 'Ἂ' => 'A', 'Ἃ' => 'A', 'Ἄ' => 'A',
            'Ἅ' => 'A', 'Ἆ' => 'A', 'Ἇ' => 'A', 'ᾈ' => 'A', 'ᾉ' => 'A', 'ᾊ' => 'A',
            'ᾋ' => 'A', 'ᾌ' => 'A', 'ᾍ' => 'A', 'ᾎ' => 'A', 'ᾏ' => 'A', 'Ᾰ' => 'A',
            'Ᾱ' => 'A', 'Ὰ' => 'A', 'ᾼ' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D',
            'Ε' => 'E', 'Έ' => 'E', 'Ἐ' => 'E', 'Ἑ' => 'E', 'Ἒ' => 'E', 'Ἓ' => 'E',
            'Ἔ' => 'E', 'Ἕ' => 'E', 'Ὲ' => 'E', 'Ζ' => 'Z', 'Η' => 'I', 'Ή' => 'I',
            'Ἠ' => 'I', 'Ἡ' => 'I', 'Ἢ' => 'I', 'Ἣ' => 'I', 'Ἤ' => 'I', 'Ἥ' => 'I',
            'Ἦ' => 'I', 'Ἧ' => 'I', 'ᾘ' => 'I', 'ᾙ' => 'I', 'ᾚ' => 'I', 'ᾛ' => 'I',
            'ᾜ' => 'I', 'ᾝ' => 'I', 'ᾞ' => 'I', 'ᾟ' => 'I', 'Ὴ' => 'I', 'ῌ' => 'I',
            'Θ' => 'T', 'Ι' => 'I', 'Ί' => 'I', 'Ϊ' => 'I', 'Ἰ' => 'I', 'Ἱ' => 'I',
            'Ἲ' => 'I', 'Ἳ' => 'I', 'Ἴ' => 'I', 'Ἵ' => 'I', 'Ἶ' => 'I', 'Ἷ' => 'I',
            'Ῐ' => 'I', 'Ῑ' => 'I', 'Ὶ' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M',
            'Ν' => 'N', 'Ξ' => 'K', 'Ο' => 'O', 'Ό' => 'O', 'Ὀ' => 'O', 'Ὁ' => 'O',
            'Ὂ' => 'O', 'Ὃ' => 'O', 'Ὄ' => 'O', 'Ὅ' => 'O', 'Ὸ' => 'O', 'Π' => 'P',
            'Ρ' => 'R', 'Ῥ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Ύ' => 'Y',
            'Ϋ' => 'Y', 'Ὑ' => 'Y', 'Ὓ' => 'Y', 'Ὕ' => 'Y', 'Ὗ' => 'Y', 'Ῠ' => 'Y',
            'Ῡ' => 'Y', 'Ὺ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'P', 'Ω' => 'O',
            'Ώ' => 'O', 'Ὠ' => 'O', 'Ὡ' => 'O', 'Ὢ' => 'O', 'Ὣ' => 'O', 'Ὤ' => 'O',
            'Ὥ' => 'O', 'Ὦ' => 'O', 'Ὧ' => 'O', 'ᾨ' => 'O', 'ᾩ' => 'O', 'ᾪ' => 'O',
            'ᾫ' => 'O', 'ᾬ' => 'O', 'ᾭ' => 'O', 'ᾮ' => 'O', 'ᾯ' => 'O', 'Ὼ' => 'O',
            'ῼ' => 'O', 'α' => 'a', 'ά' => 'a', 'ἀ' => 'a', 'ἁ' => 'a', 'ἂ' => 'a',
            'ἃ' => 'a', 'ἄ' => 'a', 'ἅ' => 'a', 'ἆ' => 'a', 'ἇ' => 'a', 'ᾀ' => 'a',
            'ᾁ' => 'a', 'ᾂ' => 'a', 'ᾃ' => 'a', 'ᾄ' => 'a', 'ᾅ' => 'a', 'ᾆ' => 'a',
            'ᾇ' => 'a', 'ὰ' => 'a', 'ᾰ' => 'a', 'ᾱ' => 'a', 'ᾲ' => 'a', 'ᾳ' => 'a',
            'ᾴ' => 'a', 'ᾶ' => 'a', 'ᾷ' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd',
            'ε' => 'e', 'έ' => 'e', 'ἐ' => 'e', 'ἑ' => 'e', 'ἒ' => 'e', 'ἓ' => 'e',
            'ἔ' => 'e', 'ἕ' => 'e', 'ὲ' => 'e', 'ζ' => 'z', 'η' => 'i', 'ή' => 'i',
            'ἠ' => 'i', 'ἡ' => 'i', 'ἢ' => 'i', 'ἣ' => 'i', 'ἤ' => 'i', 'ἥ' => 'i',
            'ἦ' => 'i', 'ἧ' => 'i', 'ᾐ' => 'i', 'ᾑ' => 'i', 'ᾒ' => 'i', 'ᾓ' => 'i',
            'ᾔ' => 'i', 'ᾕ' => 'i', 'ᾖ' => 'i', 'ᾗ' => 'i', 'ὴ' => 'i', 'ῂ' => 'i',
            'ῃ' => 'i', 'ῄ' => 'i', 'ῆ' => 'i', 'ῇ' => 'i', 'θ' => 't', 'ι' => 'i',
            'ί' => 'i', 'ϊ' => 'i', 'ΐ' => 'i', 'ἰ' => 'i', 'ἱ' => 'i', 'ἲ' => 'i',
            'ἳ' => 'i', 'ἴ' => 'i', 'ἵ' => 'i', 'ἶ' => 'i', 'ἷ' => 'i', 'ὶ' => 'i',
            'ῐ' => 'i', 'ῑ' => 'i', 'ῒ' => 'i', 'ῖ' => 'i', 'ῗ' => 'i', 'κ' => 'k',
            'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => 'k', 'ο' => 'o', 'ό' => 'o',
            'ὀ' => 'o', 'ὁ' => 'o', 'ὂ' => 'o', 'ὃ' => 'o', 'ὄ' => 'o', 'ὅ' => 'o',
            'ὸ' => 'o', 'π' => 'p', 'ρ' => 'r', 'ῤ' => 'r', 'ῥ' => 'r', 'σ' => 's',
            'ς' => 's', 'τ' => 't', 'υ' => 'y', 'ύ' => 'y', 'ϋ' => 'y', 'ΰ' => 'y',
            'ὐ' => 'y', 'ὑ' => 'y', 'ὒ' => 'y', 'ὓ' => 'y', 'ὔ' => 'y', 'ὕ' => 'y',
            'ὖ' => 'y', 'ὗ' => 'y', 'ὺ' => 'y', 'ῠ' => 'y', 'ῡ' => 'y', 'ῢ' => 'y',
            'ῦ' => 'y', 'ῧ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'p', 'ω' => 'o',
            'ώ' => 'o', 'ὠ' => 'o', 'ὡ' => 'o', 'ὢ' => 'o', 'ὣ' => 'o', 'ὤ' => 'o',
            'ὥ' => 'o', 'ὦ' => 'o', 'ὧ' => 'o', 'ᾠ' => 'o', 'ᾡ' => 'o', 'ᾢ' => 'o',
            'ᾣ' => 'o', 'ᾤ' => 'o', 'ᾥ' => 'o', 'ᾦ' => 'o', 'ᾧ' => 'o', 'ὼ' => 'o',
            'ῲ' => 'o', 'ῳ' => 'o', 'ῴ' => 'o', 'ῶ' => 'o', 'ῷ' => 'o', 'А' => 'A',
            'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'E',
            'Ж' => 'Z', 'З' => 'Z', 'И' => 'I', 'Й' => 'I', 'К' => 'K', 'Л' => 'L',
            'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S',
            'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'K', 'Ц' => 'T', 'Ч' => 'C',
            'Ш' => 'S', 'Щ' => 'S', 'Ы' => 'Y', 'Э' => 'E', 'Ю' => 'Y', 'Я' => 'Y',
            'а' => 'A', 'б' => 'B', 'в' => 'V', 'г' => 'G', 'д' => 'D', 'е' => 'E',
            'ё' => 'E', 'ж' => 'Z', 'з' => 'Z', 'и' => 'I', 'й' => 'I', 'к' => 'K',
            'л' => 'L', 'м' => 'M', 'н' => 'N', 'о' => 'O', 'п' => 'P', 'р' => 'R',
            'с' => 'S', 'т' => 'T', 'у' => 'U', 'ф' => 'F', 'х' => 'K', 'ц' => 'T',
            'ч' => 'C', 'ш' => 'S', 'щ' => 'S', 'ы' => 'Y', 'э' => 'E', 'ю' => 'Y',
            'я' => 'Y', 'ð' => 'd', 'Ð' => 'D', 'þ' => 't', 'Þ' => 'T', 'ა' => 'a',
            'ბ' => 'b', 'გ' => 'g', 'დ' => 'd', 'ე' => 'e', 'ვ' => 'v', 'ზ' => 'z',
            'თ' => 't', 'ი' => 'i', 'კ' => 'k', 'ლ' => 'l', 'მ' => 'm', 'ნ' => 'n',
            'ო' => 'o', 'პ' => 'p', 'ჟ' => 'z', 'რ' => 'r', 'ს' => 's', 'ტ' => 't',
            'უ' => 'u', 'ფ' => 'p', 'ქ' => 'k', 'ღ' => 'g', 'ყ' => 'q', 'შ' => 's',
            'ჩ' => 'c', 'ც' => 't', 'ძ' => 'd', 'წ' => 't', 'ჭ' => 'c', 'ხ' => 'k',
            'ჯ' => 'j', 'ჰ' => 'h', 'Š' => 'S', 'š' => 's', 'ș' => 's', 'ş' => 's',
            'ă' => 'a', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A',
            'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E',
            'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I',
            'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O',
            'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U',
            'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a', 'â' => 'a',
            'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e',
            'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i',
            'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o',
            'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u',
            'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y', 'ţ' => 't', 'Ţ' => 'T', 'Ț' => 'T',
            'Ș' => 'S', 'Ş' => 'S', 'Â', 'ț' => 't'
        );

        $str = str_replace(array_keys($transliteration), array_values($transliteration), $string_to_replace);

        return $str;
    }


    public function verify($token) {
        $user = User::where('confirmation_token', $token)->first();
        if(!$user) {
            return response()->json(['message' => 'Utilizatorul nu a fost indentificat'], 401);
        }
        $user->confirmation_token = null;
        $user->status = 1;
        $user->save();

        foreach($user->tickets as $ticket) {
            $ticket->status = 1;
            $ticket->save();

            // send ticket
            $pdf = PDF::loadView('emails.tickets.ticket', ['ticket'=>$ticket]);
            $pdf->save('tickets/'.$ticket->code.'.pdf')->stream($ticket->code.'.pdf');
            Mail::to($ticket->user->email)->send(new Ticket($ticket));
        }

        return response()->json(['message' => 'Utilizator validat'], 200);
    }

    // validation for untrusted users
    public function validateTicket($token) {
        $tickets = Tickets::where('confirmation_token', $token)->get();

        foreach($tickets as $ticket) {
            $ticket->status = 1;
            $ticket->confirmation_token = null;
            $ticket->save();

            // send ticket
            $pdf = PDF::loadView('emails.tickets.ticket', ['ticket'=>$ticket])->setPaper('a4');
            $pdf->save('tickets/'.$ticket->code.'.pdf')->stream($ticket->code.'.pdf');
            Mail::to($ticket->user->email)->send(new Ticket($ticket));
        }
        echo "<script>window.close();</script>";
        return false;
        // response()->json(['message' => 'Bilet validat'], 200);
    }


}
