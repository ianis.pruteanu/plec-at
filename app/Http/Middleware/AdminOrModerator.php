<?php

namespace App\Http\Middleware;

use Closure;

class AdminOrModerator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( \Auth::check() && (\Auth::user()->hasRole('admin') || (\Auth::user()->hasRole('moderator'))) )
        {
            return $next($request);
        }

        return redirect()->route('owner.dashboard', auth()->user()->company->slug);
    }
}
