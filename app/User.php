<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Activitylog\Traits\LogsActivity;
use Laravel\Cashier\Billable;
class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use LogsActivity;
    use Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'username', 'status', 'email', 'password',
    ];
    protected static $recordEvents = ['created', 'updated', 'deleted'];
    protected static $logAttributes = ['first_name', 'last_name',  'email', 'phone', 'status'];
    protected static $logOnlyDirty = true;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function company() {
        return $this->hasOne('App\Models\Companies', 'owner_id', 'id');
    }


    public function tickets() {
        return $this->hasMany('App\Models\Tickets', 'user_id', 'id');
    }


    public function numberOfConfirmedTickets() {
        return $this->tickets()->where('status', 2)->orWhere('status', 3)->count();
    }
}
