<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketDetails extends Model
{
    protected $table = "ticket_details";

    public function ticket() {
        return $this->belongsTo('App\Models\Tickets', 'id', 'ticket_id');
    }

}
