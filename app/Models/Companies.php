<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    protected $table = "companies";

    public function legalOrDisplayedName() {
        if($this->displayed_name) {
            return $this->displayed_name;
        } else {
            return $this->legal_name;
        }
    }

    public function owner() {
        return $this->hasOne('App\User', 'id', 'owner_id');
    }

    public function courses() {
        return $this->hasMany('App\Models\Courses', 'company_id', 'id');
    }

    public function buses() {
        return $this->hasMany('App\Models\Buses', 'company_id', 'id');
    }

    public function tickets() {
        return $this->hasMany('App\Models\Tickets', 'company_id', 'id');
    }
}
