<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trips extends Model
{
    protected $table = 'trips';

    public function prices() {
        return $this->hasMany('App\Models\TripPrices', 'trip_id', 'id')->where('status', 1);
    }


    public function firstPrice() {
        return $this->prices()->where('currency', $this->course->main_currency)->where('status', 1)->first();
    }


    public function departureCity() {
        return $this->hasOne('App\Models\Cities', 'id', 'departure_city');
    }


    public function departurePlace() {
        return $this->hasOne('App\Models\Places', 'id', 'departure_place');
    }


    public function arrivalCity() {
        return $this->hasOne('App\Models\Cities', 'id', 'arrival_city');
    }


    public function arrivalPlace() {
        return $this->hasOne('App\Models\Places', 'id', 'arrival_place');
    }


    public function course() {
        return $this->belongsTo('App\Models\Courses', 'course_id', 'id');
    }


    public function revenue() {
        return $this->hasOne('App\Models\TripRevenue', 'trip_id', 'id')->where('status', 1);
    }
}
