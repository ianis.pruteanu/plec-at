<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TripRevenue extends Model
{
    protected $table = 'trip_revenue';

    public function currency() {
        switch ($this->currency) {
            case 1: return 'MDL'; break;
            case 2: return 'RON'; break;
            case 3: return 'RUB'; break;
            case 4: return 'EUR'; break;
            case 5: return 'USD'; break;
        }
    }
}
