<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tickets extends Model
{
    protected $table = 'tickets';

    public function trip() {
        return $this->hasOne('App\Models\Trips', 'id', 'trip_id');
    }


    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }


    public function revenue() {
        return $this->hasOne('App\Models\Revenue', 'ticket_id', 'id');
    }


    public function details() {
        return $this->hasMany('App\Models\TicketDetails', 'ticket_id', 'id');
    }


    public function paidSumm() {
        $summ = $this->details()->where('information_type', 'paid_amount')->pluck('information_value')->first();
        return $summ;
    }


    public function paidCurrency() {
        $currency = $this->details()->where('information_type', 'paid_currency')->pluck('information_value')->first();

        switch ($currency) {
            case 1: return 'MDL'; break;
            case 2: return 'RON'; break;
            case 3: return 'RUB'; break;
            case 4: return 'EUR'; break;
            case 5: return 'USD'; break;
        }
    }


    public function otherName() {
        return false;
    }
}
