<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseExceptions extends Model
{
    protected $table = 'course_exceptions';
}
