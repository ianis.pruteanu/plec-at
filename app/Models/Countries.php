<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    protected $table = 'countries';

    public function cities() {
        return $this->hasMany('App\Models\Cities', 'country_id', 'id');
    }
}
