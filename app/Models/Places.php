<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Places extends Model
{
    protected $table = 'places';

    public function city() {
        return $this->belongsTo('App\Models', 'id','city_id');
    }
}
