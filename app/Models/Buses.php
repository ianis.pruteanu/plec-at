<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Buses extends Model
{
    protected $table = 'buses';

    public function getFullName() {
        $name = mb_strtoupper($this->mark)." ".mb_strtoupper($this->model).", ".mb_strtoupper($this->legal_number);
        return $name;
    }

}
