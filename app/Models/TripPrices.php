<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TripPrices extends Model
{
    protected $table = 'trip_prices';
}
