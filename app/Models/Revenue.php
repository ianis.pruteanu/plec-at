<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Revenue extends Model
{
    protected $table = 'revenue';

    public function ticket() {
        return $this->hasOne('App\Models\Tickets', 'id', 'ticket_id');
    }

    public function currency() {
        switch ($this->currency) {
            case 1: return 'MDL'; break;
            case 2: return 'RON'; break;
            case 3: return 'RUB'; break;
            case 4: return 'EUR'; break;
            case 5: return 'USD'; break;
        }
    }
}
