<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cities extends Model
{
    protected $table = 'cities';

    public function country() {
        $this->belongsTo('App\Models\Countries', 'id', 'country_id');
    }

    public function places() {
        return $this->hasMany('App\Models\Places', 'city_id', 'id');
    }
}
