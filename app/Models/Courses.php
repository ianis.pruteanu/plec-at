<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
    protected $table = 'courses';

    public function company() {
        return $this->hasOne('App\Models\Companies', 'id', 'company_id');
    }

    public function trips() {
        return $this->hasMany('App\Models\Trips', 'course_id', 'id');
    }

    public function departures() {
        return $this->trips()->select('departure_city')->distinct();
    }

    public function departureCity() {
        return $this->hasOne('App\Models\Cities', 'id', 'departure_city');
    }

    public function arrivalCity() {
        return $this->hasOne('App\Models\Cities', 'id', 'arrival_city');
    }

    public function defaultBus() {
        return $this->hasOne('App\Models\Buses', 'id', 'default_bus_id');
    }
}
