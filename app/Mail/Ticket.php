<?php

namespace App\Mail;

use App\Models\Tickets;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Ticket extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Tickets $ticket)
    {
        $this->user = $ticket->user;
        $this->ticket = $ticket;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.tickets.sendTicket')
            ->with([
                'user' => $this->user,
                'ticket' => $this->ticket,
            ])->from('robot@plec.at', 'plec.at')->attach('tickets/'.$this->ticket->code.'.pdf');
    }
}
