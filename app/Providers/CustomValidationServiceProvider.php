<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class CustomValidationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    // frontend controller validations
    public function registerNewUserValidator(array $data)
    {
        if ($this->request->get('email') == '') {
            return Validator::make($data, [
                'first_name'            => 'required|string|max:255',
                'last_name'             => 'required|string|max:255',
                'phone'                 => 'required|numeric|unique:users',
                'password'              => 'required|string|min:6|confirmed',
                'password_confirmation' => 'required|string|min:6',
                'recaptcha_response'    => 'required|recaptcha',
            ]);
        } else {
            return Validator::make($data, [
                'first_name'            => 'required|string|max:255',
                'last_name'             => 'required|string|max:255',
                'email'                 => 'required|string|email|max:255|unique:users,email',
                'password'              => 'required|string|min:6|confirmed',
                'password_confirmation' => 'required|string|min:6',
                'recaptcha_response'    => 'required|recaptcha',
            ]);
        }
    }

    // FRONTEND USER
    public function manageContactsValidator(array $data)
    {
        if (($this->request->email !== '') && ($this->request->phone == '')) {
            return Validator::make($data, [
                'email' => 'email|unique:users,email,' . $this->request->userId,
            ]);
        } elseif (($this->request->email == '') && ($this->request->phone !== '')) {
            return Validator::make($data, [
                'phone' => 'numeric|unique:users,phone,' . $this->request->userId,
            ]);
        } elseif (($this->request->email !== '') && ($this->request->phone !== '')) {
            return Validator::make($data, [
                'email' => 'email|unique:users,email,' . $this->request->userId,
                'phone' => 'numeric|unique:users,phone,' . $this->request->userId,
            ]);
        } elseif ($this->request->phone !== '') {
            return Validator::make($data, [
                'email' => 'required_without_all:phone|unique:users,email,' . $this->request->userId,
            ]);
        } elseif ($this->reqeust->email !== '') {
            return Validator::make($data, [
                'phone' => 'required_without_all:email|unique:users,phone,' . $this->request->userId,
            ]);
        }
    }


    public function passwordValidator(array $data)
    {
        return Validator::make($data, [
            'actual_password'  => 'required',
            'new_password'     => 'required|min:6',
            'confirm_password' => 'required|min:6|same:new_password',
        ]);
    }


    public function userImageValidator(array $data)
    {
        return Validator::make($data, [
            'userImage' => 'required|max:2048',
        ]);
    }

    public function backendPersonalInfoValidator(array $data)
    {
        if (($this->request->email !== '') && ($this->request->phone == '')) {
            return Validator::make($data, [
                'first_name'            => 'required|string|max:255',
                'last_name'             => 'required|string|max:255',
                'email' => 'email|unique:users,email,' . \Auth::user()->id,
            ]);
        } elseif (($this->request->email == '') && ($this->request->phone !== '')) {
            return Validator::make($data, [
                'first_name'            => 'required|string|max:255',
                'last_name'             => 'required|string|max:255',
                'phone' => 'numeric|unique:users,phone,' . \Auth::user()->id,
            ]);
        } elseif (($this->request->email !== '') && ($this->request->phone !== '')) {
            return Validator::make($data, [
                'first_name'            => 'required|string|max:255',
                'last_name'             => 'required|string|max:255',
                'email' => 'email|unique:users,email,' . \Auth::user()->id,
                'phone' => 'numeric|unique:users,phone,' . \Auth::user()->id,
            ]);
        } elseif ($this->request->phone !== '') {
            return Validator::make($data, [
                'first_name'            => 'required|string|max:255',
                'last_name'             => 'required|string|max:255',
                'email' => 'required_without_all:phone|unique:users,email,' . \Auth::user()->id,
            ]);
        } elseif ($this->reqeust->email !== '') {
            return Validator::make($data, [
                'first_name'            => 'required|string|max:255',
                'last_name'             => 'required|string|max:255',
                'phone' => 'required_without_all:email|unique:users,phone,' . \Auth::user()->id,
            ]);
        }
    }
}
