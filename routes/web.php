<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function() {
    return redirect()->route('dashboard');
});
Route::get('login', 'DashboardController@login')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::middleware(['web', 'auth'])->prefix('dashboard')->group(function () {


    Route::middleware(['adminOrModerator'])->group(function () {
        Route::get('/', 'DashboardController@dashboard')->name('dashboard');
        Route::get('exchange/currency/{currency}', 'DashboardController@exchange')->name('dashboard.exchange');
        Route::get('calculate', 'DashboardController@calculateRevenue')->name('dashboard.calculate');
        Route::get('users', 'UserController@list')->name('users.list');
        Route::get('users/create', 'UserController@create')->name('users.create');
        Route::post('users/store', 'UserController@store')->name('users.store');
        Route::get('user/profile/{id}', 'UserController@profile')->name('user.profile');
        Route::get('user/{id}/edit', 'UserController@edit')->name('user.edit');
        Route::post('user/save', 'UserController@save')->name('user.save');
        Route::post('user/resetPassword', 'UserController@resetPassword')->name('user.resetPassword');
        Route::get('user/{id}/delete', 'UserController@delete')->name('user.delete');

        // settings
        Route::get('settings/cities', 'SettingsController@cities')->name('settings.cities');
        Route::post('settings/cities/add', 'SettingsController@addCity')->name('settings.addCity');
        Route::get('settings/places', 'SettingsController@places')->name('settings.places');
        Route::post('settings/place/add', 'SettingsController@addLocation')->name('settings.addLocation');
        Route::get('settings/places/{city}', 'SettingsController@cityPlaces')->name('settings.cityPlaces');
        Route::get('settings/roles', 'SettingsController@listRoles')->name('settings.userRoles');
        Route::get('settings/role/{role}', 'SettingsController@listRoles')->name('settings.userRolePermissions');
        Route::post('settings/addRole', 'SettingsController@addRole')->name('settings.addRole');
        Route::post('settings/addPermission', 'SettingsController@addPermission')->name('settings.addPermission');
        Route::post('settings/changeRolePermission/{sw}/{role}/{permission}', 'SettingsController@changeRolePermission')->name('settings.changeRolePermission');
    });
    // user profile data
    Route::get('user/profile/{id}', 'UserController@profile')->name('user.profile');
    Route::get('user/{id}/edit', 'UserController@edit')->name('user.edit');
    Route::get('user/profile/{id}/settings', 'UserController@settings')->name('user.settings');
    Route::post('user/profile/info/{id}', 'UserController@personalInfo')->name('user.profile.info');
    Route::post('user/profile/picture/{id}', 'UserController@personalPicture')->name('user.profile.picture');
    Route::post('user/profile/password/{id}', 'UserController@personalPassword')->name('user.profile.password');

    // companies
    Route::get('companies', 'CompanyController@list')->name('companies.list');
    Route::get('company/create', 'CompanyController@create')->name('company.create');
    Route::get('company/{slug}', 'CompanyController@show')->name('company.show');
    Route::get('company/{slug}/edit', 'CompanyController@edit')->name('company.edit');
    Route::post('company/store', 'CompanyController@store')->name('company.store');
    Route::post('company/save','CompanyController@save')->name('company.save');
    Route::get('/company/getCourses/{company}', 'CompanyController@getCourses')->name('company.getCourses');

    // buses
    Route::get('buses', 'BusController@list')->name('buses.list');
    Route::get('buses/getModels/{mark}', 'BusController@getModels')->name('buses.getModels');
    Route::get('bus/create', 'BusController@create')->name('bus.create');
    Route::get('bus/{slug}', 'BusController@show')->name('bus.show');
    Route::get('bus/{slug}/edit', 'BusController@edit')->name('bus.edit');
    Route::post('bus/store', 'BusController@store')->name('bus.store');
    Route::post('bus/save','BusController@save')->name('bus.save');

    // courses
    Route::post('course/{id}/setDefaultLogo', 'CourseController@setDefaultLogo')->name('course.setDefaultLogo');
    Route::get('courses', 'CourseController@list')->name('courses.list');
    Route::get('courses/getPlaces/{id}', 'CourseController@getPlaces')->name('courses.getPlaces');
    Route::get('course/create', 'CourseController@create')->name('course.create');
    Route::post('course/store', 'CourseController@store')->name('course.store');
    Route::get('course/{slug}', 'CourseController@show')->name('course.show');
    Route::get('course/{slug}/edit', 'CourseController@edit')->name('course.edit');
    Route::post('course/save', 'CourseController@save')->name('course.save');

    // trips
    Route::post('addNewPrice', 'TripController@addNewPrice')->name('trip.addNewPrice');
    Route::post('trip/create', 'TripController@create')->name('trip.create');
    Route::post('trip/updatePrices', 'TripController@updatePrices')->name('trip.updatePrices');
    Route::post('trip/updateCommission', 'TripController@updateCommission')->name('trip.updateCommission');
    Route::post('trip/closePrice/{id}', 'TripController@closePrice')->name('trip.closePrice');
    Route::get('course/{slug}/trip/{id}', 'TripController@show')->name('trip.show');
    Route::post('trip/save', 'TripController@save')->name('trip.save');

    // tickets
    Route::get('tickets', 'TicketController@list')->name('tickets.list');
    Route::get('ticket/{code}', 'TicketController@show')->name('ticket.show');
    Route::any('ticket/{id}/confirm', 'TicketController@confirm')->name('ticket.confirm');
    Route::any('ticket/{id}/download', 'TicketController@download')->name('ticket.download');

    // filter tickets
    Route::get('getCompanyTickets/{company?}/{course?}/{date?}', 'TicketController@filter')->name('tickets.filter');
    Route::get('getTicketsByDate/{date?}', 'TicketController@filterByDate')->name('tickets.filterByDate');


    Route::middleware(['owner'])->prefix('{company}')->group(function () {
        Route::get('/', 'OwnerController@dashboard')->name('owner.dashboard');
        Route::get('/tickets/{id?}', 'OwnerController@tickets')->name('owner.tickets');
        Route::get('/courses', 'OwnerController@courses')->name('owner.courses');
        Route::get('/settings', 'OwnerController@settings')->name('owner.settings');
    });
});
