<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/allDestinations', 'ApiController@getAllDestinations');
Route::get('/departures', 'ApiController@getDepartures');
Route::get('/destinations/{departureId}', 'ApiController@getDestinations');
Route::post('/getTrips', 'ApiController@getTrips');

Route::post('/register', 'ApiController@register');
Route::post('/login', 'ApiController@login');
Route::post('/logout', 'ApiController@logout');

Route::middleware(['jwt.auth'])->group(function() {
    Route::get('user', 'ApiController@getUserProfile');
    Route::get('user/tickets', 'ApiController@getUserTickets');
    Route::post('user/changePassword', 'ApiController@changePassword');
    Route::post('user/updateContacts', 'ApiController@updateContacts');
});
Route::post('/bookTicket', 'BookingController@bookTicket');
Route::get('/verify/{token}', 'BookingController@verify');
Route::get('/ticket/validate/{token}', 'BookingController@validateTicket');
Route::any('ticket/{id}/download', 'TicketController@download');
