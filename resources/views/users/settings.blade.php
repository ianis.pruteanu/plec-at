@extends('backend/backend')

@section('content')
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">
			<h1>{{ $user->first_name }} {{ $user->last_name }} | @lang('dashboard.settings')
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
	<!-- END PAGE HEAD-->
	<!-- BEGIN PAGE BREADCRUMB -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ route('dashboard') }}">@lang('main.dashboard')</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="{{ route('user.profile', $user->id) }}">@lang('dashboard.my_profile')</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span class="active">@lang('dashboard.settings')</span>
		</li>
	</ul>
	<!-- END PAGE BREADCRUMB -->
	<!-- BEGIN PAGE BASE CONTENT -->
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN PROFILE SIDEBAR -->
			<div class="profile-sidebar">
				<!-- PORTLET MAIN -->
				<div class="portlet light profile-sidebar-portlet bordered">
					<!-- SIDEBAR USERPIC -->
					<div class="profile-userpic">
						<img src="{{ asset($user->pic) }}" class="img-responsive" alt="">
					</div>
					<!-- END SIDEBAR USERPIC -->
					<!-- SIDEBAR USER TITLE -->
					<div class="profile-usertitle">
						<div class="profile-usertitle-name"> {{ $user->first_name }} {{ $user->last_name }}</div>
						<div class="profile-usertitle-job"> Developer </div>
					</div>
					<!-- END SIDEBAR USER TITLE -->
					<!-- SIDEBAR BUTTONS -->
					<div class="profile-userbuttons">
						<button type="button" class="btn btn-circle green btn-sm">Follow</button>
						<button type="button" class="btn btn-circle red btn-sm">Message</button>
					</div>
					<!-- END SIDEBAR BUTTONS -->
					<!-- SIDEBAR MENU -->
					<div class="profile-usermenu">
						<ul class="nav">
							<li>
								<a href="{{ route('user.profile', $user->id) }}">
									<i class="icon-home"></i> @lang('dashboard.my_profile') </a>
							</li>
							<li class="active">
								<a href="{{ route('user.settings', $user->id) }}">
									<i class="icon-settings"></i>@lang('dashboard.settings') </a>
							</li>
							<li>
								<a href="page_user_profile_1_help.html">
									<i class="icon-info"></i> Help </a>
							</li>
						</ul>
					</div>
					<!-- END MENU -->
				</div>
				<!-- END PORTLET MAIN -->
				<!-- PORTLET MAIN -->
				<div class="portlet light bordered">
					<!-- STAT -->
					<div class="row list-separated profile-stat">
						<div class="col-md-4 col-sm-4 col-xs-6">
							<div class="uppercase profile-stat-title"> 37 </div>
							<div class="uppercase profile-stat-text"> Projects </div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6">
							<div class="uppercase profile-stat-title"> 51 </div>
							<div class="uppercase profile-stat-text"> Tasks </div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6">
							<div class="uppercase profile-stat-title"> 61 </div>
							<div class="uppercase profile-stat-text"> Uploads </div>
						</div>
					</div>
					<!-- END STAT -->
					<div>
						<h4 class="profile-desc-title">About Marcus Doe</h4>
						<span class="profile-desc-text"> Lorem ipsum dolor sit amet diam nonummy nibh dolore. </span>
						<div class="margin-top-20 profile-desc-link">
							<i class="fa fa-globe"></i>
							<a href="http://www.keenthemes.com">www.keenthemes.com</a>
						</div>
						<div class="margin-top-20 profile-desc-link">
							<i class="fa fa-twitter"></i>
							<a href="http://www.twitter.com/keenthemes/">@keenthemes</a>
						</div>
						<div class="margin-top-20 profile-desc-link">
							<i class="fa fa-facebook"></i>
							<a href="http://www.facebook.com/keenthemes/">keenthemes</a>
						</div>
					</div>
				</div>
				<!-- END PORTLET MAIN -->
			</div>
			<!-- END BEGIN PROFILE SIDEBAR -->
			<!-- BEGIN PROFILE CONTENT -->
			<div class="profile-content">
				<div class="row">
					<div class="col-md-12">
						<div class="portlet light bordered">
						<div class="backendAlertMessages">
							@if ($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
							<div class="portlet-title tabbable-line">
								<div class="caption caption-md">
									<i class="icon-globe theme-font hide"></i>
									<span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
								</div>
								<ul class="nav nav-tabs">
									<li class="active">
										<a href="#tab_1_1" data-toggle="tab">Personal Info</a>
									</li>
									<li>
										<a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
									</li>
									<li>
										<a href="#tab_1_3" data-toggle="tab">Change Password</a>
									</li>
									<li>
										<a href="#tab_1_4" data-toggle="tab">Privacy Settings</a>
									</li>
								</ul>
							</div>
							<div class="portlet-body">
								<div class="tab-content">
									<!-- PERSONAL INFO TAB -->
									<div class="tab-pane active" id="tab_1_1">
										<form action="{{ route('user.profile.info', Auth::user()->id) }}" method="POST">
											{{ csrf_field() }}
											<div class="form-group">
												<label class="control-label">@lang('main.first_name') <span class="hm_requires_star">*</span></label>
												<input name="first_name" type="text" value="{{ $user->first_name }}" class="form-control"
												/> </div>
											<div class="form-group">
												<label class="control-label">@lang('main.last_name') <span class="hm_requires_star">*</span></label>
												<input name="last_name" type="text" value="{{ $user->last_name }}" class="form-control"
												/> </div>
											<div class="form-group">
												<label class="control-label">@lang('main.phone')</label>
												<input name="phone" type="text" value="{{ $user->phone }}" class="form-control" />	</div>
											<div class="form-group">
												<label class="control-label">Email</label>
												<input name="email" type="text" value="{{ $user->email }}" class="form-control" />
											</div>
											<div class="margiv-top-10">
												<button type="submit" class="btn green">Save Changes</button>
												<button type="reset" class="btn default">Cancel</button>
											</div>
										</form>
									</div>
									<!-- END PERSONAL INFO TAB -->
									<!-- CHANGE AVATAR TAB -->
									<div class="tab-pane" id="tab_1_2">
										<p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
											eiusmod. </p>
										<form action="{{ route('user.profile.picture', Auth::user()->id) }}" id="changeBackendUserProfileImage" method="POST" enctype="multipart/form-data">
											{{ csrf_field() }}
											<div class="form-group">
												<div class="fileinput fileinput-new" data-provides="fileinput">
													<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
														<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
													<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
													<div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="userImage"> </span>
														<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="userImage"> Remove </a>
													</div>
												</div>
												<div class="clearfix margin-top-10">
													<span class="label label-danger">NOTE! </span>
													<span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
												</div>
											</div>
											<div class="margin-top-10">
												<button type="submit" id="createBackendUserSubmitButton" class="btn green">Change Avatar</button>
												<button type="reset" class="btn default">Cancel</button>
											</div>
										</form>
									</div>
									<!-- END CHANGE AVATAR TAB -->
									<!-- CHANGE PASSWORD TAB -->
									<div class="tab-pane" id="tab_1_3">
										<form action="{{ route('user.profile.password', Auth::user()->id) }}" method="POST">
											{{ csrf_field() }}
											<div class="form-group">
												<label class="control-label">Current Password</label>
												<input name="actual_password" type="password" class="form-control" /> </div>
											<div class="form-group">
												<label class="control-label">New Password</label>
												<input name="new_password" type="password" class="form-control" /> </div>
											<div class="form-group">
												<label class="control-label">Re-type New Password</label>
												<input name="confirm_password" type="password" class="form-control" /> </div>
											<div class="margin-top-10">
												<button type="submit" class="btn green">Change Password</button>
												<button type="reset" class="btn default">Cancel</button>
											</div>
										</form>
									</div>
									<!-- END CHANGE PASSWORD TAB -->
									<!-- PRIVACY SETTINGS TAB -->
									<div class="tab-pane" id="tab_1_4">
										<form action="#">
											<table class="table table-light table-hover">
												<tr>
													<td> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus.. </td>
													<td>
														<div class="mt-radio-inline">
															<label class="mt-radio">
																<input type="radio" name="optionsRadios1" value="option1" /> Yes
																<span></span>
															</label>
															<label class="mt-radio">
																<input type="radio" name="optionsRadios1" value="option2" checked/> No
																<span></span>
															</label>
														</div>
													</td>
												</tr>
												<tr>
													<td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
													<td>
														<div class="mt-radio-inline">
															<label class="mt-radio">
																<input type="radio" name="optionsRadios11" value="option1" /> Yes
																<span></span>
															</label>
															<label class="mt-radio">
																<input type="radio" name="optionsRadios11" value="option2" checked/> No
																<span></span>
															</label>
														</div>
													</td>
												</tr>
												<tr>
													<td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
													<td>
														<div class="mt-radio-inline">
															<label class="mt-radio">
																<input type="radio" name="optionsRadios21" value="option1" /> Yes
																<span></span>
															</label>
															<label class="mt-radio">
																<input type="radio" name="optionsRadios21" value="option2" checked/> No
																<span></span>
															</label>
														</div>
													</td>
												</tr>
												<tr>
													<td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
													<td>
														<div class="mt-radio-inline">
															<label class="mt-radio">
																<input type="radio" name="optionsRadios31" value="option1" /> Yes
																<span></span>
															</label>
															<label class="mt-radio">
																<input type="radio" name="optionsRadios31" value="option2" checked/> No
																<span></span>
															</label>
														</div>
													</td>
												</tr>
											</table>
											<!--end profile-settings-->
											<div class="margin-top-10">
												<a href="javascript:;" class="btn red"> Save Changes </a>
												<a href="javascript:;" class="btn default"> Cancel </a>
											</div>
										</form>
									</div>
									<!-- END PRIVACY SETTINGS TAB -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PROFILE CONTENT -->
		</div>
	</div>
@endsection