@extends('backend/backend')

@section('content')

    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>@lang('dashboard.users_management')
                <small>@lang('dashboard.manage_it_good')</small>
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">@lang('main.dashboard')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">@lang('dashboard.users_management')</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    {{-- @include('flash::message') --}}
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-users font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">@lang('dashboard.users_list')</span>
                    </div>
                    <div class="actions">
                        <a href="{{ route('users.create') }}" class="btn btn-cons blue"><i class="fa fa-user-plus"></i>
                            @lang('dashboard.create_user')</a>
                        {{--<div class="btn-group">
                            <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                                <i class="fa fa-share"></i>
                                <span class="hidden-xs"> Trigger Tools </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right" id="sample_3_tools">
                                <li>
                                    <a href="javascript:;" data-action="0" class="tool-action">
                                        <i class="icon-printer"></i> Print</a>
                                </li>
                                <li>
                                    <a href="javascript:;" data-action="1" class="tool-action">
                                        <i class="icon-check"></i> Copy</a>
                                </li>
                                <li>
                                    <a href="javascript:;" data-action="2" class="tool-action">
                                        <i class="icon-doc"></i> PDF</a>
                                </li>
                                <li>
                                    <a href="javascript:;" data-action="3" class="tool-action">
                                        <i class="icon-paper-clip"></i> Excel</a>
                                </li>
                                <li>
                                    <a href="javascript:;" data-action="4" class="tool-action">
                                        <i class="icon-cloud-upload"></i> CSV</a>
                                </li>
                                <li class="divider"> </li>
                                <li>
                                    <a href="javascript:;" data-action="5" class="tool-action">
                                        <i class="icon-refresh"></i> Reload</a>
                                </li>
                            </ul>
                        </div>--}}
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <table class="table table-striped table-bordered table-hover" id="sample_3">
                            <thead>
                            <tr>
                                <th> @lang('dashboard.user_role') </th>
                                <th> @lang('main.first_name') </th>
                                <th> @lang('main.last_name') </th>
                                <th> @lang('main.phone') </th>
                                <th style="width: 150px"> @lang('main.actions') </th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>
                                        @foreach($user->roles->pluck('name') as $role)

                                            @if($role === 'admin')
                                                <span class="label label-danger"> @lang('main.'.$role)</span>
                                            @elseif($role === 'moderator')
                                                <span class="label label-info"> @lang('main.'.$role)</span>
                                            @elseif($role === 'owner')
                                                <span class="label label-warning"> @lang('main.'.$role)</span>
                                            @elseif($role === 'driver')
                                                <span class="label label-default"> @lang('main.'.$role)</span>
                                            @elseif($role === 'user')
                                                <span class="label label-warning"> @lang('main.'.$role)</span>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td> {{ $user->first_name }} </td>
                                    <td> {{ $user->last_name }} </td>
                                    <td> {{ $user->phone }} </td>
                                    <td>
                                        <div class="btn-group btn-group-xs btn-group-solid">
                                            <a href="{{ route('user.profile', $user->id) }}" class="btn btn-cons grey">
                                                <i class="icon-user"></i> @lang('dashboard.profile')</a>
                                            <a href="{{ route('user.edit', $user->id) }}" class="btn btn-cons green">
                                                <i class="icon-pencil"></i> @lang('main.edit')</a>
                                            @role('admin')
                                            <a href="{{ route('user.delete', $user->id) }}" class="btn btn-cons red">
                                                <i class="fa fa-remove"></i> @lang('main.delete')</a>
                                            @endrole
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End: life time stats -->
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->


@endsection