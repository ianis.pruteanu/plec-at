@extends('backend/backend')

@section('content')
	<!-- BEGIN PAGE HEAD-->
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">
			<h1>@lang('dashboard.edit_user')</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
	<!-- END PAGE HEAD-->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ route('dashboard') }}">@lang('main.dashboard')</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="{{ route('users.list') }}">@lang('dashboard.users_management')</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span class="active">@lang('dashboard.create_user')</span>
		</li>
	</ul>
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN VALIDATION STATES-->
			<div class="portlet light portlet-fit portlet-form bordered">
				<div class="portlet-body">
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<!-- BEGIN FORM-->
					<form action="{{ route('user.save') }}" method="POST">
						{{ csrf_field() }}
						<input type="hidden" class="form-control" name="id" value="{{ $user->id }}">
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" name="first_name" id="first_name"
									value="{{ $user->first_name }}">
								<label for="first_name">@lang('main.first_name')
									<span class="required">*</span>
								</label>
							</div>
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" name="last_name" id="last_name"
									value="{{ $user->last_name }}">
								<label for="last_name">@lang('main.last_name')
									<span class="required">*</span>
								</label>
							</div>
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" id="email" name="email"
								       value="{{ $user->email }}">
								<label for="email">@lang('main.email')
									{{-- <span class="required">*</span> --}}
								</label>
							</div>
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" id="phone" name="phone"
								       value="{{ $user->phone }}">
								<label for="phone">@lang('main.phone')</label>
							</div>
							<div class="form-group form-md-line-input">
								<select class="form-control" name="role" id="role">
									<option value="user" @if($user->hasanyrole('user'))
										selected="selected" @endif>@lang('main.user')</option>
									<option value="driver" @if($user->hasanyrole('driver'))
										selected="selected" @endif>@lang('main.driver')</option>
									<option value="owner" @if($user->hasanyrole('owner'))
										selected="selected" @endif>@lang('main.owner')</option>
									<option value="moderator" @if($user->hasanyrole('moderator'))
										selected="selected" @endif>@lang('main.moderator')</option>
									<option value="admin" @if($user->hasanyrole('admin'))
									 selected="selected" @endif>@lang('main.admin')</option>
								</select>
								<label for="role">@lang('dashboard.user_role')</label>
							</div>
							<div class="form-group form-md-checkboxes">
								<div class="md-checkbox-list">
									<div class="md-checkbox">
										<input type="checkbox" id="active" name="active" value="1" class="md-check" {{$user->status ? 'checked' : ''}}>
										<label for="active">
											<span class="inc"></span>
											<span class="check"></span>
											<span class="box"></span> @lang('main.active') </label>
									</div>
								</div>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<button type="submit" class="btn green">Validate</button>
									<button type="reset" class="btn default">Reset</button>
								</div>
							</div>
						</div>
					</form>
					<!-- END FORM-->
				</div>
			</div>
			<!-- END VALIDATION STATES-->
		</div>
	</div>
@endsection