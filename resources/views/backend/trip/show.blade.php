@extends('backend/backend')

@section('content')
	<link href="{{ asset('backend/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet"
	type="text/css" />
	<link href="{{ asset('backend/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('backend/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- BEGIN PAGE HEAD-->
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">
			<h1>@lang('geography.'.$trip->departureCity->slug) - @lang('geography.'.$trip->arrivalCity->slug)
				<small>{{ $trip->course->name }}</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
	<!-- END PAGE HEAD-->
	<!-- BEGIN PAGE BREADCRUMB -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ route('dashboard') }}">@lang('main.dashboard')</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="{{ route('course.show', $trip->course->slug) }}">{{ $trip->course->name }}</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span class="active">@lang('dashboard.trip')</span>
		</li>
	</ul>
	<!-- END PAGE BREADCRUMB -->
	<!-- BEGIN PAGE BASE CONTENT -->
	<div class="row">
		<div class="col-md-6">
			<div class="portlet light portlet-fit portlet-form bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-money"></i>
						<span class="caption-subject bold uppercase"> @lang('dashboard.prices')</span>
						<span class="caption-helper"></span>
					</div>
				</div>
				<div class="portlet-body">
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<form action="{{ route('trip.updatePrices') }}" method="post" role="form" enctype="multipart/form-data">
						{{ csrf_field() }}
						<input type="hidden" name="trip_id" value="{{ $trip->id }}" />
						<div class="form-body">
							@foreach($trip->prices as $price)
								<input type="hidden" value="{{ $price->id }}" name="price_id[]" />
								<div class="row">
									<div class="col-md-5">
										<div class="form-group">
											<label>@lang('main.currency')</label>
											<select class="form-control" name="currency[]" readonly="">
												@foreach(config('config.currencies') as $key => $currency)
													<option value="{{ $key }}"
														@if($price->currency == $key) selected="selected" @endif>{{ $currency }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>@lang('main.price_amount')</label>
											<input type="number" name="amount[]" value="{{ $price->amount }}" class="form-control" />
										</div>
									</div>
									<div class="col-md-1">
										<label>&nbsp;</label>
										<button type="button" class="btn btn-sm btn-cogs red closeTripPrice" data-id="{{ $price->id }}">
											<i class="fa fa-remove"></i></button>
									</div>
								</div>
							@endforeach
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="hidden" value="" name="price_id[]" />
										<label>@lang('main.currency')</label>
										<select class="form-control" name="currency[]">
											@foreach(config('config.currencies') as $key => $currency)
												@if(!in_array($key, $exist_currencies))
													<option value="{{ $key }}">{{ $currency }}</option>
												@endif
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>@lang('main.price_amount')</label>
										<input type="number" name="amount[]" class="form-control" />
									</div>
								</div>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<button type="submit" class="btn green">@lang('main.save')</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="portlet light portlet-fit portlet-form bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-money"></i>
						<span class="caption-subject bold uppercase"> @lang('dashboard.commission')</span>
						<span class="caption-helper"></span>
					</div>
				</div>
				<div class="portlet-body">
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<form @if(auth()->user()->hasRole('admin')) action="{{ route('trip.updateCommission') }}" @endif method="post" role="form"
					      enctype="multipart/form-data">
						{{ csrf_field() }}
						<input type="hidden" name="trip_id" value="{{ $trip->id }}" />
						<div class="form-body">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>@lang('dashboard.type')</label>
										<select class="form-control" name="type" @if(!auth()->user()->hasRole('admin')) disabled @endif>
											<option>@lang('main.select')</option>
											@foreach(config('config.commissions') as $key => $commission)
												<option @if($trip->revenue AND $trip->revenue->type == $key) selected @endif value="{{ $key }}">{{ $commission
												}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>@lang('dashboard.rate')</label>
										<input type="number" name="rate" @if($trip->revenue) value="{{ $trip->revenue->amount }}" @endif class="form-control"
										       @if(!auth()->user()->hasRole('admin')) disabled @endif/>
									</div>
								</div>
								<div class="col-md-12">

									<div class="form-group">
										@if($trip->revenue->type == 2 OR auth()->user()->hasRole('admin'))
										<label>@lang('main.currency')</label>
										<select class="form-control" name="currency" @if(!auth()->user()->hasRole('admin')) disabled @endif>
											@foreach(config('config.currencies') as $key => $currency)
												<option @if($trip->revenue AND $trip->revenue->currency == $key) selected @endif value="{{ $key }}">
													{{ $currency }}</option>
											@endforeach
										</select>
										@else
											<br />
											<br />
											<br />
										@endif
									</div>

								</div>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									@if(auth()->user()->hasRole('admin'))
										<button type="submit" class="btn green">@lang('main.save')</button>
									@else
										<button disabled class="btn green">@lang('main.save')</button>
									@endif
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN VALIDATION STATES-->
			<div class="portlet light portlet-fit portlet-form bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-map-signs"></i>
						<span class="caption-subject bold uppercase"> @lang('dashboard.main')</span>
						<span class="caption-helper"></span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="backendAlertMessages">
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					</div>
					<!-- BEGIN FORM-->
					<form action="{{ route('trip.save') }}" method="POST">
						{{ csrf_field() }}
						<input type="hidden" name="trip_id" value="{{ $trip->id }}">
						<div class="form-body">
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label for="departureCity" class="control-label">@lang('main.departure_city')</label>
										<select name="departure_city" class="form-control select2 getPlaces">
											<option></option>
											@foreach($countries as $country)
												<optgroup label="@lang('geography.'.$country->slug)">
													@foreach($country->cities as $city)
														<option value="{{ $city->id }}" @if($trip->departure_city == $city->id) selected @endif>
															@lang('geography.'.$city->slug)
														</option>
													@endforeach
												</optgroup>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label for="departurePlace" class="control-label">@lang('main.departure_place')</label>
										<select id="departurePlace" name="departure_place" class="form-control select2">
											<option></option>
											@foreach($trip->departureCity->places as $place)
												<option value="{{ $place->id }}" @if($trip->departure_place == $place->id) selected @endif>
													{{ $place->name }}
												</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>@lang('main.departure_time')</label>
										<div class="input-group">
											<input type="text" name="departure_time"
											value="{{ \Carbon\Carbon::parse($trip->departure_time)->format('H:i') }}"
											class="form-control timepicker	timepicker-24">
											<span class="input-group-btn">
		                                        <button class="btn default" type="button">
		                                            <i class="fa fa-clock-o"></i>
		                                        </button>
		                                    </span>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>@lang('dashboard.timezone')</label>
										<select class="form-control" name="departure_timezone">
											@foreach(config('config.timezones') as $key => $tz)
												<option value="{{ $key }}"
											        @if($trip->departure_timezone == $key) selected @endif>{{ $tz }}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label for="arrivalCity" class="control-label">@lang('main.arrival_city')</label>
										<select name="arrival_city" class="form-control select2 getPlaces">
											<option></option>
											@foreach($countries as $country)
												<optgroup label="@lang('geography.'.$country->slug)">
													@foreach($country->cities as $city)
														<option value="{{ $city->id }}"  @if($trip->arrival_city == $city->id) selected @endif>
															@lang('geography.'.$city->slug)</option>
													@endforeach
												</optgroup>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label for="arrivalPlace" class="control-label">@lang('main.arrival_place')</label>
										<select id="arrivalPlace" name="arrival_place" class="form-control select2">
											<option></option>
											@foreach($trip->arrivalCity->places as $place)
												<option value="{{ $place->id }}" @if($trip->arrival_place == $place->id) selected @endif>
													{{ $place->name }}
												</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>@lang('main.arrival_time')</label>
										<div class="input-group">
											<input type="text" name="arrival_time"
											value="{{ \Carbon\Carbon::parse($trip->arrival_time)->format('H:i') }}"
											 class="form-control timepicker	timepicker-24">
											<span class="input-group-btn">
		                                        <button class="btn default" type="button">
		                                            <i class="fa fa-clock-o"></i>
		                                        </button>
		                                    </span>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label>@lang('dashboard.timezone')</label>
										<select class="form-control" name="arrival_timezone">
											@foreach(config('config.timezones') as $key => $tz)
											<option value="{{ $key }}"
												@if($trip->arrival_timezone == $key) selected @endif>{{ $tz }}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<div class="form-group form-md-line-input">
										<select class="form-control" name="duration" id="status">
											<option value="0" @if($trip->duration == 0)
											selected="selected" @endif>@lang('dashboard.same_day')</option>
											<option value="1" @if($trip->duration == 1)
											selected="selected" @endif>@lang('dashboard.next_day')</option>
											<option value="2" @if($trip->duration == 2)
											selected="selected" @endif>@lang('dashboard.3rd_day')</option>
											<option value="3" @if($trip->duration == 3)
											selected="selected" @endif>@lang('dashboard.4rd_day')</option>
											<option value="4" @if($trip->duration == 4)
											selected="selected" @endif>@lang('dashboard.5rd_day')</option>
										</select>
										<label for="duration">@lang('dashboard.when_arrive')</label>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group form-md-line-input">
										<select class="form-control" name="status" id="status">
											<option value="2" @if($trip->status == 2)
												selected="selected" @endif>@lang('dashboard.suspended')</option>
											<option value="1" @if($trip->status == 1)
												selected="selected" @endif>@lang('dashboard.active')</option>
											<option value="0" @if($trip->status == 0)
												selected="selected" @endif disabled>@lang('dashboard.inactive')</option>
										</select>
										<label for="status">@lang('dashboard.status')</label>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group form-md-checkboxes">
										<label for="can_buy">&nbsp;</label>
										<div class="md-checkbox-list">
											<div class="md-checkbox">
												<input type="checkbox" id="can_buy" name="can_buy" value="1" class="md-check" {{$trip->can_buy ? 'checked':''}}>
												<label for="can_buy">
													<span class="inc"></span>
													<span class="check"></span>
													<span class="box"></span> @lang('dashboard.can_buy') </label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group form-md-checkboxes">
										<label for="can_buy">&nbsp;</label>
										<div class="md-checkbox-list">
											<div class="md-checkbox">
												<input type="checkbox" id="can_pay_at_bus" name="can_pay_at_bus" value="1" class="md-check"
														{{$trip->pay_at_bus ? 'checked':''}}>
												<label for="can_pay_at_bus">
													<span class="inc"></span>
													<span class="check"></span>
													<span class="box"></span> @lang('dashboard.can_pay_at_bus') </label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<button type="submit" class="btn green">@lang('main.save')</button>
								</div>
							</div>
						</div>
					</form>
					<!-- END FORM-->
				</div>
			</div>
			<!-- END VALIDATION STATES-->
		</div>
	</div>

@endsection