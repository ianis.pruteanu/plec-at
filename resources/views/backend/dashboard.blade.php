@extends('backend/backend')

@section('content')
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>@lang('main.dashboard')
                <small>@lang('main.welcome_to_dashboard')</small>
            </h1>
        </div>
        <!-- END PAGE TITLE -->
        <div class="page-toolbar">
            <a href="{{ route('dashboard.calculate') }}">{{ trans('dashboard.update') }}</a>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <span class="active">@lang('main.dashboard')</span>
        </li>
    </ul>
    <!-- END PAGE HEAD-->
    <div class="row widget-row">
        <div class="col-md-4 col-sm-6">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">@lang('dashboard.total_sales')
                    <span class="pull-right">
                    </span>
                </h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-red-thunderbird fa fa-money"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle currencyLabel">EUR</span>
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{ $totalIncome }}">{{ $totalIncome }}</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3 col-sm-6">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">@lang('dashboard.app_revenue')
                    <span class="pull-right">
                    </span>
                </h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-red-thunderbird fa fa-money"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">EUR</span>
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{ $appIncome }}">{{ $appIncome }}</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">@lang('dashboard.revenue') </h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-blue-madison fa fa-money"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">EUR</span>
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{ $revenue }}">{{ $revenue }}</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-2">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">@lang('dashboard.total_tickets_sold')</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-red-thunderbird fa fa-ticket"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">@lang('dashboard.tickets')</span>
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{!! $soldTickets + $confirmedTickets !!}">{!!
                        $soldTickets + $confirmedTickets !!}</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
    </div>
    <div class="row widget-row">
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">@lang('dashboard.sales_today')</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-green fa fa-money"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">EUR</span>
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{!! $todayIncome !!}  ">{!! $todayIncome !!}</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">@lang('dashboard.today_tickets_sold')</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-green fa fa-ticket"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">@lang('dashboard.tickets')</span>
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{!! $soldTicketsToday !!}">{!! $soldTicketsToday !!}</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">@lang('dashboard.total_week_sales')
                    {{ \Carbon\Carbon::now()->subDay()->startOfWeek()->format('d M') }} - {{ \Carbon\Carbon::now()->subDay()->format('d M') }}</h4></h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-purple-studio icon-screen-desktop"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">EUR</span>
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{ $weekIncome }}">{{ $weekIncome }}</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">@lang('dashboard.tickets_sold_between')
                    {{ \Carbon\Carbon::now()->subDay()->startOfWeek()->format('d M') }} - {{ \Carbon\Carbon::now()->subDay()->format('d M') }}</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-purple-studio icon-bar-chart"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">@lang('dashboard.tickets')</span>
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{!! $soldTicketsThisWeek !!}">{!! $soldTicketsThisWeek !!}</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
    </div>
@endsection