@extends('backend/backend')

@section('content')
	<link href="{{ asset('backend/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet"
	type="text/css" />
	<link href="{{ asset('backend/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('backend/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- BEGIN PAGE HEAD-->
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">
			<h1>{{ $course->name }}
				<small>@lang('dashboard.manage_it_good')</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
	<!-- END PAGE HEAD-->
	<!-- BEGIN PAGE BREADCRUMB -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ route('dashboard') }}">@lang('main.dashboard')</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span class="active">@lang('dashboard.companies')</span>
		</li>
	</ul>
	<!-- END PAGE BREADCRUMB -->
	<!-- BEGIN PAGE BASE CONTENT -->
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN VALIDATION STATES-->
			<div class="portlet light portlet-fit portlet-form bordered">
				<div class="portlet-body">
					<div class="backendAlertMessages">
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
				</div>
				<!-- BEGIN FORM-->
					<form action="{{ route('course.save') }}" method="POST">
						{{ csrf_field() }}
						<input type="hidden" name="course" value="{{ $course->id }}">
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" name="name"
								       value="{{ $course->name }}" id="name">
								<label for="name">@lang('main.name')
									<span class="required">*</span>
								</label>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="departureCity" class="control-label">@lang('main.departure_city')</label>
										<select name="departure_city" class="form-control select2 getPlaces">
											<option></option>
											@foreach($countries as $country)
												<optgroup label="@lang('geography.'.$country->slug)">
													@foreach($country->cities as $city)
														<option value="{{ $city->id }}" @if($course->departure_city == $city->id) selected @endif>
															@lang('geography.'.$city->slug)
														</option>
													@endforeach
												</optgroup>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="departurePlace" class="control-label">@lang('main.departure_place')</label>
										<select id="departurePlace" name="departure_place" class="form-control select2">
											<option></option>
											@foreach($course->departureCity->places as $place)
												<option value="{{ $place->id }}" @if($course->departure_place == $place->id) selected @endif>
													{{ $place->name }}
												</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>@lang('main.departure_time')</label>
										<div class="input-group">
											<input type="text" name="departure_hour" 
											value="{{ \Carbon\Carbon::parse($course->departure_hour)->format('H:i') }}"
											class="form-control timepicker	timepicker-24">
											<span class="input-group-btn">
		                                        <button class="btn default" type="button">
		                                            <i class="fa fa-clock-o"></i>
		                                        </button>
		                                    </span>
										</div>
									</div>
								</div>
								</div>
								<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label for="arrivalCity" class="control-label">@lang('main.arrival_city')</label>
										<select name="arrival_city" class="form-control select2 getPlaces">
											<option></option>
											@foreach($countries as $country)
												<optgroup label="@lang('geography.'.$country->slug)">
													@foreach($country->cities as $city)
														<option value="{{ $city->id }}"  @if($course->arrival_city == $city->id) selected @endif>
															@lang('geography.'.$city->slug)</option>
													@endforeach
												</optgroup>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label for="arrivalPlace" class="control-label">@lang('main.arrival_place')</label>
										<select id="arrivalPlace" name="arrival_place" class="form-control select2">
											<option></option>
											@foreach($course->arrivalCity->places as $place)
												<option value="{{ $place->id }}" @if($course->arrival_place == $place->id) selected @endif>
													{{ $place->name }}
												</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>@lang('main.arrival_time')</label>
										<div class="input-group">
											<input type="text" name="arrival_hour"
											value="{{ \Carbon\Carbon::parse($course->arrival_hour)->format('H:i') }}"
											 class="form-control timepicker	timepicker-24">
											<span class="input-group-btn">
		                                        <button class="btn default" type="button">
		                                            <i class="fa fa-clock-o"></i>
		                                        </button>
		                                    </span>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label>@lang('dashboard.booking_hour_limit')</label>
										<input type="number" name="limit_hours" class="form-control" value="{{ $course->last_hour_payment }}">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>@lang('dashboard.default_bus')</label>
										<select name="default_bus" class="form-control">
											<option></option>
											@foreach($course->company->buses as $bus)
												<option value="{{ $bus->id }}" @if($bus->id == $course->default_bus_id) selected="selected" @endif> {{
												$bus->getFullName()
												}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>@lang('dashboard.main_currency')</label>
										<select class="form-control" name="main_currency" id="">
											@foreach(config('config.currencies') as $key => $currency)
												<option @if($course->main_currency == $key) selected @endif value="{{ $key }}">
													{{ $currency }}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>@lang('dashboard.course_frequency')</label>
										<select class="form-control" name="frequency" id="frequency">
											<option></option>
											<option value="1" @if($course->frequency == 1) selected @endif>@lang('dashboard.every_day')</option>
											<option value="2" @if($course->frequency == 2) selected @endif>@lang('dashboard.every_second_day')</option>
											<option value="3" @if($course->frequency == 3) selected @endif>@lang('dashboard.odd_days')</option>
											<option value="4" @if($course->frequency == 4) selected @endif>@lang('dashboard.even_days')</option>
											<option value="5" @if($course->frequency == 5) selected @endif>@lang('dashboard.custom')</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group @if($course->frequency <> 2) hide @endif" id="startDate">
										<label>@lang('dashboard.start_date')</label>
										<input type="text" name="start_date" class="form-control" value="{{ $course->start_date }}">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group @if($course->frequency <> 5) hide @endif" id="custom-frequency">
										<label>&nbsp;</label>
										<div class="md-checkbox-inline">
											<div class="md-checkbox has-info">
												<input type="checkbox" name="custom_frequency[]" value="mon" id="monday" class="md-check">
												<label for="monday">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> @lang('main.monday') </label>
											</div>
											<div class="md-checkbox has-info">
												<input type="checkbox" name="custom_frequency[]" value="tue" id="tuesday" class="md-check">
												<label for="tuesday">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> @lang('main.tuesday') </label>
											</div>
											<div class="md-checkbox has-info">
												<input type="checkbox" name="custom_frequency[]" value="wed" id="wednesday" class="md-check">
												<label for="wednesday">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> @lang('main.wednesday') </label>
											</div>
											<div class="md-checkbox has-info">
												<input type="checkbox" name="custom_frequency[]" value="thu" id="thursday" class="md-check">
												<label for="thursday">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> @lang('main.thursday') </label>
											</div>
											<div class="md-checkbox has-info">
												<input type="checkbox" name="custom_frequency[]" value="fri" id="friday" class="md-check">
												<label for="friday">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> @lang('main.friday') </label>
											</div>
											<div class="md-checkbox has-info">
												<input type="checkbox" name="custom_frequency[]" value="sat" id="saturday" class="md-check">
												<label for="saturday">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> @lang('main.saturday') </label>
											</div>
											<div class="md-checkbox has-info">
												<input type="checkbox" name="custom_frequency[]" value="sun" id="sunday" class="md-check">
												<label for="sunday">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> @lang('main.sunday') </label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<button type="submit" class="btn green">@lang('main.save')</button>
								</div>
							</div>
						</div>
					</form>
					<!-- END FORM-->
				</div>
			</div>
			<!-- END VALIDATION STATES-->
		</div>
	</div>

@endsection