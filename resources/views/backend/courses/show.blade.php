@extends('backend/backend')

@section('content')
	<link href="{{ asset('backend/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('backend/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('backend/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- BEGIN PAGE HEAD-->
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">
			<h1>{{ $course->name }}
				<small>{{ $course->company->legal_name }}</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
		<div class="page-toolbar">
			<a href="{{ route('course.edit', $course->slug) }}" class="btn btn-lg blue"><i class="fa fa-cogs"></i> @lang('dashboard.settings')</a>
		</div>
	</div>
	<!-- END PAGE HEAD-->
	<div class="backendAlertMessages">
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
	<!-- BEGIN PAGE BREADCRUMB -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ route('dashboard') }}">@lang('main.dashboard')</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="{{ route('courses.list') }}">@lang('main.all_courses')</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span class="active">{{ $course->name }}</span>
		</li>
	</ul>
	<!-- END PAGE BREADCRUMB -->
	<!-- BEGIN PAGE BASE CONTENT -->
	<div class="row">
		<div class="col-md-3">
			<div class="portlet light">
				<div class="portlet-title tabbable-line">
					<div class="caption">
						<i class="fa fa-map-signs"></i>
						<span class="caption-subject bold uppercase"> @lang('dashboard.trips')</span>
						<span class="caption-helper"></span>
					</div>
					<div class="actions">
						<button class="btn btn-circle btn-default" data-target="#add_trip" data-toggle="modal">
							<i class="fa fa-plus"></i> @lang('main.add') </button>
						<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
					</div>
				</div>
				<div class="portlet-body">
					<ul class="nav nav-tabs nav-justified">
						<li class="active">
							<a href="#tur" data-toggle="tab" aria-expanded="true"> @lang('main.tur') </a>
						</li>
						<li class="">
							<a href="#retur" data-toggle="tab" aria-expanded="false"> @lang('main.retur') </a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tur">
							<div class="panel-group accordion" id="accordion3">
								@foreach($course->trips as $trip)
									@if($trip->tur_retur == 0)
										<p><a href="{{ route('trip.show',[$course->slug, $trip->id]) }}">
												@lang('geography.'.$trip->departureCity->slug) - @lang('geography.'.$trip->arrivalCity->slug)
											</a> </p>
									@endif
								@endforeach
							</div>
						</div>
						<div class="tab-pane" id="retur">
							<div class="panel-group accordion" id="accordion2">
								@foreach($course->trips as $trip)
									@if($trip->tur_retur == 1)
										<p><a href="{{ route('trip.show',[$course->slug, $trip->id]) }}">
												@lang('geography.'.$trip->departureCity->slug) - @lang('geography.'.$trip->arrivalCity->slug)
											</a> </p>
									@endif
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-7">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-map font-blue"></i>
						<span class="caption-subject bold uppercase font-blue"> @lang('main.bookings')</span>
						<span class="caption-helper"></span>
					</div>
				</div>
				<div class="portlet-body">
					@foreach($tickets as $ticket)
						<p class="margin-bottom-5">
							<strong><a href="{{ route('ticket.show', $ticket->code) }}">{{ $ticket->code }}</a></strong>,
							<u>{{ \Carbon\Carbon::parse($ticket->date)->format('d M Y') }}</u>,
							{{ $ticket->user->first_name }} {{ $ticket->user->last_name }},
							<strong>
								{{ trans('geography.'.$ticket->trip->departureCity->slug) }} - {{ trans('geography.'.$ticket->trip->arrivalCity->slug) }}
							</strong>,
							{{ $ticket->user->email }}@if($ticket->user->phone) / {{ $ticket->user->phone }} @endif
							@if($ticket->status == 0)
								<span class=" pull-right"><i class="fa fa-circle-o-notch"></i> @lang('dashboard.unconfirmed')</span>
							@elseif($ticket->status == 1)
								<span class="font-blue pull-right"><i class="fa fa-bookmark"></i> @lang('dashboard.booked')
									<a href="{{ route('ticket.confirm', $ticket->id) }}" class="btn green-dark btn-cons btn-xs">
									<i class="fa fa-check"></i> </a></span>
							@elseif($ticket->status == 2)
								<span class="font-green-dark pull-right"><i class="fa fa-hand-pointer-o"></i> @lang('dashboard.confirmed')</span>
							@elseif($ticket->status == 3)
								<span class="font-green  pull-right"><i class="fa fa-check"></i> @lang('dashboard.paid')</span>
							@elseif($ticket->status == 4)
								<span class="font-red-intense  pull-right"><i class="fa fa-close"></i> @lang('dashboard.canceled')</span>
							@elseif($ticket->status == 5)
								<span class="font-purple-intense  pull-right"><i class="fa fa-close"></i> @lang('dashboard.refunded')</span>
							@endif
						</p>
					@endforeach
				</div>
			</div>
		</div>
	</div>

	{{-- Add Trip Modal --}}
	<div class="modal fade bs-modal-lg" id="add_trip" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title"><i class='fa fa-plus'></i>  @lang('dashboard.add_new_trip')</h4>
				</div>
				<form action="{{ route('trip.create') }}" method="post" enctype="multipart/form-data">
					<div class="modal-body">
						{{ csrf_field() }}
						<input type="hidden" name="course_id" value="{{ $course->id }}" />
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="departureCityTrip" class="control-label">@lang('main.departure_city')</label>
									<select id="departureCityTrip" name="departure_city" class="form-control select2">
										<option></option>
										@foreach($countries as $country)
										<optgroup label="@lang('geography.'.$country->slug)">
												@foreach($country->cities as $city)
													<option value="{{ $city->id }}">@lang('geography.'.$city->slug)</option>
												@endforeach
										</optgroup>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="departurePlacesTrip" class="control-label">@lang('main.departure_place')
									</label>
									<select id="departurePlacesTrip" name="departure_place" disabled="disabled"
									        class="form-control select2">
										<option></option>
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>@lang('main.time')</label>
									<div class="input-group">
										<input type="text" name="departure_time" class="form-control timepicker
										timepicker-24">
										<span class="input-group-btn">
	                                        <button class="btn default" type="button">
	                                            <i class="fa fa-clock-o"></i>
	                                        </button>
	                                    </span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="arrivalCityTrip" class="control-label">@lang('main.arrival_city')</label>
									<select id="arrivalCityTrip" name="arrival_city" class="form-control select2">
										<option></option>
										@foreach($countries as $country)
											<optgroup label="@lang('geography.'.$country->slug)">
												@foreach($country->cities as $city)
													<option value="{{ $city->id }}">@lang('geography.'.$city->slug)</option>
												@endforeach
											</optgroup>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="arrivalPlacesTrip" class="control-label">@lang('main.arrival_place')</label>
									<select id="arrivalPlacesTrip" name="arrival_place" disabled="disabled"
									        class="form-control select2">
										<option></option>
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>@lang('main.time')</label>
									<div class="input-group">
										<input type="text" name="arrival_time" class="form-control timepicker timepicker-24">
										<span class="input-group-btn">
	                                        <button class="btn default" type="button">
	                                            <i class="fa fa-clock-o"></i>
	                                        </button>
	                                    </span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3 col-md-offset-9">
								<div class="mt-radio-inline">
									<label class="mt-radio">
										<input type="radio" name="re_tur" id="re_tur" value="0" checked="checked"> Tur
										<span></span>
									</label>
									<label class="mt-radio">
										<input type="radio" name="re_tur" id="re_tur" value="1"> Retur
										<span></span>
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn dark btn-outline" data-dismiss="modal">@lang('main.close')</button>
						<button type="submit" class="btn green">@lang('main.save')</button>
					</div>
				</form>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
@endsection