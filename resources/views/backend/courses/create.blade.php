@extends('backend/backend')

@section('content')
	<link href="{{ asset('backend/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet"
	type="text/css" />
	<link href="{{ asset('backend/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('backend/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- BEGIN PAGE HEAD-->
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">
			<h1>@lang('dashboard.add_new_course')
				<small>@lang('dashboard.manage_it_good')</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
	<!-- END PAGE HEAD-->
	<!-- BEGIN PAGE BREADCRUMB -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ route('dashboard') }}">@lang('main.dashboard')</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span class="active">@lang('dashboard.companies')</span>
		</li>
	</ul>
	<!-- END PAGE BREADCRUMB -->
	<!-- BEGIN PAGE BASE CONTENT -->
	<div class="row backendCreateCourse">
		<div class="col-md-12">
			<div class="portlet light portlet-fit portlet-form bordered">
				<div class="portlet-body">
				<div class="backendAlertMessages">
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
				</div>
					<form action="{{ route('course.store') }}" method="POST">
						{{ csrf_field() }}
						<div class="row">
							<div class="col-md-6">
								<div class="form-group form-md-line-input">
									<input type="text" class="form-control" name="name" value="{{ old('name')}}" id="name">
									<label for="name">@lang('dashboard.course_name')
										<span class="required">*</span>
									</label>
								</div>
							</div>
							<div class="col-md-6">
								@hasanyrole('admin|moderator')
								<div class="form-group form-md-line-input">
									<select class="form-control" name="company_id">
										<option></option>
										@foreach($companies as $company)
											<option value="{{ $company->id }}" value="@if(old('company_id')) {{ old('company_id') }} @endif" @if(old('company_id')) {{ 'selected' }} @endif>{{ $company->legal_name }}</option>
										@endforeach
									</select>
									<label for="form_control_1">@lang('dashboard.company') <span class="required">*</span></label>

								</div>
								@else
									@can('create course')
										<div class="form-group form-md-line-input">
											<input type="text" disabled="disabled" value="{{ auth()->user()->company()->legal_name }}" />
											<label for="form_control_1">@lang('dashboard.company') <span class="required">*</span></label>
										</div>
									@endcan
								@endhasanyrole
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="departureCity" class="control-label">@lang('main.departure_city') <span class="required">*</span></label>
									<select name="departure_city" class="form-control select2 getPlaces">
										<option></option>
										@foreach($countries as $country)
											<optgroup label="@lang('geography.'.$country->slug)">
												@foreach($country->cities as $city)
													<option value="{{ $city->id }}">@lang('geography.'.$city->slug)</option>
												@endforeach
											</optgroup>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="departurePlace" class="control-label">@lang('main.departure_place') <span class="required">*</span></label>
									<select id="departurePlace" name="departure_place" class="form-control select2" disabled>
										<option></option>
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>@lang('main.departure_time')</label>
									<div class="input-group">
										<input type="text" name="departure_hour" class="form-control timepicker
										timepicker-24">
										<span class="input-group-btn">
	                                        <button class="btn default" type="button">
	                                            <i class="fa fa-clock-o"></i>
	                                        </button>
	                                    </span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="arrivalCity" class="control-label">@lang('main.arrival_city') <span class="required">*</span></label>
										<select name="arrival_city" class="form-control select2 getPlaces">
											<option></option>
											@foreach($countries as $country)
												<optgroup label="@lang('geography.'.$country->slug)">
													@foreach($country->cities as $city)
														<option value="{{ $city->id }}">@lang('geography.'.$city->slug)</option>
													@endforeach
												</optgroup>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="arrivalPlace" class="control-label">@lang('main.arrival_place') <span class="required">*</span></label>
										<select id="arrivalPlace" name="arrival_place" class="form-control select2" disabled>
											<option></option>
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>@lang('main.arrival_time')</label>
										<div class="input-group">
											<input type="text" name="arrival_hour" class="form-control timepicker timepicker-24">
											<span class="input-group-btn">
		                                        <button class="btn default" type="button">
		                                            <i class="fa fa-clock-o"></i>
		                                        </button>
		                                    </span>
										</div>
									</div>
								</div>
							</div>
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<label>@lang('dashboard.main_currency')</label>
									<select class="form-control" name="main_currency" id="">
										@foreach(config('config.currencies') as $key => $currency)
											<option value="{{ $key }}">
												{{ $currency }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>@lang('dashboard.course_frequency')</label>
									<select class="form-control" name="frequency" id="frequency">
										<option></option>
										<option value="1">@lang('dashboard.every_day')</option>
										<option value="2">@lang('dashboard.every_second_day')</option>
										<option value="3">@lang('dashboard.odd_days')</option>
										<option value="4">@lang('dashboard.even_days')</option>
										<option value="5">@lang('dashboard.custom')</option>
									</select>
								</div>
							</div>
							<div class="col-md-5">
								<div class="form-group hide" id="startDate">
									<label>@lang('dashboard.start_date')</label>
									<input type="text" name="start_date" class="form-control" value="{{ \Carbon\Carbon::today()->format('Y-m-d') }}">
								</div>
							</div>
							<div class="col-md-5">
								<div class="form-group hide" id="custom-frequency">
									<label>&nbsp;</label>
									<div class="md-checkbox-inline">
										<div class="md-checkbox has-info">
											<input type="checkbox" name="custom_frequency[]" value="mon" id="monday" class="md-check">
											<label for="monday">
												<span></span>
												<span class="check"></span>
												<span class="box"></span> @lang('main.monday') </label>
										</div>
										<div class="md-checkbox has-info">
											<input type="checkbox" name="custom_frequency[]" value="tue" id="tuesday" class="md-check">
											<label for="tuesday">
												<span></span>
												<span class="check"></span>
												<span class="box"></span> @lang('main.tuesday') </label>
										</div>
										<div class="md-checkbox has-info">
											<input type="checkbox" name="custom_frequency[]" value="wed" id="wednesday" class="md-check">
											<label for="wednesday">
												<span></span>
												<span class="check"></span>
												<span class="box"></span> @lang('main.wednesday') </label>
										</div>
										<div class="md-checkbox has-info">
											<input type="checkbox" name="custom_frequency[]" value="thu" id="thursday" class="md-check">
											<label for="thursday">
												<span></span>
												<span class="check"></span>
												<span class="box"></span> @lang('main.thursday') </label>
										</div>
										<div class="md-checkbox has-info">
											<input type="checkbox" name="custom_frequency[]" value="fri" id="friday" class="md-check">
											<label for="friday">
												<span></span>
												<span class="check"></span>
												<span class="box"></span> @lang('main.friday') </label>
										</div>
										<div class="md-checkbox has-info">
											<input type="checkbox" name="custom_frequency[]" value="sat" id="saturday" class="md-check">
											<label for="saturday">
												<span></span>
												<span class="check"></span>
												<span class="box"></span> @lang('main.saturday') </label>
										</div>
										<div class="md-checkbox has-info">
											<input type="checkbox" name="custom_frequency[]" value="sun" id="sunday" class="md-check">
											<label for="sunday">
												<span></span>
												<span class="check"></span>
												<span class="box"></span> @lang('main.sunday') </label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<button type="submit" class="btn green">@lang('main.save')</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

@endsection