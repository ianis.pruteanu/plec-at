@extends('backend/backend')

@section('content')
	<!-- BEGIN PAGE HEAD-->
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">
			<h1>@lang('dashboard.create_company')</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
	<!-- END PAGE HEAD-->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ route('dashboard') }}">@lang('main.dashboard')</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="{{ route('users.list') }}">@lang('dashboard.companies')</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span class="active">@lang('dashboard.create_company')</span>
		</li>
	</ul>
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN VALIDATION STATES-->
			<div class="portlet light portlet-fit portlet-form bordered">
				<div class="portlet-body">
					<div class="backendAlertMessages">
						@if ($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
					</div>
					<!-- BEGIN FORM-->
					<form action="{{ route('company.store') }}" method="POST">
						{{ csrf_field() }}
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" name="legal_name"
								       value="{{ old('legal_name')}}" id="legal_name">
								<label for="legal_name">@lang('dashboard.legal_name')
									<span class="required">*</span>
								</label>
							</div>
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" name="displayed_name"
								       value="{{ old('displayed_name')}}" id="displayed_name">
								<label for="displayed_name">@lang('dashboard.displayed_name')
									<span class="required">*</span>
								</label>
							</div>
							<div class="row">
								<div class="col-md-10">
									<div class="form-group form-md-line-input">
										<select class="form-control" name="owner" id="owner-select">
											<option value="">@lang('main.select')</option>
											@foreach($owners as $owner)
												<option value="{{ $owner->id }}">
													{{ $owner->first_name }} {{ $owner->last_name }}
												</option>
											@endforeach
										</select>
										<label for="owner-select">@lang('dashboard.owner')</label>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group form-md-checkboxes">
										<label for="new_owner">&nbsp;</label>
										<div class="md-checkbox-list">
											<div class="md-checkbox">
												<input type="checkbox" id="new_owner" name="new_owner" value="1"
												       class="md-check">
												<label for="new_owner">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> @lang('dashboard.create_owner') </label>
											</div>
										</div>
									</div>
								</div>
							</div>
							{{-- New Owner Form --}}
							<div class="new-owner-form hidden">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group form-md-line-input">
											<input type="text" class="form-control" name="first_name"
											       value="{{ old('first_name')}}" id="first_name">
											<label for="first_name">@lang('main.first_name')
												<span class="required">*</span>
											</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group form-md-checkboxes">
											<div class="form-group form-md-line-input">
												<input type="text" class="form-control" name="last_name"
												       value="{{ old('last_name')}}" id="last_name">
												<label for="last_name">@lang('main.last_name')
													<span class="required">*</span>
												</label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group form-md-line-input">
											<input type="text" class="form-control" name="phone"
											       value="{{ old('phone')}}" id="phone">
											<label for="phone">@lang('main.phone')
												{{-- <span class="required">*</span> --}}
											</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group form-md-checkboxes">
											<div class="form-group form-md-line-input">
												<input type="email" class="form-control" name="email"
												       value="{{ old('email')}}" id="email">
												<label for="email">@lang('main.email')
													{{-- <span class="required">*</span> --}}
												</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							{{-- end new owner form --}}
						</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-12">
										<button type="submit" id="createCompanyButton" class="btn green">@lang('main.save')</button>
										<button type="reset" class="btn default">Reset</button>
									</div>
								</div>
							</div>
					</form>
					<!-- END FORM-->
				</div>
			</div>
			<!-- END VALIDATION STATES-->
		</div>
	</div>
@endsection
