@extends('backend/backend')

@section('content')

	<!-- BEGIN PAGE HEAD-->
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">
			<h1>@if($company->displayed_name)
					{{ $company->displayed_name }}
				@else
					{{ $company->legal_name }}
				@endif
				<small>@lang('dashboard.manage_it_good')</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->

	</div>
	<!-- END PAGE HEAD-->
	<!-- BEGIN PAGE BREADCRUMB -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ route('dashboard') }}">@lang('main.dashboard')</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span class="active">@lang('dashboard.companies')</span>
		</li>
	</ul>
	<!-- END PAGE BREADCRUMB -->
	<!-- BEGIN PAGE BASE CONTENT -->
	<div class="row">
		<div class="col-md-3">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-map-signs"></i>
						<span class="caption-subject bold uppercase"> @lang('main.courses')</span>
						<span class="caption-helper"></span>
					</div>
					<div class="actions">
						<a href="{{ route('course.create') }}" class="btn btn-circle btn-default">
							<i class="fa fa-plus"></i> @lang('main.add') </a>
						<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
					</div>
				</div>
				<div class="portlet-body">
					@foreach($company->courses as $course)
						<p class="margin-bottom-5"><a href="{{ route('course.show', $course->slug) }}">{{ $course->name }}</a></p>
					@endforeach
				</div>
			</div>
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-map-signs"></i>
						<span class="caption-subject bold uppercase"> @lang('dashboard.buses')</span>
						<span class="caption-helper"></span>
					</div>
					<div class="actions">
						<a href="{{ route('bus.create') }}" class="btn btn-circle btn-default">
							<i class="fa fa-plus"></i> @lang('main.add') </a>
						<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
					</div>
				</div>
				<div class="portlet-body">
					@foreach($company->buses as $bus)
						<p class="margin-bottom-5"><a href="{{ route('bus.show', $bus->slug) }}">{{ $bus->getFullName() }}</a></p>
					@endforeach
				</div>
			</div>
		</div>
		<div class="col-md-7">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-map font-blue"></i>
						<span class="caption-subject bold font-blue uppercase"> @lang('main.bookings')</span>
						<span class="caption-helper"></span>
					</div>
					<div class="actions">
						<a href="javascript:;" class="btn btn-circle btn-default">
							<i class="fa fa-plus"></i> @lang('main.add') </a>
						<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
					</div>
				</div>
				<div class="portlet-body">
					@foreach($tickets as $ticket)
						<p class="margin-bottom-5">
							<strong><a href="{{ route('ticket.show', $ticket->code) }}">{{ $ticket->code }}</a></strong>,
							<u>{{ \Carbon\Carbon::parse($ticket->date)->format('d M Y') }}</u>,
							{{ $ticket->user->first_name }} {{ $ticket->user->last_name }},
							<strong>
								{{ trans('geography.'.$ticket->trip->departureCity->slug) }} - {{ trans('geography.'.$ticket->trip->arrivalCity->slug) }}
							</strong>,
							{{ $ticket->user->email }}@if($ticket->user->phone) / {{ $ticket->user->phone }} @endif
							@if($ticket->status == 0)
								<span class=" pull-right"><i class="fa fa-circle-o-notch"></i> @lang('dashboard.unconfirmed')</span>
							@elseif($ticket->status == 1)
								<span class="font-blue pull-right"><i class="fa fa-bookmark"></i> @lang('dashboard.booked')
									<a href="{{ route('ticket.confirm', $ticket->id) }}" class="btn green-dark btn-cons btn-xs">
									<i class="fa fa-check"></i> </a></span>
							@elseif($ticket->status == 2)
								<span class="font-green-dark pull-right"><i class="fa fa-hand-pointer-o"></i> @lang('dashboard.confirmed')</span>
							@elseif($ticket->status == 3)
								<span class="font-green  pull-right"><i class="fa fa-check"></i> @lang('dashboard.paid')</span>
							@elseif($ticket->status == 4)
								<span class="font-red-intense  pull-right"><i class="fa fa-close"></i> @lang('dashboard.canceled')</span>
							@elseif($ticket->status == 5)
								<span class="font-purple-intense  pull-right"><i class="fa fa-close"></i> @lang('dashboard.refunded')</span>
							@endif
						</p>
					@endforeach
				</div>
			</div>
		</div>
		<div class="col-md-2">
			<img src="{{ asset($company->logo) }}" class="img-responsive" />
		</div>
	</div>
@endsection