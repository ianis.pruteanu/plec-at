@extends('backend/backend')

@section('content')

	<!-- BEGIN PAGE HEAD-->
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">
			<h1>@lang('dashboard.companies')
				<small>@lang('dashboard.manage_it_good')</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
		<div class="page-toolbar">
			<a href="{{ route('company.create') }}" class="btn btn-lg blue"><i class="fa fa-plus"></i> @lang('dashboard.create_company')</a>
		</div>
	</div>
	<!-- END PAGE HEAD-->
	<!-- BEGIN PAGE BREADCRUMB -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ route('dashboard') }}">@lang('main.dashboard')</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span class="active">@lang('dashboard.companies')</span>
		</li>
	</ul>
	<!-- END PAGE BREADCRUMB -->
	<!-- BEGIN PAGE BASE CONTENT -->
	<div class="row">
		@foreach($companies as $company)
		<div class="col-sm-12 col-md-3">
			<div class="thumbnail">
				<img src="{{ asset($company->cover) }}" class="closed" style="width: 100%; height: 200px; display: block;">
				<div class="caption">
					<a href="{{ route('company.show', $company->slug) }}">
						<h3>
							@if($company->displayed_name)
								{{ $company->displayed_name }}
							@else
								{{ $company->legal_name }}
							@endif
						</h3>
					</a>
					<p>
					<div class="row">
						<div class="col-md-8">
							<a href="{{ route('company.show', $company->slug) }}" class="btn default">
								<i class="fa fa-external-link-square"></i> @lang('main.open')
							</a>
							<a href="{{ route('company.edit', $company->slug) }}" class="btn blue">
								<i class="fa fa-pencil"></i> @lang('main.edit')
							</a>
						</div>
						<div class="col-md-4">
							@if($company->status <> 4)
							<button class="btn red pull-right roPasswordConfirm" data-id="{{ $company->id }}" data-action="close" data-type="company">
								<i class="fa fa-close"></i> @lang('main.close')
							</button>
							@endif
						</div>
					</div>
					</p>
				</div>
			</div>
		</div>
		@endforeach
	</div>

@endsection