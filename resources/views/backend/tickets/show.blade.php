@extends('backend/backend')

@section('content')

    <link href="{{ asset('backend/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>@lang('dashboard.ticket') <strong>{{ $ticket->code }}</strong>
                <small>@lang('main.welcome_to_dashboard')</small>
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <div class="row">
        <div class="col-md-8">
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-list-alt"></i> @lang('dashboard.details') </div>
                </div>
                <div class="portlet-body flip-scroll">
                    <div class="pull-right">
                        <h3 class="pull-right margin-top-10">
                            @if($ticket->status == 0)
                                <span class=""><i class="fa fa-circle-o-notch"></i> @lang('dashboard.unconfirmed')</span>
                            @elseif($ticket->status == 1)
                                <span class="font-blue"><i class="fa fa-bookmark"></i> @lang('dashboard.booked')</span>
                                <a href="{{ route('ticket.confirm', $ticket->id) }}" class="btn green-dark btn-cons btn-xs">
                                    <i class="fa fa-check"></i> </a>
                            @elseif($ticket->status == 2)
                                <span class="font-green-dark"><i class="fa fa-hand-pointer-o"></i> @lang('dashboard.confirmed')</span>
                            @elseif($ticket->status == 3)
                                <span class="font-green"><i class="fa fa-check"></i> @lang('dashboard.paid')</span>
                            @elseif($ticket->status == 4)
                                <span class="font-red-intense"><i class="fa fa-close"></i> @lang('dashboard.canceled')</span>
                            @elseif($ticket->status == 5)
                                <span class="font-purple-intense"><i class="fa fa-close"></i> @lang('dashboard.refunded')</span>
                            @endif
                        </h3><br /><br />
                        <a href="{{ route('ticket.download', $ticket->id) }}" class="btn btn grey-cararra pull-right">
                            <i class="fa fa-download"></i> @lang('dashboard.download')</a>
                    </div>
                    <h4>@lang('main.first_name') <strong>{{ $ticket->user->first_name }}</strong></h4>
                    <h4>@lang('main.last_name') <strong>{{ $ticket->user->last_name }}</strong></h4>
                    <h4>@lang('main.phone') <strong>{{ $ticket->user->phone }}
                        @if($ticket->details()->where('information_type', 'phone_number')->first()) /
                            <span class="font-blue"> {{ $ticket->details()->where('information_type', 'phone_number')->first()->information_value }} </span>
                        @endif</strong>
                    </h4>
                    <h4>@lang('main.email') <strong>{{ $ticket->user->email }}</strong></h4>
                    <hr />
                    <h4>@lang('dashboard.created_at') <strong>{{ \Carbon\Carbon::parse($ticket->created_at)->format('d M Y H:i') }}</strong></h4>
                    @if($ticket->status == 2 && $ticket->created_at <> $ticket->updated_at)
                        <h4>@lang('dashboard.confirmed_at') <strong>{{ \Carbon\Carbon::parse($ticket->updated_at)->format('d M Y H:i') }}</strong></h4>
                    @endif
                    <hr />
                    @if($ticket->revenue)
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="margin-bottom-5">@lang('dashboard.paid_summ'):
                                <strong>{{ $ticket->paidSumm() }} {{ $ticket->paidCurrency() }}
                                    @if($ticket->paidCurrency() <> 'EUR') / {{ $ticket->revenue->paid }} EUR @endif</strong></h4>
                            @if($ticket->revenue->tax_amount <> 0)
                                <h4 class="margin-bottom-5">@lang('dashboard.payment_commission'): <strong>{{ $ticket->revenue->tax_amount }} EUR</strong></h4>
                            @endif
                            <h4 class="margin-bottom-5">@lang('dashboard.revenue'): <strong class="font-red-flamingo">{{ $ticket->revenue->revenue }}</strong></h4>
                            <h4 class="margin-bottom-5">@lang('dashboard.company_revenue'): <strong>{{ $ticket->revenue->company_revenue }}</strong></h4>
                        </div>
                        <div class="col-md-6">
                            @if($ticket->trip->revenue->type == 1)
                                <h3 class="no-margin">
                                    <label class="label label-info">
                                        @lang('dashboard.revenue_rate'): {{ $ticket->trip->revenue->amount }} %
                                    </label>
                                </h3>
                            @else
                                <h3 class="no-margin">
                                    <label class="label label-info">
                                        @lang('dashboard.revenue_rate'): {{ $ticket->trip->revenue->amount }} {{ $ticket->trip->revenue->currency() }}
                                    </label>
                                </h3>
                            @endif
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection