@extends('backend/backend')

@section('content')
	<!-- BEGIN PAGE HEAD-->
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">
			<h1>{{ $bus->getFullName() }}</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
	<!-- END PAGE HEAD-->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ route('dashboard') }}">@lang('main.dashboard')</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="{{ route('users.list') }}">@lang('dashboard.buses')</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span class="active">{{ $bus->getFullName() }}</span>
		</li>
	</ul>
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN VALIDATION STATES-->
			<div class="portlet light portlet-fit portlet-form bordered">
				<div class="portlet-body">
					<div class="backendAlertMessages">
						@if ($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
					</div>
					<!-- BEGIN FORM-->
					<form action="{{ route('bus.save') }}" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}
						<input type="hidden" value="{{ $bus->id }}" name="bus"/>
						<div class="form-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group form-md-line-input">
										<select class="form-control" name="mark" id="busMark">
											<option value="">@lang('main.select')</option>
											@foreach(config('config.busMarks') as $key => $mark)
												<option value="{{ $key }}" @if($bus->mark == $key) selected="selected" @endif>
													{{ $mark }}
												</option>
											@endforeach
										</select>
										<label for="busMark">@lang('dashboard.mark')</label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-md-line-input">
										<select class="form-control" name="model" id="busModels">
											<option value="">@lang('main.select')</option>
											@foreach(config('config.busModels.'.$bus->mark) as $key => $model)
												<option value="{{ $key }}" @if($bus->model == $key) selected="select" @endif>{{ $model }}</option>
											@endforeach
										</select>
										<label for="busModels">@lang('dashboard.model')</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group form-md-line-input">
										<select class="form-control" name="year" id="year">
											<option value="">@lang('main.select')</option>
											@for($i = \Carbon\Carbon::now()->format('Y'); $i >= 1980 ; $i--)
												<option value="{{ $i}}" @if($bus->year == $i) selected="selected" @endif>
													{{ $i }}
												</option>
											@endfor
										</select>
										<label for="year">@lang('dashboard.year')</label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-md-line-input">
										<select class="form-control" name="color" id="color">
											<option value="">@lang('main.select')</option>
											<option value="white" @if($bus->color === 'white') selected="selected" @endif>
												@lang('dashboard.white')</option>
											<option value="black" @if($bus->color === 'black') selected="selected" @endif>
												@lang('dashboard.black')</option>
											<option value="grey" @if($bus->color === 'grey') selected="selected" @endif>
												@lang('dashboard.grey')</option>
											<option value="blue" @if($bus->color === 'blue') selected="selected" @endif>
												@lang('dashboard.blue')</option>
											<option value="green" @if($bus->color === 'green') selected="selected" @endif>
												@lang('dashboard.green')</option>
											<option value="red" @if($bus->color === 'red') selected="selected" @endif>
												@lang('dashboard.red')</option>
											<option value="yellow" @if($bus->color === 'yellow') selected="selected" @endif>
												@lang('dashboard.yellow')</option>
										</select>
										<label for="busModels">@lang('dashboard.color')</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>@lang('dashboard.image')</label>
										<input type="file" class="form-control" name="image" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group form-md-line-input">
										<input type="number" min="1" max="{{ config('config.max_bus_seats') }}"
										       class="form-control" id="seats" name="seats" value="{{ $bus->seats }}"/>
										<label for="seats">@lang('dashboard.seats_number')</label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="legal_number" id="legal_number" style="text-transform:uppercase" value="{{ $bus->legal_number }}"/>
										<label for="legal_number">@lang('dashboard.legal_number')</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group form-md-line-input">
										<select class="form-control" name="owner" id="owner-select">
											<option value="">@lang('main.select')</option>
											@foreach($companies as $company)
												<option value="{{ $company->id }}" @if($bus->company_id == $company->id) selected="selected" @endif>
													{{ $company->legalOrDisplayedName() }}
												</option>
											@endforeach
										</select>
										<label for="owner-select">@lang('dashboard.owner')</label>
									</div>
								</div>
							</div>
						</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-12">
										<button type="submit" class="btn green">@lang('main.save')</button>
										<button type="reset" class="btn default">Reset</button>
									</div>
								</div>
							</div>
					</form>
					<!-- END FORM-->
				</div>
			</div>
			<!-- END VALIDATION STATES-->
		</div>
	</div>
@endsection
