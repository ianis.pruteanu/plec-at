@extends('backend/backend')

@section('content')

	<!-- BEGIN PAGE HEAD-->
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">
			<h1>@lang('dashboard.buses')
				<small>@lang('dashboard.manage_it_good')</small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
	<!-- END PAGE HEAD-->
	<!-- BEGIN PAGE BREADCRUMB -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ route('dashboard') }}">@lang('main.dashboard')</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span class="active">@lang('dashboard.buses')</span>
		</li>
	</ul>
	<!-- END PAGE BREADCRUMB -->
	<!-- BEGIN PAGE BASE CONTENT -->
	<div class="row">
		@foreach($buses as $bus)
		<div class="col-sm-12 col-md-3">
			<div class="thumbnail">
				<img src="{{ asset($bus->image) }}" class="closed" style="width: 100%; height: 200px; display: block;">
				<div class="caption">
					<a href="#" data-toggle="modal" data-target="#bus{{ $bus->id }}">
						<h3>
							{{ $bus->mark }} {{ $bus->model }}, {{ $bus->year }}
						</h3>
					</a>
					<p>
					<div class="row">
						<div class="col-md-8">
							<a href="#" data-toggle="modal" data-target="#bus{{ $bus->id }}" class="btn default">
								<i class="fa fa-external-link-square"></i> @lang('main.open')
							</a>
							<a href="{{ route('bus.edit', $bus->slug) }}" class="btn blue">
								<i class="fa fa-pencil"></i> @lang('main.edit')
							</a>
						</div>
						<div class="col-md-4">
							@if($bus->status <> 4)
							<button class="btn red pull-right roPasswordConfirm" data-id="{{ $bus->id }}" data-action="close" data-type="bus">
								<i class="fa fa-close"></i> @lang('main.close')
							</button>
							@endif
						</div>
					</div>
					</p>
				</div>
			</div>
		</div>
		@endforeach
	</div>

@endsection