@extends('backend/backend')

@section('content')
	<!-- BEGIN PAGE HEAD-->
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">
			<h1>@lang('dashboard.add_bus')</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
	<!-- END PAGE HEAD-->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ route('dashboard') }}">@lang('main.dashboard')</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="{{ route('users.list') }}">@lang('dashboard.buses')</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span class="active">@lang('dashboard.add_bus')</span>
		</li>
	</ul>
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN VALIDATION STATES-->
			<div class="portlet light portlet-fit portlet-form bordered">
				<div class="portlet-body">
					<div class="backendAlertMessages">
						@if ($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
					</div>
					<!-- BEGIN FORM-->
					<form action="{{ route('bus.store') }}" method="POST">
						{{ csrf_field() }}
						<div class="form-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group form-md-line-input">
										<select class="form-control" name="mark" id="busMark">
											<option value="">@lang('main.select') <span class="required">*</span></option>
											@foreach(config('config.busMarks') as $key => $mark)
												<option value="{{ $key }}">
													{{ $mark }}
												</option>
											@endforeach
										</select>
										<label for="busMark">@lang('dashboard.mark')</label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-md-line-input">
										<select class="form-control" name="model" disabled="true" id="busModels">
											<option value="">@lang('main.select')</option>
										</select>
										<label for="busModels">@lang('dashboard.model')</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group form-md-line-input">
										<select class="form-control" name="year" id="year">
											<option value="">@lang('main.select') <span class="required">*</span></option>
											@for($i = \Carbon\Carbon::now()->format('Y'); $i >= 1980 ; $i--)
												<option value="{{ $i}}">
													{{ $i }}
												</option>
											@endfor
										</select>
										<label for="year">@lang('dashboard.year')</label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-md-line-input">
										<select class="form-control" name="color" id="color">
											<option value="">@lang('main.select')</option>
											<option value="white">@lang('dashboard.white')</option>
											<option value="black">@lang('dashboard.black')</option>
											<option value="grey">@lang('dashboard.grey')</option>
											<option value="blue">@lang('dashboard.blue')</option>
											<option value="green">@lang('dashboard.green')</option>
											<option value="red">@lang('dashboard.red')</option>
											<option value="yellow">@lang('dashboard.yellow')</option>
										</select>
										<label for="busModels">@lang('dashboard.color')</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group form-md-line-input">
										<input type="number" min="1" max="{{ config('config.max_bus_seats') }}"
										       class="form-control" id="seats" name="seats" value="{{old('seats')}}"/>
										<label for="seats">@lang('dashboard.seats_number') <span class="required">*</span></label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" name="legal_number" id="legal_number" style="text-transform:uppercase" value="{{ old('legal_number') }}"/>
										<label for="legal_number">@lang('dashboard.legal_number') <span class="required">*</span></label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group form-md-line-input">
										<select class="form-control" name="owner" id="owner-select">
											<option value="">@lang('main.select') <span class="required">*</span></option>
											@foreach($companies as $company)
												<option value="{{ $company->id }}" value="@if(old('owner')) {{ old('owner') }} @endif" @if(old('owner')) {{ 'selected' }} @endif>
													{{ $company->legalOrDisplayedName() }}
												</option>
											@endforeach
										</select>
										<label for="owner-select">@lang('dashboard.owner')</label>
									</div>
								</div>
							</div>
						</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-12">
										<button type="submit" class="btn green">@lang('main.save')</button>
										<button type="reset" class="btn default">Reset</button>
									</div>
								</div>
							</div>
					</form>
					<!-- END FORM-->
				</div>
			</div>
			<!-- END VALIDATION STATES-->
		</div>
	</div>
@endsection
