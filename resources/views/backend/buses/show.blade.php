@extends('backend/backend')

@section('content')

    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>{{ $bus->getFullName() }}
                <small>@lang('dashboard.manage_it_good')</small>
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">@lang('main.dashboard')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('buses.list') }}">@lang('main.buses')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">{{ $bus->getFullName() }}</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    {{-- @include('flash::message') --}}
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
        <div class="col-md-2">
            <img src="{{ $bus->image }}" class="image-responsive" />
        </div>
        <div class="col-md-10">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-map-signs"></i>
                        <span class="caption-subject bold uppercase"> @lang('dashboard.log')</span>
                        <span class="caption-helper"></span>
                    </div>
                    <div class="actions">
                        <a href="{{ route('bus.create') }}" class="btn btn-circle btn-default">
                            <i class="fa fa-plus"></i> @lang('main.add') </a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    </div>
                </div>
                <div class="portlet-body">

                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
@endsection