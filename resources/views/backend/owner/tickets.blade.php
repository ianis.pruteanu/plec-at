@extends('backend/backend')

@section('content')

    <link href="{{ asset('backend/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>@lang('dashboard.tickets')
                <small>{{ auth()->user()->company->legal_name }}</small>
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->

    <div class="row">
        <div class="col-md-9">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-list-alt"></i> @lang('dashboard.list') </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
                    </div>
                </div>
                <div class="portlet-body flip-scroll">
                    @if($tickets)
                    <table class="table table-bordered table-striped table-condensed flip-content">
                        <thead class="flip-content">
                        <tr>
                            <th width="10%"> @lang('dashboard.code') </th>
                            <th width=""> @lang('dashboard.from') </th>
                            <th width=""> @lang('dashboard.to') </th>
                            <th width="20%"> @lang('dashboard.date') / @lang('dashboard.time') </th>
                            <th width="15%"> @lang('main.first_name'), @lang('main.last_name') </th>
                            <th width=""> @lang('main.phone') </th>
                            <th width="10%"> @lang('dashboard.status') </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tickets as $ticket)
                        <tr>
                            <td> <a href="{{ route('ticket.show', $ticket->code) }}">{{ $ticket->code }} </a> </td>
                            <td class=""> @lang('geography.'.$ticket->trip->departureCity->slug) </td>
                            <td class=""> @lang('geography.'.$ticket->trip->arrivalCity->slug) </td>
                            <td class=""> {{ \Carbon\Carbon::parse($ticket->date)->format('d M Y') }}
                                {{ \Carbon\Carbon::parse($ticket->trip->departure_time)->format('H:i') }}
                            </td>
                            <td class=""> {{ $ticket->user->last_name }} {{ $ticket->user->first_name }}</td>
                            <td class=""> {{ $ticket->user->phone }} </td>
                            <td class="">
                                @if($ticket->status == 0)
                                    <span class=""><i class="fa fa-circle-o-notch"></i> @lang('dashboard.unconfirmed')</span>
                                @elseif($ticket->status == 1)
                                    <span class="font-blue"><i class="fa fa-bookmark"></i> @lang('dashboard.booked')</span>
                                    <a href="{{ route('ticket.confirm', $ticket->id) }}" class="btn green-dark btn-cons btn-xs">
                                        <i class="fa fa-check"></i> </a>
                                @elseif($ticket->status == 2)
                                    <span class="font-green-dark"><i class="fa fa-hand-pointer-o"></i> @lang('dashboard.confirmed')</span>
                                @elseif($ticket->status == 3)
                                    <span class="font-green"><i class="fa fa-check"></i> @lang('dashboard.paid')</span>
                                @elseif($ticket->status == 4)
                                    <span class="font-red-intense"><i class="fa fa-close"></i> @lang('dashboard.canceled')</span>
                                @elseif($ticket->status == 5)
                                    <span class="font-purple-intense"><i class="fa fa-close"></i> @lang('dashboard.refunded')</span>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @else
                        <h4>@lang('dashboard.there_is_no_tickets')</h4>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-list-alt"></i> @lang('main.courses')
                    </div>
                </div>
                <div class="portlet-body">
                    <p class="margin-bottom-10"><a href="{{ route('owner.tickets', auth()->user()->company->slug) }}">@lang('main.all')</a></p>
                    @foreach(auth()->user()->company->courses as $course)
                        <p class="margin-bottom-10"><a href="{{ route('owner.tickets', [auth()->user()->company->slug, $course->id]) }}">{{ $course->name }}</a></p>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection