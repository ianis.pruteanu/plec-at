@extends('backend/backend')

@section('content')
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>@lang('main.dashboard')
                <small>@lang('main.welcome_to_dashboard')</small>
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <div class="row widget-row">
        <div class="col-md-6">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">@lang('dashboard.total_sales')
                    <span class="pull-right">
                        <a href="#"><i class="icon-reload bold"></i> </a>
                    </span>
                </h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-red-thunderbird fa fa-money"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">EUR</span>
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="496,644">496,644</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-6">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">@lang('dashboard.total_tickets_sold')</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-red-thunderbird fa fa-ticket"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">@lang('dashboard.tickets')</span>
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="1,293">80,293</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
    </div>
    <div class="row widget-row">
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">@lang('dashboard.sales_today')</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-green fa fa-money"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">EUR</span>
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="815">815</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">@lang('dashboard.today_tickets_sold')</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-green fa fa-ticket"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">@lang('dashboard.tickets')</span>
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="5,071">5,071</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">@lang('dashboard.total_week_sales')</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-purple-studio icon-screen-desktop"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">EUR</span>
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="815">815</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">@lang('dashboard.total_week_tickets_sold')</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-purple-studio icon-bar-chart"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-subtitle">@lang('dashboard.tickets')</span>
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="5,071">5,071</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
    </div>
@endsection