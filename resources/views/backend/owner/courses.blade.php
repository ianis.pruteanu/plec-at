@extends('backend/backend')

@section('content')

	<!-- BEGIN PAGE HEAD-->
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">
			<h1>@lang('main.all_courses')
				<small></small>
			</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
	<!-- END PAGE HEAD-->
	<!-- BEGIN PAGE BREADCRUMB -->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ route('dashboard') }}">@lang('main.dashboard')</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span class="active">@lang('main.all_courses')</span>
		</li>
	</ul>
	<!-- END PAGE BREADCRUMB -->
	<!-- BEGIN PAGE BASE CONTENT -->
	<div class="row">
		@foreach($courses as $course)
			<div class="col-sm-12 col-md-3">
				<div class="thumbnail">
					<img src="{{ asset($course->image) }}" class="" style="width: 100%; height: 200px; display: block;">
					<div class="caption">
						<a href="{{ route('course.show', $course->slug) }}">
							<h4>
								{{ $course->name }}
							</h4>
						</a>
						<h4 class="font-red">{{ $course->company->legalOrDisplayedName() }}</h4>
						<p>
						<div class="row">
							<div class="col-md-8">
								<a href="{{ route('course.show', $course->slug) }}" class="btn default">
									<i class="fa fa-external-link-square"></i> @lang('main.open')
								</a>
								<a href="{{ route('course.edit', $course->slug) }}" class="btn blue">
									<i class="fa fa-pencil"></i> @lang('main.edit')
								</a>
							</div>
							<div class="col-md-4">
								@if($course->status <> 4)
									<button class="btn red pull-right roPasswordConfirm" data-id="{{ $course->id }}" data-action="close" data-type="course">
										<i class="fa fa-close"></i> @lang('main.close')
									</button>
								@endif
							</div>
						</div>
						</p>
					</div>
				</div>
			</div>
		@endforeach
	</div>

@endsection