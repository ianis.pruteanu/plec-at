@extends('backend/backend')

@section('content')
	<!-- BEGIN PAGE HEAD-->
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">
			<h1>{{ $company->legal_name }}</h1>
		</div>
		<!-- END PAGE TITLE -->
	</div>
	<!-- END PAGE HEAD-->
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ route('dashboard') }}">@lang('main.dashboard')</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="{{ route('users.list') }}">@lang('dashboard.companies')</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span class="active">@lang('dashboard.create_company')</span>
		</li>
	</ul>
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN VALIDATION STATES-->
			<div class="portlet light portlet-fit portlet-form bordered">
				<div class="portlet-body">
					<div class="backendAlertMessages">
						@if ($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
					</div>
					<!-- BEGIN FORM-->
					<form action="{{ route('company.save') }}" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}
						<input type="hidden" name="company_id" value="{{ $company->id }}">
						<div class="form-body">
							<div class="form-group form-md-line-input">
								<input type="text" disabled class="form-control" name="legal_name"
								       value="{{ $company->legal_name }}" id="legal_name">
								<label for="legal_name">@lang('dashboard.legal_name')
									<span class="required">*</span>
								</label>
							</div>
							<div class="form-group form-md-line-input">
								<input type="text" class="form-control" name="displayed_name"
								       value="{{ $company->displayed_name }}" id="displayed_name">
								<label for="displayed_name">@lang('dashboard.displayed_name')
									<span class="required">*</span>
								</label>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>@lang('dashboard.company_logo')</label>
										<input type="file" class="form-control" name="logo" />
									</div>
								</div>
							</div>
							<div class="row">

								<div class="col-md-2">
									<div class="form-group form-md-checkboxes">
										<label for="new_owner">&nbsp;</label>
										<div class="md-checkbox-list">
											<div class="md-checkbox">
												<input type="checkbox" id="new_owner" name="new_owner" value="1" disabled="" @if($company->status == 1)
												checked="checked" @endif
												       class="md-check">
												<label for="new_owner">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> @lang('dashboard.active') </label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-12">
										<button type="submit" class="btn green">@lang('main.save')</button>
									</div>
								</div>
							</div>
					</form>
					<!-- END FORM-->
				</div>
			</div>
			<!-- END VALIDATION STATES-->
		</div>
	</div>
@endsection
