<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>
        @if(auth()->user()->company)
            {{ auth()->user()->company->legal_name }}
        @else
            loggico.eu
        @endif
    </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #4 for blank page layout" name="description" />
    <meta content="" name="author" />
    <meta content="{{ csrf_token() }}" name="csrf-token" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ asset('backend/global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{ asset('backend/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/global/css/custom.css') }}" rel="stylesheet" type="text/css" /> {{-- custom css file --}}
    <link href="{{ asset('backend/global/css/flag-icon.min.css') }}" rel="stylesheet" type="text/css" /> {{-- custom flag css file --}}
    <!-- END THEME GLOBAL STYLES -->
    <link href="{{ asset('backend/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/pages/css/profile.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- BEGIN THEME LAYOUT STYLES -->

    <link href="{{ asset('backend/layouts/layout4/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/layouts/layout4/css/themes/default.min.css') }}" rel="stylesheet" type="text/css" id="style_color" />
    <link href="{{ asset('backend/layouts/layout4/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <!--[if lt IE 9]> -->
    {{-- BEGIN CORE PLUGINS --}}
    <script src="{{ asset('backend/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <script src="{{ asset('backend/global/plugins/respond.min.js') }}"></script>
    <script src="{{ asset('backend/global/plugins/excanvas.min.js') }}"></script>
    <script src="{{ asset('backend/global/plugins/ie8.fix.min.js') }}"></script>
    {{-- endif --}}


    <link rel="shortcut icon" href="favicon.ico" /> </head>
<!-- END HEAD -->

<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="{{ route('dashboard') }}">
                @if(auth()->user()->company)
                <img src="{{ asset(auth()->user()->company->logo) }}" alt="logo" class="logo-default" /> </a>
                @else
                <img src="{{ asset('backend/layouts/layout4/img/logodefault.png') }}" alt="logo" class="logo-default" /> </a>
                @endif
            <div class="menu-toggler sidebar-toggler">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN PAGE ACTIONS -->
        <!-- DOC: Remove "hide" class to enable the page header actions -->
        <div class="page-actions">
            <div class="btn-group">
                @if(auth()->user()->hasRole('admin'))
                    <button class="btn white btn-sm font-red bold">@lang('dashboard.administrator')</button>
                @endif
                @if(auth()->user()->hasRole('moderator'))
                    <button class="btn white btn-sm font-blue bold">@lang('dashboard.moderator')</button>
                @endif
                {{--<button type="button" class="btn red-haze btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">--}}
                    {{--<span class="hidden-sm hidden-xs">Actions&nbsp;</span>--}}
                    {{--<i class="fa fa-angle-down"></i>--}}
                {{--</button>--}}
                {{--<ul class="dropdown-menu" role="menu">--}}
                    {{--<li>--}}
                        {{--<a href="javascript:;">--}}
                            {{--<i class="icon-docs"></i> New Post </a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="javascript:;">--}}
                            {{--<i class="icon-tag"></i> New Comment </a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="javascript:;">--}}
                            {{--<i class="icon-share"></i> Share </a>--}}
                    {{--</li>--}}
                    {{--<li class="divider"> </li>--}}
                    {{--<li>--}}
                        {{--<a href="javascript:;">--}}
                            {{--<i class="icon-flag"></i> Comments--}}
                            {{--<span class="badge badge-success">4</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="javascript:;">--}}
                            {{--<i class="icon-users"></i> Feedbacks--}}
                            {{--<span class="badge badge-danger">2</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            </div>
        </div>
        <!-- END PAGE ACTIONS -->
        <!-- BEGIN PAGE TOP -->
        <div class="page-top">
            <!-- BEGIN HEADER SEARCH BOX -->
            <!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
            <!-- END HEADER SEARCH BOX -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <li class="separator hide"> </li>
                    <!-- BEGIN NOTIFICATION DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <!-- DOC: Apply "dropdown-hoverable" class after "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
                    <!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
                    {{--<li class="dropdown dropdown-extended dropdown-notification dropdown-dark" id="header_notification_bar">--}}
                        {{--<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">--}}
                            {{--<i class="icon-bell"></i>--}}
                            {{--<span class="badge badge-success"> 7 </span>--}}
                        {{--</a>--}}
                        {{--<ul class="dropdown-menu">--}}
                            {{--<li class="external">--}}
                                {{--<h3>--}}
                                    {{--<span class="bold">12 pending</span> notifications</h3>--}}
                                {{--<a href="page_user_profile_1.html">view all</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">--}}
                                    {{--<li>--}}
                                        {{--<a href="javascript:;">--}}
                                            {{--<span class="time">just now</span>--}}
                                            {{--<span class="details">--}}
                                                        {{--<span class="label label-sm label-icon label-success">--}}
                                                            {{--<i class="fa fa-plus"></i>--}}
                                                        {{--</span> New user registered. </span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<a href="javascript:;">--}}
                                            {{--<span class="time">3 mins</span>--}}
                                            {{--<span class="details">--}}
                                                        {{--<span class="label label-sm label-icon label-danger">--}}
                                                            {{--<i class="fa fa-bolt"></i>--}}
                                                        {{--</span> Server #12 overloaded. </span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<a href="javascript:;">--}}
                                            {{--<span class="time">10 mins</span>--}}
                                            {{--<span class="details">--}}
                                                        {{--<span class="label label-sm label-icon label-warning">--}}
                                                            {{--<i class="fa fa-bell-o"></i>--}}
                                                        {{--</span> Server #2 not responding. </span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<a href="javascript:;">--}}
                                            {{--<span class="time">14 hrs</span>--}}
                                            {{--<span class="details">--}}
                                                        {{--<span class="label label-sm label-icon label-info">--}}
                                                            {{--<i class="fa fa-bullhorn"></i>--}}
                                                        {{--</span> Application error. </span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<a href="javascript:;">--}}
                                            {{--<span class="time">2 days</span>--}}
                                            {{--<span class="details">--}}
                                                        {{--<span class="label label-sm label-icon label-danger">--}}
                                                            {{--<i class="fa fa-bolt"></i>--}}
                                                        {{--</span> Database overloaded 68%. </span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<a href="javascript:;">--}}
                                            {{--<span class="time">3 days</span>--}}
                                            {{--<span class="details">--}}
                                                        {{--<span class="label label-sm label-icon label-danger">--}}
                                                            {{--<i class="fa fa-bolt"></i>--}}
                                                        {{--</span> A user IP blocked. </span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<a href="javascript:;">--}}
                                            {{--<span class="time">4 days</span>--}}
                                            {{--<span class="details">--}}
                                                        {{--<span class="label label-sm label-icon label-warning">--}}
                                                            {{--<i class="fa fa-bell-o"></i>--}}
                                                        {{--</span> Storage Server #4 not responding dfdfdfd. </span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<a href="javascript:;">--}}
                                            {{--<span class="time">5 days</span>--}}
                                            {{--<span class="details">--}}
                                                        {{--<span class="label label-sm label-icon label-info">--}}
                                                            {{--<i class="fa fa-bullhorn"></i>--}}
                                                        {{--</span> System Error. </span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<a href="javascript:;">--}}
                                            {{--<span class="time">9 days</span>--}}
                                            {{--<span class="details">--}}
                                                        {{--<span class="label label-sm label-icon label-danger">--}}
                                                            {{--<i class="fa fa-bolt"></i>--}}
                                                        {{--</span> Storage server failed. </span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    <!-- END NOTIFICATION DROPDOWN -->
                    {{--<li class="separator hide"> </li>--}}
                    <!-- BEGIN INBOX DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    {{--<li class="dropdown dropdown-extended dropdown-inbox dropdown-dark" id="header_inbox_bar">--}}
                        {{--<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">--}}
                            {{--<i class="icon-envelope-open"></i>--}}
                            {{--<span class="badge badge-danger"> 4 </span>--}}
                        {{--</a>--}}
                        {{--<ul class="dropdown-menu">--}}
                            {{--<li class="external">--}}
                                {{--<h3>You have--}}
                                    {{--<span class="bold">7 New</span> Messages</h3>--}}
                                {{--<a href="app_inbox.html">view all</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">--}}
                                    {{--<li>--}}
                                        {{--<a href="#">--}}
                                                    {{--<span class="photo">--}}
                                                        {{--<img src="{{ asset('backend/layouts/layout4/img/avatar2.jpg') }}" class="img-circle" alt=""> </span>--}}
                                            {{--<span class="subject">--}}
                                                        {{--<span class="from"> Lisa Wong </span>--}}
                                                        {{--<span class="time">Just Now </span>--}}
                                                    {{--</span>--}}
                                            {{--<span class="message"> Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... </span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<a href="#">--}}
                                                    {{--<span class="photo">--}}
                                                        {{--<img src="{{ asset('backend/layouts/layout4/img/avatar3.jpg') }}" class="img-circle" alt=""> </span>--}}
                                            {{--<span class="subject">--}}
                                                        {{--<span class="from"> Richard Doe </span>--}}
                                                        {{--<span class="time">16 mins </span>--}}
                                                    {{--</span>--}}
                                            {{--<span class="message"> Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<a href="#">--}}
                                                    {{--<span class="photo">--}}
                                                        {{--<img src="{{ asset('backend/layouts/layout4/img/avatar1.jpg') }}" class="img-circle" alt=""> </span>--}}
                                            {{--<span class="subject">--}}
                                                        {{--<span class="from"> Bob Nilson </span>--}}
                                                        {{--<span class="time">2 hrs </span>--}}
                                                    {{--</span>--}}
                                            {{--<span class="message"> Vivamus sed nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<a href="#">--}}
                                                    {{--<span class="photo">--}}
                                                        {{--<img src="{{ asset('backend/layouts/layout4/img/avatar2.jpg') }}" class="img-circle" alt=""> </span>--}}
                                            {{--<span class="subject">--}}
                                                        {{--<span class="from"> Lisa Wong </span>--}}
                                                        {{--<span class="time">40 mins </span>--}}
                                                    {{--</span>--}}
                                            {{--<span class="message"> Vivamus sed auctor 40% nibh congue nibh... </span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<a href="#">--}}
                                                    {{--<span class="photo">--}}
                                                        {{--<img src="{{ asset('backend/layouts/layout4/img/avatar3.jpg') }}" class="img-circle" alt=""> </span>--}}
                                            {{--<span class="subject">--}}
                                                        {{--<span class="from"> Richard Doe </span>--}}
                                                        {{--<span class="time">46 mins </span>--}}
                                                    {{--</span>--}}
                                            {{--<span class="message"> Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    <!-- END INBOX DROPDOWN -->
                    {{--<li class="separator hide"> </li>--}}
                    <!-- BEGIN TODO DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    {{--<li class="dropdown dropdown-extended dropdown-tasks dropdown-dark" id="header_task_bar">--}}
                        {{--<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">--}}
                            {{--<i class="icon-calendar"></i>--}}
                            {{--<span class="badge badge-primary"> 3 </span>--}}
                        {{--</a>--}}
                        {{--<ul class="dropdown-menu extended tasks">--}}
                            {{--<li class="external">--}}
                                {{--<h3>You have--}}
                                    {{--<span class="bold">12 pending</span> tasks</h3>--}}
                                {{--<a href="?p=page_todo_2">view all</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">--}}
                                    {{--<li>--}}
                                        {{--<a href="javascript:;">--}}
                                                    {{--<span class="task">--}}
                                                        {{--<span class="desc">New release v1.2 </span>--}}
                                                        {{--<span class="percent">30%</span>--}}
                                                    {{--</span>--}}
                                            {{--<span class="progress">--}}
                                                        {{--<span style="width: 40%;" class="progress-bar progress-bar-success" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">--}}
                                                            {{--<span class="sr-only">40% Complete</span>--}}
                                                        {{--</span>--}}
                                                    {{--</span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<a href="javascript:;">--}}
                                                    {{--<span class="task">--}}
                                                        {{--<span class="desc">Application deployment</span>--}}
                                                        {{--<span class="percent">65%</span>--}}
                                                    {{--</span>--}}
                                            {{--<span class="progress">--}}
                                                        {{--<span style="width: 65%;" class="progress-bar progress-bar-danger" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100">--}}
                                                            {{--<span class="sr-only">65% Complete</span>--}}
                                                        {{--</span>--}}
                                                    {{--</span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<a href="javascript:;">--}}
                                                    {{--<span class="task">--}}
                                                        {{--<span class="desc">Mobile app release</span>--}}
                                                        {{--<span class="percent">98%</span>--}}
                                                    {{--</span>--}}
                                            {{--<span class="progress">--}}
                                                        {{--<span style="width: 98%;" class="progress-bar progress-bar-success" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100">--}}
                                                            {{--<span class="sr-only">98% Complete</span>--}}
                                                        {{--</span>--}}
                                                    {{--</span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<a href="javascript:;">--}}
                                                    {{--<span class="task">--}}
                                                        {{--<span class="desc">Database migration</span>--}}
                                                        {{--<span class="percent">10%</span>--}}
                                                    {{--</span>--}}
                                            {{--<span class="progress">--}}
                                                        {{--<span style="width: 10%;" class="progress-bar progress-bar-warning" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">--}}
                                                            {{--<span class="sr-only">10% Complete</span>--}}
                                                        {{--</span>--}}
                                                    {{--</span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<a href="javascript:;">--}}
                                                    {{--<span class="task">--}}
                                                        {{--<span class="desc">Web server upgrade</span>--}}
                                                        {{--<span class="percent">58%</span>--}}
                                                    {{--</span>--}}
                                            {{--<span class="progress">--}}
                                                        {{--<span style="width: 58%;" class="progress-bar progress-bar-info" aria-valuenow="58" aria-valuemin="0" aria-valuemax="100">--}}
                                                            {{--<span class="sr-only">58% Complete</span>--}}
                                                        {{--</span>--}}
                                                    {{--</span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<a href="javascript:;">--}}
                                                    {{--<span class="task">--}}
                                                        {{--<span class="desc">Mobile development</span>--}}
                                                        {{--<span class="percent">85%</span>--}}
                                                    {{--</span>--}}
                                            {{--<span class="progress">--}}
                                                        {{--<span style="width: 85%;" class="progress-bar progress-bar-success" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100">--}}
                                                            {{--<span class="sr-only">85% Complete</span>--}}
                                                        {{--</span>--}}
                                                    {{--</span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<a href="javascript:;">--}}
                                                    {{--<span class="task">--}}
                                                        {{--<span class="desc">New UI release</span>--}}
                                                        {{--<span class="percent">38%</span>--}}
                                                    {{--</span>--}}
                                            {{--<span class="progress progress-striped">--}}
                                                        {{--<span style="width: 38%;" class="progress-bar progress-bar-important" aria-valuenow="18" aria-valuemin="0" aria-valuemax="100">--}}
                                                            {{--<span class="sr-only">38% Complete</span>--}}
                                                        {{--</span>--}}
                                                    {{--</span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    <!-- END TODO DROPDOWN -->
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <span class="username username-hide-on-mobile">
                                {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}
                            </span>
                            <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                            <img src="{{ asset(Auth::user()->pic) }}" class="img-circle" alt="">
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="{{ route('user.settings', Auth::user()->id) }}">
                                    <i class="icon-user"></i> @lang('dashboard.my_profile') </a>
                            </li>
                            {{--<li>--}}
                                {{--<a href="app_calendar.html">--}}
                                    {{--<i class="icon-calendar"></i> My Calendar </a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="app_inbox.html">--}}
                                    {{--<i class="icon-envelope-open"></i> My Inbox--}}
                                    {{--<span class="badge badge-danger"> 3 </span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="app_todo_2.html">--}}
                                    {{--<i class="icon-rocket"></i> My Tasks--}}
                                    {{--<span class="badge badge-success"> 7 </span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li class="divider"> </li>--}}
                            {{--<li>--}}
                                {{--<a href="page_user_lock_1.html">--}}
                                    {{--<i class="icon-lock"></i> Lock Screen </a>--}}
                            {{--</li>--}}
                            <li>
                                <a href="{{ route('logout') }}">
                                    <i class="icon-key"></i> @lang('main.logout') </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->

</div>
<!-- END HEADER -->
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
        <!-- BEGIN SIDEBAR -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <div class="page-sidebar navbar-collapse collapse">
            <!-- BEGIN SIDEBAR MENU -->
            <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
            <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
            <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
            <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
            <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
            <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
            <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                <li class="nav-item start ">
                    @if(auth()->user()->hasRole('owner'))
                    <a href="{{ route('owner.dashboard', auth()->user()->company->slug) }}" class="nav-link ">
                    @else
                    <a href="{{ route('dashboard') }}" class="nav-link ">
                    @endif
                        <i class="icon-bar-chart"></i>
                        <span class="title">@lang('main.dashboard')</span>
                    </a>
                </li>
                <li class="heading">
                    <h3 class="uppercase">@lang('dashboard.business')</h3>
                </li>
                <li class="nav-item  ">
                    @if(auth()->user()->hasRole('owner'))
                    <a href="{{ route('owner.tickets', auth()->user()->company->slug) }}" class="nav-link ">
                    @else
                    <a href="{{ route('tickets.list') }}" class="nav-link ">
                    @endif
                        <i class="fa fa-ticket"></i>
                        <span class="title">@lang('main.tickets')</span>
                    </a>
                </li>
                <li c
                <li class="heading">
                    <h3 class="uppercase">@lang('dashboard.management')</h3>
                </li>
                @if(\Auth::user()->hasRole('admin') || \Auth::user()->hasRole('moderator'))
                <li class="nav-item  ">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-users"></i>
                        <span class="title">@lang('dashboard.users')</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="{{ route('users.list') }}" class="nav-link ">
                                <span class="title">@lang('main.list')</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="{{ route('users.create') }}" class="nav-link ">
                                <span class="title">@lang('main.create')</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item  ">
                    <a href="{{ route('companies.list') }}" class="nav-link nav-toggle">
                        <i class="fa fa-star"></i>
                        <span class="title">@lang('dashboard.companies')</span>
                    </a>
                </li>
                @endif
                <li class="nav-item  ">
                    @if(auth()->user()->hasRole('owner'))
                    <a href="{{ route('owner.courses', auth()->user()->company->slug) }}" class="nav-link ">
                    @else
                    <a href="{{ route('courses.list') }}" class="nav-link ">
                    @endif
                        <i class="fa fa-map-signs"></i>
                        <span class="title">@lang('main.courses')</span>
                    </a>
                </li>
                @if(auth()->user()->hasRole('owner'))
                    <li class="nav-item">
                        <a href="{{ route('owner.settings', auth()->user()->company->slug) }}" class="nav-link nav-toggle">
                            <i class="icon-settings"></i>
                            <span class="title">@lang('dashboard.settings')</span>
                        </a>
                    </li>
                @endif
                @if(auth()->user()->hasRole('admin|moderator'))
                <li class="nav-item  ">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-file-text"></i>
                        <span class="title">Log</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="#" class="nav-link ">
                                <span class="title">All</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="table_static_responsive.html" class="nav-link ">
                                <span class="title">Companies</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="table_bootstrap.html" class="nav-link ">
                                <span class="title">Courses</span>
                            </a>
                        </li>
                        @if(auth()->user()->hasRole('admin'))
                        <li class="nav-item  ">
                            <a href="table_bootstrap.html" class="nav-link ">
                                <span class="title">Staff</span>
                            </a>
                        </li>
                        @endif
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-settings"></i>
                        <span class="title">Settings</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-globe"></i> Geography
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item">
                                    <a href="{{ route('settings.cities') }}" class="nav-link">
                                        <i class="icon-home"></i> @lang('dashboard.cities')
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('settings.places') }}" class="nav-link">
                                        <i class="fa fa-map-pin"></i> @lang('dashboard.locations')</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/translations') }}" class="nav-link">
                                <i class="fa fa-map"></i> @lang('dashboard.translations')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('settings.userRoles') }}" class="nav-link">
                                <i class="fa fa-lock"></i> @lang('dashboard.user_roles')
                            </a>
                        </li>
                    </ul>
                </li>
                @endif
            </ul>
            <!-- END SIDEBAR MENU -->
        </div>
        <!-- END SIDEBAR -->
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            @yield('content')
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN QUICK SIDEBAR -->
    {{-- @include('backend.shared.sidebar') --}}
    <!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner"> 2016 &copy; Metronic Theme By
        <a target="_blank" href="http://keenthemes.com">Keenthemes</a> &nbsp;|&nbsp;
        <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
<!-- BEGIN QUICK NAV -->
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="https://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" target="_blank" class="active">
                <span>Purchase Metronic</span>
                <i class="icon-basket"></i>
            </a>
        </li>
        <li>
            <a href="https://themeforest.net/item/metronic-responsive-admin-dashboard-template/reviews/4021469?ref=keenthemes" target="_blank">
                <span>Customer Reviews</span>
                <i class="icon-users"></i>
            </a>
        </li>
        <li>
            <a href="http://keenthemes.com/showcast/" target="_blank">
                <span>Showcase</span>
                <i class="icon-user"></i>
            </a>
        </li>
        <li>
            <a href="http://keenthemes.com/metronic-theme/changelog/" target="_blank">
                <span>Changelog</span>
                <i class="icon-graph"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>

@if(session()->has('messages.0'))
    @foreach(session('messages') as $key => $message)
        <?php $width = ($key + 1) + 100 + ($key * 70); ?>
        <style>
            .alert-style{{ $key }} {
                position: fixed; margin: 0px; z-index: 9999; top: {{ $width }}px; right: 20px;
            }
        </style>
        <div class="bootstrap-growl alert alert-{{ $message['level'] }} alert-dismissible alert-style{{ $key }}">
            <button class="close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span><span class="sr-only">Close</span>
            </button>
            {!! $message['content'] !!}
        </div>
    @endforeach
@endif
<!-- END QUICK NAV -->

<script src="{{ asset('backend/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend//global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>


<script src="{{ asset('backend/global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/global/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/global/plugins/jstree/dist/jstree.min.js') }}" type="text/javascript"></script>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{ asset('backend/global/scripts/app.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/pages/scripts/table-datatables-buttons.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/pages/scripts/form-validation-md.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/pages/scripts/profile.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/pages/scripts/ui-bootbox.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/pages/scripts/ui-tree.js') }}" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="{{ asset('backend/layouts/layout4/scripts/layout.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/layouts/layout4/scripts/demo.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/layouts/global/scripts/quick-nav.min.js') }}" type="text/javascript"></script>
{{-- Custom JS File --}}
<script src="{{ asset('backend/global/scripts/custom.js') }}" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script>
    $(document).ready(function()
    {
        $('#clickmewow').click(function()
        {
            $('#radio1003').attr('checked', 'checked');
        });
    })
    $('div.alert').delay(5000).fadeOut(350);
</script>
</body>

</html>