@extends('backend/backend')

@section('content')
    <link href="{{ asset('backend/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>@lang('dashboard.cities')
                <small>@lang('main.welcome_to_dashboard')</small>
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <div class="row">
        <div class="col-md-2">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-map"></i>
                        <span class="caption-subject font-blue-sharp bold uppercase">@lang('dashboard.user_roles')</span>
                    </div>
                </div>
                <div class="portlet-body">
                    @foreach($roles as $rol)
                        <h4><a href="{{ route('settings.userRolePermissions', $rol->name) }}" class="padding-10 loadPermissions" id="{{ $rol->id }}">
                                @lang('dashboard.'.$rol->name)</a></h4>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-map"></i>
                        <span class="caption-subject font-blue-chambray bold uppercase">@lang('dashboard.role_permissions')
                            @if($role) - <span class="font-blue-sharp">@lang('dashboard.'.$role->name)</span> @endif</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                    @if($allPermissions)
                        <div class="md-checkbox-inline">
                            @foreach($allPermissions as $permission)
                                <div class="col-md-3">
                                    <input type="checkbox" value="1" data-type="role" data-role="{{ $role->id }}" data-permission="{{ $permission->name }}"
                                           class="changePermission" @if($role->hasPermissionTo($permission->name)) checked @endif />
                                        @lang('dashboard.'.$permission->name)
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="col-md-12">
                            <h4 class="" id="select-role-text">@lang('dashboard.select_role')</h4>
                        </div>
                    @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-plus"></i>
                        <span class="caption-subject font-blue-sharp bold uppercase">@lang('dashboard.add_role')</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <form method="post" role="form" action="{{ route('settings.addRole') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>@lang('dashboard.slug')</label>
                            <input type="text" class="form-control" name="slug" />
                        </div>
                        <button type="submit" class="btn btn-block blue"><i class="fa fa-floppy-o"></i>  @lang('main.save')</button>
                    </form>
                </div>
            </div>
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-plus"></i>
                        <span class="caption-subject font-blue-sharp bold uppercase">@lang('dashboard.add_permission')</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <form method="post" role="form" action="{{ route('settings.addPermission') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>@lang('dashboard.slug')</label>
                            <input type="text" class="form-control" name="name" />
                        </div>
                        <button type="submit" class="btn btn-block blue"><i class="fa fa-floppy-o"></i>  @lang('main.save')</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection