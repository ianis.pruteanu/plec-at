@extends('backend/backend')

@section('content')
    <link href="{{ asset('backend/global/plugins/jstree/dist/themes/default/style.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>@lang('dashboard.cities')
                <small>@lang('main.welcome_to_dashboard')</small>
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <div class="row">
        <div class="col-md-8">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-map"></i>
                        <span class="caption-subject font-blue-sharp bold uppercase">@lang('dashboard.countries_list')</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-cloud-upload"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-wrench"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-trash"></i>
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="tree_1" class="tree-demo">
                        <ul>
                            <li data-jstree='{ "opened" : true  }'> @lang('dashboard.countries')
                                <ul>
                                    @foreach($countries as $country)
                                        <li data-jstree=''> <i class="flag-icon flag-icon-gr"></i> @lang('geography.'.$country->slug)
                                        <ul>
                                            @foreach($country->cities as $city)
                                            <li data-jstree='{ }'> @lang('geography.'.$city->slug)
                                                <ul>
                                                    @foreach($city->places as $place)
                                                        <li data-jstree='{ "icon" : "fa fa-home", "disabled" : true }'> {{ $place->name }} </li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-plus"></i>
                        <span class="caption-subject font-blue-sharp bold uppercase">@lang('dashboard.add_city')</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <form method="post" role="form" action="{{ route('settings.addCity') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>@lang('dashboard.country')</label>
                            <select class="select2" name="country">
                                @foreach($countries as $country)
                                    <option value="{{ $country->id }}">@lang('geography.'.$country->slug)</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>@lang('dashboard.slug')</label>
                            <input type="text" class="form-control" name="slug" />
                        </div>
                        <button type="submit" class="btn btn-block blue"><i class="fa fa-floppy-o"></i>  @lang('main.save')</button>
                    </form>
                </div>
            </div>
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-plus"></i>
                        <span class="caption-subject font-blue-sharp bold uppercase">@lang('dashboard.add_location')</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <form method="post" role="form" action="{{ route('settings.addLocation') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>@lang('dashboard.city')</label>
                            <select class="select2" name="city">
                                @foreach($countries as $country)
                                    <optgroup label="@lang('geography.'.$country->slug)">
                                        @foreach($country->cities as $city)
                                            <option value="{{ $city->id }}">@lang('geography.'.$city->slug)</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>@lang('dashboard.slug')</label>
                            <input type="text" class="form-control" name="slug" />
                        </div>
                        <button type="submit" class="btn btn-block blue"><i class="fa fa-floppy-o"></i>  @lang('main.save')</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection