<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta name="description" content="plec.at - ticket">
	<meta name="author" content="plec.at">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
</head>
<body>

<img src="<?php echo $message->embed(public_path().'/images/logo_plecat.png'); ?>" />
<h3>{{ $user->first_name }}, îți mulțumim că ai ales serviciul nostru.</h3>
<h4>Mai jos ai link-ul de confirmare al biletului.</h4>
<h4>După confirmarea biletului vei primi un email cu biletul atașat.</h4>
<div>
	{{ \UrlShortener::shorten(URL::to('api/ticket/validate/'.$ticket->confirmation_token)) }}<br/>
</div>

</body>
</html>