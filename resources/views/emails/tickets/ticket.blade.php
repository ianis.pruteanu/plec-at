<!DOCTYPE html>
<html>
<head>
	<meta name="description" content="plec.at - ticket">
	<meta name="author" content="plec.at">
	<title>plec.at - ticket</title>
	<link href="{{ asset('backend/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('backend/global/css/ticket.css') }}" rel="stylesheet" type="text/css" >
</head>

<body>
<div class="container-fluid">
	<div class="top-line">
		<div class="col-md-6">
			<h3>BILET: {{ $ticket->code }} <span class="pull-right">DATA REZERVĂRII: {{ \Carbon\Carbon::parse($ticket->created_at)->setTimezone
			('Europe/Bucharest')
			->format('d/m/Y H:i') }}</span></h3>
		</div>
	</div>
	<br>
	<div class="col-md-12">
		<div class="row passenger">
			<div class="col-sm-4">
				<h4>NUME</h4>
				<h3 class="uppercase">
					@if($ticket->otherName())

					@else
						{{ $ticket->user->first_name }} {{ $ticket->user->last_name }}
					@endif
				</h3>
			</div>
			<div class="col-sm-4">
				@if($ticket->details()->where('information_type', 'phone_number')->first())
					<h4>TELEFON</h4>
					<h3 class="uppercase">{{ $ticket->details()->where('information_type', 'phone_number')->first()->information_value }}</h3>
				@elseif($ticket->user->phone)
					<h4>TELEFON</h4>
					<h3 class="uppercase">{{ $ticket->user->phone }}</h3>
				@else
					<h4>E-MAIL</h4>
					<h3 class="uppercase">{{ $ticket->user->email }}</h3>
				@endif
			</div>
			<div class="col-sm-4">
				<img src="{{ asset('images/logo_plecat.png') }}" width="190">
			</div>
		</div>
		<div class="row trip">
			<div class="col-sm-3">
				<h4>DATA ȘI ORA PLECĂRII</h4>
				<h3 class="uppercase"> {{ \Carbon\Carbon::parse($ticket->date)->format('d/m/Y') }}, {{ \Carbon\Carbon::parse($ticket->trip->departure_time)
				->format('H:i') }}</h3>
			</div>
			<div class="col-sm-5">
				<h4>PORNIRE</h4>
				<h3 class="uppercase">{!! trans('geography.'.$ticket->trip->departureCity->slug) !!} /<span> {{ $ticket->trip->departurePlace->name
				}}</span></h3>
			</div>
			<div class="col-sm-4">
				<h4>PREȚ</h4>
				<h3 class="uppercase">{{ $ticket->trip->firstPrice()->amount }}<span> {{ $ticket->trip->firstPrice()->currency_index }}</span></h3>
			</div>
		</div>
		<div class="row trip">
			<div class="col-sm-3">
				<h4>DATA ȘI ORA SOSIRII</h4>
				<h3 class="uppercase"> {{ \Carbon\Carbon::parse($ticket->date)->addDays($ticket->trip->duration)->format('d/m/Y') }}, {{ \Carbon\Carbon::parse
				($ticket->trip->arrival_time)->format('H:i') }}</h3>
			</div>
			<div class="col-sm-5">
				<h4>SOSIRE</h4>
				<h3 class="uppercase">{!! trans('geography.'.$ticket->trip->arrivalCity->slug) !!} /<span> {{ $ticket->trip->arrivalPlace->name }}</span></h3>
			</div>
			<div class="col-sm-4 text-left">
				<div class="status">
					<h4>REZERVAT</h4>
				</div>
			</div>
		</div>
	</div>

</div><!-- /.container -->

</body></html>