$(document).ready(function() {
    /* new owner form switcher */
    $('#new_owner').on('change', function(e) {
        e.preventDefault();
        $('.new-owner-form').toggleClass('hidden');
        if ($('#new_owner').is(':checked')) {
            $('#owner-select').attr('disabled', true);
        } else {
            $('#owner-select').attr('disabled', false);
        }
    });
    $('.getPlaces').on('change', function(e) {
        e.preventDefault();
        let selectedDropdown = $(this).attr('name')
        $.ajax({
            type: "GET",
            url: '/dashboard/courses/getPlaces/' + $(this).val(),
            success: function(data) {
                if(selectedDropdown == 'departure_city'){
                    var placesSelect = $('#departurePlace');
                }else if(selectedDropdown == 'arrival_city'){
                    var placesSelect = $('#arrivalPlace');
                }
                placesSelect.empty();
                placesSelect.attr('disabled', false);
                $.each(data, function(key, element) {
                    placesSelect.append($("<option></option>").attr("value", element.id).text(element.name));
                });
            }
        });
    });
    $('#departureCityTrip').on('change', function(e) {
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: '/dashboard/courses/getPlaces/' + $(this).val(),
            success: function(data) {
                var placesSelect = $('#departurePlacesTrip');
                placesSelect.empty();
                placesSelect.attr('disabled', false);
                $.each(data, function(key, element) {
                    placesSelect.append($("<option></option>").attr("value", element.id).text(element.name));
                });
            }
        });
    });
    $('#arrivalCityTrip').on('change', function(e) {
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: '/dashboard/courses/getPlaces/' + $(this).val(),
            success: function(data) {
                var placesSelect = $('#arrivalPlacesTrip');
                placesSelect.empty();
                placesSelect.attr('disabled', false);
                $.each(data, function(key, element) {
                    placesSelect.append($("<option></option>").attr("value", element.id).text(element.name));
                });
            }
        });
    });
    $('.price_submit').click(function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var currency = $(".currency" + id).val();
        var amount = $('.amount' + id).val();
        $.ajax({
            type: "POST",
            url: "/dashboard/trips/addNewPrice/" + id + "/" + currency + "/" + amount,
            processData: false,
            contentType: false,
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                $(".price_list" + id).append("<p>" + amount + " " + currency + "</p>");
            }
        });
    });
    /*** Frontend ***/
    $('#departureCity').on('change', function(e) {
        e.preventDefault();
        // var course = $(this).data('course');
        $.ajax({
            type: "GET",
            url: '/courses/getArrivals/' + $(this).val(),
            success: function(data) {
                var arrivals = $('#arrivalCityFe');
                arrivals.empty();
                arrivals.attr('disabled', false);
                arrivals.append($("<option></option>"));
                $.each(data, function(key, element) {
                    console.debug(element.arrival_city);
                    arrivals.append($("<option></option>").attr("value", element.arrival_city.id).text(element.arrival_city[0]));
                });
            }
        });
    });
    $('#arrivalCityFe').on('change', function(e) {
        e.preventDefault();
        var departure = $('#departureCity').val();
        var arrival = $(this).val();
        var course = $(this).data('course');
        $.ajax({
            type: "GET",
            url: "/courses/getTrip/" + course + "/" + departure + "/" + arrival,
            success: function(data) {
                $('#tripInput').val(data.id);
            }
        });
    });
    // JS switch btw user and company register form
    $('.checkbox_radio_con').click(function() {
        if (document.getElementById('as_user').checked) {
            $('.user_register_form').show();
            $('.company_register_form').hide();
        } else if (document.getElementById('as_company').checked) {
            $('.user_register_form').hide();
            $('.company_register_form').show();
        }
    });
    // REGISTER USER FORNTEND
    $(".send_button.full_button").on("click", function(e) {
        e.preventDefault();
        var recaptcha_response = grecaptcha.getResponse();
        if (document.getElementById('as_user').checked) {
            let first_name = $('#first_name').val();
            let last_name = $('#last_name').val();
            let email_and_phone = $('#email_phone').val();
            let email = '';
            let phone = '';
            if (validateEmail(email_and_phone)) {
                email = email_and_phone;
            } else {
                phone = email_and_phone;
            }
            let password = $('#password').val();
            let password_confirmation = $('#confirm_password').val();
            $.ajax({
                url: '/register-frontend-user',
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    first_name: first_name,
                    last_name: last_name,
                    email: email,
                    phone: phone,
                    password: password,
                    password_confirmation: password_confirmation,
                    recaptcha_response: recaptcha_response
                },
                success: function(data) {
                    let myDiv = $(data).find('.alert.alert-danger');
                    if ($('.alert.alert-danger').length > 0) {
                        $('.alert.alert-danger').remove();
                        $('.checkbox_radio_con').append(myDiv);
                    } else {
                        $('.checkbox_radio_con').append(myDiv);
                    }
                    // redirect if no error messages
                    if ($('.alert.alert-danger').length == 0) {
                        window.location.href = "/user/" + data + "/settings";
                    }
                    // reset recaptcha
                    grecaptcha.reset();
                },
            });
        }
    });
    // create booking and open create user popup
    $('.create_booking_button.userNotLoggedIn').click(function(e) {
        e.preventDefault();
        let from = $.trim($('#select2-departureCity-container').text());
        let to = $.trim($('#select2-arrivalCityFe-container').text());
        let quantity = $('input[name=quantity]').val();
        let date = $('input[name=date]').val();
        let first_name = $('.p-t-b-40 input[name=first_name]').val();
        let last_name = $('.p-t-b-40 input[name=last_name]').val();
        let email_and_phone = $('.p-t-b-40 input[name=email_phone]').val()
        let email = '';
        let phone = '';
        if (validateEmail(email_and_phone)) {
            email = email_and_phone;
        } else {
            phone = email_and_phone;
        }
        $.ajax({
            url: '/validate-booking',
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                from: from,
                to: to,
                quantity: quantity,
                date: date,
                first_name: first_name,
                last_name: last_name,
                email: email,
                phone: phone,
            },
            success: function(data) {
                let myDiv = $(data).find('.alert.alert-danger');
                if ($('.alert.alert-danger').length > 0) {
                    $('.alert.alert-danger').remove();
                    $('.col-md-6.p-t-b-40').prepend(myDiv);
                } else {
                    $('.col-md-6.p-t-b-40').prepend(myDiv);
                }
                // open save password popup if no error messages
                if ($('.alert.alert-danger').length == 0) {
                    $('.popup-with-move-anim.open_set_password_popup_link').click()
                } else if (($('.alert.alert-danger li').text() == 'The phone has already been taken.') || ($('.alert.alert-danger li').text() == 'The email has already been taken.')) {
                    // open login popup
                    $('.open_course_login_form').click();
                    // fill out the log form inputs
                    $('.course_login_popup #login_user_name').val(email);
                    $('.course_login_popup #login_password').val('');
                }
                // set inputs with my data
                $('.set_password_popup input[name=first_name]').val(first_name);
                $('.set_password_popup input[name=last_name]').val(last_name);
                $('.set_password_popup input[name=email_phone]').val(email_and_phone);
                $('.set_password_popup input[name=password]').val('');
            }
        });
    });
    // create booking when user already registered but not logged in
    $('.course_login_popup .send_button').click(function(e) {
        e.preventDefault();
        let from = $.trim($('#select2-departureCity-container').text());
        let to = $.trim($('#select2-arrivalCityFe-container').text());
        let email = $('.course_login_popup input[name=email]').val();
        let password = $('.course_login_popup input[name=password]').val();
        let trip_id = $('#tripInput').attr('value');
        let date = $('input[name=date]').val();
        $.ajax({
            url: '/bookWithExistentUser',
            type: "GET",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                from: from,
                to: to,
                email: email,
                password: password,
                trip_id: trip_id,
                date: date
            },
            success: function(data) {
                let myDiv = $(data).find('.alert.alert-danger');
                if ($('.alert.alert-danger').length > 0) {
                    $('.alert.alert-danger').remove();
                    $('.course_login_popup .login_header').append(myDiv);
                } else {
                    $('.course_login_popup .login_header').append(myDiv);
                }
                // after success ticket booking and user registration redirect to user history page
                if ($('.alert.alert-danger').length == 0) {
                    $.ajax({
                        url: '/redirect-to-user-profile/' + email,
                        type: "GET",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {},
                        success: function(data) {
                            window.location.href = "/user/" + data + "/settings";
                        }
                    });
                }
            }
        })
    });
    // creating booking when user already loggedin
    $('.create_booking_button.userLoggedIn').click(function(e) {
        e.preventDefault();
        let from = $.trim($('#select2-departureCity-container').text());
        let to = $.trim($('#select2-arrivalCityFe-container').text());
        let quantity = $('input[name=quantity]').val();
        let date = $('input[name=date]').val();
        let trip_id = $('#tripInput').attr('value');
        let username = $('#usernameLoggedIn').val();
        $.ajax({
            url: '/bookWithLoggedUser',
            type: "GET",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                from: from,
                to: to,
                quantity: quantity,
                date: date,
                trip_id: trip_id
            },
            success: function(data) {
                let myDiv = $(data).find('.alert.alert-danger');
                if ($('.alert.alert-danger').length > 0) {
                    $('.alert.alert-danger').remove();
                    $('.col-md-6.p-t-b-40').prepend(myDiv);
                } else {
                    $('.col-md-6.p-t-b-40').prepend(myDiv);
                }
                // after success ticket booking and user registration redirect to user history page
                if ($('.alert.alert-danger').length == 0) {
                    $.ajax({
                        url: '/redirect-to-user-profile/' + username,
                        type: "GET",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {},
                        success: function(data) {
                            window.location.href = "/user/" + data + "/settings";
                        }
                    });
                }
            },
        });
    });
    // create user popup
    $('.create_booking_user').click(function(e) {
        e.preventDefault();
        let from = $.trim($('#select2-departureCity-container').text());
        let to = $.trim($('#select2-arrivalCityFe-container').text());
        let quantity = $('input[name=quantity]').val();
        let date = $('input[name=date]').val();
        let first_name = $('.set_password_popup input[name=first_name]').val();
        let last_name = $('.set_password_popup input[name=last_name]').val();
        let email_and_phone = $('.set_password_popup input[name=email_phone]').val();
        let email = '';
        let phone = '';
        if (validateEmail(email_and_phone)) {
            email = email_and_phone;
        } else {
            phone = email_and_phone;
        }
        let password = $('.set_password_popup input[name=password]').val();
        let password_confirmation = $('.set_password_popup input[name=confirm_password]').val();
        let trip_id = $('#tripInput').attr('value');
        $.ajax({
            url: '/booking',
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                from: from,
                to: to,
                quantity: quantity,
                date: date,
                first_name: first_name,
                last_name: last_name,
                email: email,
                phone: phone,
                password: password,
                password_confirmation: password_confirmation,
                trip_id: trip_id
            },
            success: function(data) {
                let myDiv = $(data).find('.alert.alert-danger');
                if ($('.alert.alert-danger').length > 0) {
                    $('.alert.alert-danger').remove();
                    $('.set_password_popup .lfc_user_row').append(myDiv);
                } else {
                    $('.set_password_popup .lfc_user_row').append(myDiv);
                }
                // after success ticket booking and user registration redirect to user history page
                if ($('.alert.alert-danger').length == 0) {
                    $.ajax({
                        url: '/redirect-to-user-profile/' + email_and_phone,
                        type: "GET",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {},
                        success: function(data) {
                            window.location.href = "/user/" + data + "/settings";
                        }
                    })
                }
            },
        });
    });
    // if click on already have an account, open login form
    $('.set_password_popup .lfc_forget_pass a').click(function(e) {
        e.preventDefault();
        $('.set_password_popup .mfp-close').click();
        setTimeout(function() {
            // your code that should run after 0.3 second
            $('.top_login a.popup-with-move-anim').click();
        }, 300);
    });
    // header login popup
    $('.login_popup .send_button').click(function(e) {
        e.preventDefault();
        let username = $('.login_popup #login_user_name').val();
        let password = $('.login_popup #login_password').val();
        $.ajax({
            url: '/login',
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                username: username,
                password: password
            },
            success: function(data) {
                let myDiv = $(data).find('.alert');
                if ($('.alert.alert-danger').length > 0) {
                    $('.alert.alert-danger').remove();
                    $('.login_popup .lfc_user_row.header').append(myDiv);
                } else {
                    $('.login_popup .lfc_user_row.header').append(myDiv);
                }
                // after success ticket booking and user registration redirect to user history page
                if ($('.alert.alert-danger').length == 0) {
                    $.ajax({
                        url: '/redirect-to-user-profile/' + username,
                        type: "GET",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {},
                        success: function(data) {
                            window.location.href = "/user/" + data + "/settings";
                        }
                    })
                }
            },
        });
    });
    // access page login form
    $('.access_page_login_form .send_button').click(function(e) {
        e.preventDefault();
        let username = $('.access_page_login_form #login_user_name').val();
        let password = $('.access_page_login_form #login_password').val()
        $.ajax({
            url: '/login',
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                username: username,
                password: password
            },
            success: function(data) {
                let myDiv = $(data).find('.alert');
                if ($('.alert.alert-danger').length > 0) {
                    $('.alert.alert-danger').remove();
                    $('.access_page_login_form .lfc_user_row.header').append(myDiv);
                } else {
                    $('.access_page_login_form .lfc_user_row.header').append(myDiv);
                }
                // after success ticket booking and user registration redirect to user history page
                if ($('.alert.alert-danger').length == 0) {
                    $.ajax({
                        url: '/redirect-to-user-profile/' + username,
                        type: "GET",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {},
                        success: function(data) {
                            window.location.href = "/user/" + data + "/settings";
                        }
                    })
                }
            },
        });
    });
    // homepage ticket select dropdowns
    $('select[name=departure_city]').on('change', function(e) {
        e.preventDefault();
        getArrivalDestionations($(this).val());
    });
    if ($('#departure_city').val() !== '') {
        getArrivalDestionations($('#departure_city').val());
    }
    // manage user contacts - email and phone
    $('.user-contacts-form button').click(function() {
        let email = $('.user-contacts-form input[name=email]').val();
        let phone = $('.user-contacts-form input[name=phone]').val();
        $.ajax({
            url: '/manageContactsEmailAndPhone',
            type: "GET",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                email: email,
                phone: phone
            },
            success: function(data) {
                let myDiv = $(data).find('.alert');
                if ($('.alert.alert-danger').length > 0) {
                    $('.alert.alert-danger').remove();
                    $('.main_title.upper.manage_contacts').append(myDiv);
                } else {
                    $('.main_title.upper.manage_contacts').append(myDiv);
                }
                if ($('.alert.alert-danger').length == 0) {
                    // show contact details and hide form
                    $('.user-contacts').removeClass('hide');
                    $('.user-contacts-form').addClass('hide');
                    $('.edit-user-contacts').removeClass('hide');
                    // change values for phone and email
                    if (email !== '') {
                        if ($('.user-contacts .user-contact-email h5').hasClass('hide')) {
                            $('.user-contacts .user-contact-email h5').removeClass('hide');
                        }
                        // if email do not exist and user add one
                        if ($('.user-contacts .user-contact-email h5').length == 0) {
                            $('.user-contacts .user-contact-email').append('<h5>' + email + '</h5>');
                        } else {
                            $('.user-contacts .user-contact-email h5').text(email);
                        }
                    } else {
                        // if email exist and user delete the user
                        if ($('.user-contacts .user-contact-email h5').length == 1) {
                            $('.user-contacts .user-contact-email h5').addClass('hide');
                        }
                    }
                    if (phone !== '') {
                        if ($('.user-contacts .user-contact-phone h5').hasClass('hide')) {
                            $('.user-contacts .user-contact-phone h5').removeClass('hide');
                        }
                        // if phone do not exist and user add one
                        if ($('.user-contacts .user-contact-phone h5').length == 0) {
                            $('.user-contacts .user-contact-phone').append('<h5>' + phone + '</h5>');
                        } else {
                            $('.user-contacts .user-contact-phone h5').text(phone);
                        }
                    } else {
                        // if email exist and user delete the user
                        if ($('.user-contacts .user-contact-phone h5').length == 1) {
                            $('.user-contacts .user-contact-phone h5').addClass('hide');
                        }
                    }
                    // after 3 sec remove success messasge
                    setTimeout(function() {
                        // your code that should run after 3 second
                        $('.alert.alert-success').addClass('hide');
                    }, 3000);
                }
            },
        });
    });
    // manage user password change
    $('.change_password .btn').click(function(e) {
        e.preventDefault();
        let actual_password = $('.change_password input[name=actual_password]').val();
        let new_password = $('.change_password input[name=new_password]').val();
        let confirm_password = $('.change_password input[name=confirm_password]').val();
        $.ajax({
            url: '/changeUserPassword',
            type: "GET",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                actual_password: actual_password,
                new_password: new_password,
                confirm_password: confirm_password
            },
            success: function(data) {
                let myDiv = $(data).find('.alert');
                if ($('.alert.alert-danger').length > 0) {
                    // emty password fields
                    $('.change_password input[name=actual_password]').val('');
                    $('.change_password input[name=new_password]').val('');
                    $('.change_password input[name=confirm_password]').val('');
                    $('.alert.alert-danger').remove();
                    $('.change_password .main_title').append(myDiv);
                } else {
                    $('.change_password .main_title').append(myDiv);
                }
                if ($('.alert.alert-success').length > 0) {
                    // emty password fields
                    $('.change_password input[name=actual_password]').val('');
                    $('.change_password input[name=new_password]').val('');
                    $('.change_password input[name=confirm_password]').val('');
                }
                // after 3 sec remove success messasge
                setTimeout(function() {
                    // your code that should run after 3 second
                    $('.alert.alert-success').addClass('hide');
                }, 3000);
            }
        });
    });
    // on course change from dropdown, get the new course departure time
    $('#departureCity, #arrivalCityFe').change(function(e) {
        e.preventDefault();
        let departure_city = $('#departureCity').val();
        let arrival_city = $('#arrivalCityFe').val();
        $.ajax({
            url: '/getCourseDepartureTime',
            type: "GET",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                departure_city: departure_city,
                arrival_city: arrival_city,
            },
            success: function(data) {
                var departureTimes = $('#departure_hour');
                departureTimes.empty();
                departureTimes.attr('disabled', false);
                departureTimes.append($("<option></option>"));
                let departureTime_length = data['departure_time'].length;
                $.each(data['departure_time'], function(key, element) {
                    // set tripId
                    $('#tripInput').val(data['tripId']);
                    // set course departure tiem dropdown
                    if (departureTime_length == 1) {
                        departureTimes.append($("<option selected></option>").attr("value", key).text(element.substring(0, 5)));
                        $('#departure_hour').attr('disabled', 'disabled');
                    } else {
                        departureTimes.append($("<option></option>").attr("value", key).text(element.substring(0, 5)));
                        $('#departure_hour option[value=0]').attr('selected', 'selected');
                    }
                });
                // set active just some dates in datapicker
                courseAvailableDates(data['frequency'], data['created_at']);
            }
        });
    });
    // on page load set datepicker dates from frequency input
    window.onload = function() {
        if (window.location.pathname.includes('course')) {
            let frequency = $('#freqInput').val();
            // set active just some dates in datapicker
            courseAvailableDates(frequency);
        }
    };
    // set active/disabled dates on datepicker calendar
    function courseAvailableDates(frequencyData, created_at) {
        let curentMonthNumberOfDays = daysInMonth(new Date().getMonth(), new Date().getFullYear());
        $('#date, .date.date-picker span').datepicker('remove');
        let count = 0;
        if (frequencyData == 1) {
            $('#date, .date.date-picker span').datepicker({
                todayHighlight: true,
                startDate: 'now',
                autoclose: true,
            });
        } else if (frequencyData == 2) {
            $('#date, .date.date-picker span').datepicker({
                todayHighlight: true,
                startDate: 'now',
                autoclose: true,
                beforeShowDay: disableEverySecondDay
            });
        } else if (frequencyData == 3) {
            $('#date, .date.date-picker span').datepicker({
                todayHighlight: true,
                startDate: 'now',
                autoclose: true,
                beforeShowDay: disableOddDays
            });
        } else if (frequencyData == 4) {
            $('#date, .date.date-picker span').datepicker({
                todayHighlight: true,
                startDate: 'now',
                autoclose: true,
                beforeShowDay: disableEvenDays
            });
        } else {
            $('#date, .date.date-picker span').datepicker({
                todayHighlight: true,
                startDate: 'now',
                autoclose: true,
                beforeShowDay: disableCustomDaysOfTheWeek
            });
        }
        // disable odd days
        function disableOddDays(date) {
            var day = date.getDate();
            if (day % 2 == 1) return true;
            else return false;
        }
        // disable even days
        function disableEvenDays(date) {
            var day = date.getDate();
            if (day % 2 == 0) return true;
            else return false;
        }
        // disable every second day
        function disableEverySecondDay(date) { //:TODO
            var day = date.getDate();
            let numberOfDaysOfMonth = daysInMonth(date.getMonth(), date.getFullYear());
            // console.log(created_at % 2);
            // if (created_at % 2 == 0) {
            //     // check if last day of month is disabled
            //     if (numberOfDaysOfMonth % 2 == 1) {
            //         if (day % 2 == 0) return false;
            //         else return true;
            //     }else{
            //         if (day % 2 == 1) return false;
            //         else return true;
            //     }
            // }
            if (day == 1) {
                if (day % 2 == 0) return false;
                else return true;
            } else {
                if ((day + numberOfDaysOfMonth) % 2 == 0) return true;
                else return false;
            }
        }
        // disable custom days of the week
        function disableCustomDaysOfTheWeek(date) {
            let mon = frequencyData[0];
            let tue = frequencyData[1];
            let wed = frequencyData[2];
            let thu = frequencyData[3];
            let fri = frequencyData[4];
            let sat = frequencyData[5];
            let sun = frequencyData[6];
            var day = date.getDay();
            if (((day == 1) && (mon == 1)) || ((day == 2) && (tue == 1)) || ((day == 3) && (wed == 1)) || ((day == 4) && (thu == 1)) || ((day == 5) && (fri == 1)) || ((day == 6) && (sat == 1)) || ((day == 0) && (sun == 1))) {
                return true;
            } else {
                return false;
            }
        }
        //Month is 1 based
        function daysInMonth(month, year) {
            return new Date(year, month, 0).getDate();
        }
    }
    // get the new tripId when changeing the departure, arrivals or departure time
    $('#departure_hour').change(function(e) {
        e.preventDefault();
        let departure_city = $('#departureCity').val();
        let arrival_city = $('#arrivalCityFe').val();
        let departure_time = $.trim($('#select2-departure_hour-container').text());
        $.ajax({
            url: '/getTripId',
            type: "GET",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                departure_city: departure_city,
                arrival_city: arrival_city,
                departure_time: departure_time
            },
            success: function(data) {
                $('#tripInput').val(data);
            }
        });
    });

    function getArrivalDestionations(departureCity) {
        $.ajax({
            type: "GET",
            url: '/courses/getArrivals/' + departureCity,
            success: function(data) {
                var arrivals = $('select#arrival_city');
                arrivals.empty();
                arrivals.attr('disabled', false);
                arrivals.append($("<option></option>"));
                $.each(data, function(key, element) {
                    console.debug(element.arrival_city);
                    arrivals.append($("<option></option>").attr("value", element.arrival_city.id).text(element.arrival_city[0]));
                });
            }
        });
    }
    // email validation
    function validateEmail(email) {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    // UX Password confimation ( for Administrator and owner's actions )
    $('.roPasswordConfirm').click(function() {
        var id = $(this).data('id'),
            type = $(this).data('type'),
            action = $(this).data('action');
        bootbox.prompt({
            inputType: "password",
            title: "Confirmare parola",
            callback: function(password) {
                if (!password) {
                    return false;
                }
                $.ajax({
                    type: "POST",
                    url: "/dashboard/actions-password-confirmation/" + password,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        if (data == false) {
                            alert('Parola incorecta');
                        } else if (data == true) {
                            $.ajax({
                                type: "POST",
                                url: "/dashboard/" + action + '/' + type + '/' + id,
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                success: {}
                            })
                        } else {
                            return false;
                        }
                    }
                })
            }
        });
    });
    // edit user contacts
    $('.edit-user-contacts').on('click', function(e) {
        e.preventDefault();
        // hide  if visible success message
        if ($('.alert.alert-success').length > 0) {
            $('.alert.alert-success').addClass('hide');
        }
        $(this).toggleClass('hide');
        $('.user-contacts').toggleClass('hide');
        $('.user-contacts-form').toggleClass('hide');
    });
    $('.add-contact-form').on('click', function() {
        var y = $(this).data('order');
        $(this).attr('data-order', y + 1);
        $('.user-contacts-inputs').append("<hr /><div class='form-group'>" + "<div class='checkbox_radio_con'>" + "<input type='radio' checked value='phone' id='new-phone' name='cc" + y + "'>" + "<span> <i class='fa fa-phone'></i></span>" + "<input type='radio' value='email' id='new-email'  name='cc" + y + "' style='margin-left: 20px;'>" + "<span> <i class='fa fa-envelope'></i> </span>" + "</div>\
                <input type='email' class='form-control'>\
            </div>")
    });
    // edit user picture
    $('.edit-user-picture').on('click', function(e) {
        e.preventDefault();
        // show file uploader
        if ($('.fileUploader').hasClass('hide')) {
            $('.fileUploader').removeClass('hide');
        }
    });
    /* Bus management */
    $('#busMark').on('change', function(e) {
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: '/dashboard/buses/getModels/' + $(this).val(),
            success: function(data) {
                var busModels = $('#busModels');
                busModels.empty();
                busModels.attr('disabled', false);
                $.each(data, function(key, element) {
                    busModels.append($("<option></option>").attr("value", key).text(element));
                });
            }
        });
    });
    $('#frequency').on('change', function(e) {
        if ($(this).val() == 5) {
            $('#custom-frequency').removeClass('hide');
        } else {
            $('#custom-frequency').attr('class', 'hide');
        }
    });
    $('#frequency').on('change', function(e) {
        if ($(this).val() == 2) {
            $('#startDate').removeClass('hide');
        } else {
            $('#startDate').attr('class', 'hide');
        }
    });
    // BACKEND
    // don't allow into bus/create legal number any space
    $("input#legal_number").on({
        keydown: function(e) {
            if (e.which === 32) return false;
        },
        change: function() {
            this.value = this.value.replace(/\s/g, "");
        }
    });

    function findAncestor (el, cls) {
        console.log(el);
        while ((el = el.parentElement) && !el.classList.contains(cls));
        return el;
    }

    $('.closeTripPrice').click(function(e) {
        e.preventDefault();
        var hideRow = findAncestor($(this)[0], 'row');

        var id = $(this).data('id');
        $.ajax({
            type: "post",
            url: "/dashboard/trip/closePrice/"+id,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                hideRow.remove();
            }
        });
    });

    /* Tickets Filter */
    $('.ticketsCompanySelect').on('change', function(e) {
        e.preventDefault();
        var companyId = $(this).val();
        var date = $('.ticketsDateSelect').val();
        if(companyId !== '888') {
            var url = '/dashboard/getCompanyTickets/'+companyId;
            var coursesUrl = '/dashboard/company/getCourses/'+companyId;
            $.ajax({
                type: "GET",
                url: coursesUrl,
                success: function(data) {
                    $('.ticketsCourseSelect').empty();
                    $('.ticketsCourseSelect').attr('disabled', false);
                    $.each(data, function(i, val) {
                        $('.ticketsCourseSelect').append(
                            "<option value='"+val.id+"'>"+val.name+"</option>"
                        )
                    });
                }
            });

        } else {
            var url = '/dashboard/getCompanyTickets';
            $('.ticketsCourseSelect').empty();
            $('.ticketsCourseSelect').attr('disabled', true);
        }
        $.ajax({
            type: "GET",
            url: url,
            success: function(data) {
                $('#ticketsTable').empty();
                $.each(data, function(i, val) {
                    $('#ticketsTable').append(
                        '<tr><td>'+val.code+'</td>' +
                        '<td>'+val.from+'</td>'+
                        '<td>'+val.to+'</td>'+
                        '<td>'+val.time+'</td>'+
                        '<td>'+val.user+'</td>'+
                        '<td>'+val.email+'</td>'+
                        '<td>'+val.status+'</td></tr>'
                    )
                });
            }
        });
    });

    // by course
    $('.ticketsCourseSelect').on('change', function(e) {
        e.preventDefault();
        var companyId = $('.ticketsCompanySelect').val();
        var courseId = $(this).val();

        if(companyId !== '888') {
            var url = '/dashboard/getCompanyTickets/'+companyId+'/'+courseId;
        } else {
            var url = '/dashboard/getCompanyTickets/'+companyId;
        }

        $.ajax({
            type: "GET",
            url: url,
            success: function(data) {
                $('#ticketsTable').empty();
                $.each(data, function(i, val) {
                    $('#ticketsTable').append(
                        '<tr><td>'+val.code+'</td>' +
                        '<td>'+val.from+'</td>'+
                        '<td>'+val.to+'</td>'+
                        '<td>'+val.time+'</td>'+
                        '<td>'+val.user+'</td>'+
                        '<td>'+val.email+'</td>'+
                        '<td>'+val.status+'</td></tr>'
                    )
                });
            }
        });
    });

    $('.ticketsDateSelect').on('change', function(e) {
        var date = $(this).val();
        var url = '/dashboard/getTicketsByDate/'+date;
        $.ajax({
            type: "GET",
            url: url,
            success: function(data) {
                $('#ticketsTable').empty();
                $.each(data, function(i, val) {
                    $('#ticketsTable').append(
                        '<tr><td>'+val.code+'</td>' +
                        '<td>'+val.from+'</td>'+
                        '<td>'+val.to+'</td>'+
                        '<td>'+val.time+'</td>'+
                        '<td>'+val.user+'</td>'+
                        '<td>'+val.email+'</td>'+
                        '<td>'+val.status+'</td></tr>'
                    )
                });
            }
        });
    });
    /* End Tickets Filter */
    // Dashboard display statistics by selected currency
    $('#changeCurrency').on('change', function(e) {
        e.preventDefault();
        var currencyId = $(this).val();
        url = '/dashboard/exchange/currency/'+ currencyId;
        $.ajax({
            type: "GET",
            url: url,
            success: function(data) {
                console.log(data);
                $('.currencyLabel').text(data.currency);
            }
        });
    });


    /* End Dashboard settings */
    /* Roles and Permissions */
    $('.changePermission').on('change', function(e) {
        e.preventDefault();
        var type = $(this).data('type');
        if(type === 'user') {

        } else if (type === 'role') {
            var role = $(this).data('role');
            var permission = $(this).data('permission');
            if($(this).is(':checked')) { var sw = true;} else { var sw = false;}
            $.ajax({
                type: "POST",
                url: "/dashboard/settings/changeRolePermission/"+sw+"/"+role+"/"+permission,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    location.reload();
                }
            });
        }
    });
});